<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct(); // needed when adding a constructor to a controller
        $this->data = array(
            'layoutmode' => $this->config->item('layoutconfigdev'),

        );

        if(!$this->session->userdata('user_id')){
            $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
            redirect($this->agent->referrer());
        }

        $this->load->model('Postmodel');
        $this->load->model('Batchmodel');
    }


    function getExtension($str)
    {
        $i = strrpos($str,".");
        if (!$i)
        {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }



    public function activity_console(){

        //checking of date is blank or not
        if($this->input->post('go') != ""){
            $from_date=$this->input->post('from_date');
            $to_date=$this->input->post('to_date');
            $time_interval=0;
        }
        else{
            $from_date=0;
            $to_date=0;
            $time_interval=4;
        }

        //assigning value to an array
        $datacounter = array(
            'from_date' => $from_date,
            'to_date' => $to_date,
            'time_interval' => $time_interval,
        );

        $body_data['console_data'] = $this->Postmodel->get_all_user_activity($datacounter);
        @$this->db->free_db_resource();

        $body_data['abc'] = $this->Postmodel->get_activity_counter($datacounter);
        @$this->db->free_db_resource();

        $body_data['time_interval']=$time_interval;
        $body_data['base_url_main']=$this->config->item('base_url_main');

        //basic info for the header
        $layout_data['pageTitle'] = "Activity Console";
        $layout_data['meta_description'] = "Activity Console";
        $layout_data['meta_keywords'] = "ragamix_admin,Activity Console";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/activity_console', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }


    public function dashboard(){
        //basic info for the header
        $layout_data['pageTitle'] = "Dashboard";
        $layout_data['meta_description'] = "Dashboard";
        $layout_data['meta_keywords'] = "ragamix_admin,Dashboard";
        $layout_data['meta_classification'] = "home";
        $body_data[] = '';
        $layout_data['content_body'] = $this->load->view('dashboard', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }


    public function profileEdit()
    {
        if(!$this->session->userdata('user_id')){
            $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
            redirect($this->agent->referrer());
        }

        $base_url = base_url();

        //basic info for the header
        $layout_data['pageTitle'] = "Edit Your Profile - Ragamix";
        $layout_data['meta_description'] = "Under Ground Lyrics, hardcore, metal, emo, rock";
        $layout_data['meta_keywords'] = "lyrics,song,songs,words,hardore,metal,emo,rock";
        $layout_data['meta_url'] = "$base_url";
        $layout_data['meta_classification'] = "home";
        if($this->uri->segment(3) == 'basic'){
            $body_data['feeds'] = 0;
            if($this->uri->segment(4) != ""){
                $body_data['basicprofiledata'] = $this->Profilemodel->getBasicProfileData($this->uri->segment(4));
            }else{
                $body_data['basicprofiledata'] = $this->Profilemodel->getBasicProfileData($this->session->userdata('user_id'));
            }
            @$this->db->free_db_resource();
            $idVal = 0;
            $body_data['userTypeList'] = $this->Profilemodel->getUsertypeLists($idVal);
            @$this->db->free_db_resource();
            $body_data['userTypeListTwo'] = $this->Profilemodel->getUsertypeListsTwo($idVal);
            @$this->db->free_db_resource();
            $body_data['countryName'] = $this->Profilemodel->getCountry();
            @$this->db->free_db_resource();
            $body_data['cityName'] = $this->Profilemodel->getCity($countryVal='india');
            //Get Lat Long data
            $this->session->set_userdata(
                array(
                    'userlat' => $body_data['basicprofiledata'][0]->loc_lat,
                    'userlong' => $body_data['basicprofiledata'][0]->loc_lang
                )
            );
        }
        else if($this->uri->segment(3) == 'music'){
            $body_data['feeds'] = 1;
            if($this->uri->segment(4) != "") {
                $body_data['musicdata'] = $this->Profilemodel->getMusicData($this->uri->segment(4));
            }else{
                $body_data['musicdata'] = $this->Profilemodel->getMusicData($this->session->userdata('user_id'));
            }
            @$this->db->free_db_resource();
            $body_data['getGenre'] = $this->Profilemodel->getGenreList();
            @$this->db->free_db_resource();
            $body_data['getVocals'] = $this->Profilemodel->getVocalsList();
            @$this->db->free_db_resource();
            $body_data['getStrIns'] = $this->Profilemodel->getStringInstruments();
            @$this->db->free_db_resource();
            $body_data['getReedIns'] = $this->Profilemodel->getReedInstruments();
            @$this->db->free_db_resource();
            $body_data['getPer'] = $this->Profilemodel->getPercussions();
            @$this->db->free_db_resource();
            $body_data['getWindIns'] = $this->Profilemodel->getWindInstruments();
            @$this->db->free_db_resource();
        }
        else if($this->uri->segment(3) == 'photo'){
            $body_data['feeds'] = 2;
            if($this->uri->segment(4) != "") {
                $body_data['getPhotos'] = $this->Profilemodel->getProfilePhotos($this->uri->segment(4));
                @$this->db->free_db_resource();
            }else{
                $body_data['getPhotos'] = $this->Profilemodel->getProfilePhotos($this->session->userdata('user_id'));
                @$this->db->free_db_resource();
            }
            $body_data['getCdnUrl'] = $this->cdnImage['cdnimage'];
        }
        else if($this->uri->segment(3) == 'audiovideo'){
            $body_data['feeds'] = 3;
            if($this->uri->segment(4) != "") {
                $body_data['getAllAudios'] = $this->Profilemodel->getAudios($this->uri->segment(4));
                @$this->db->free_db_resource();
                $body_data['getAllVideos'] = $this->Profilemodel->getVideos($this->uri->segment(4));
                @$this->db->free_db_resource();
            }else {
                $body_data['getAllAudios'] = $this->Profilemodel->getAudios($this->session->userdata('user_id'));
                @$this->db->free_db_resource();
                $body_data['getAllVideos'] = $this->Profilemodel->getVideos($this->session->userdata('user_id'));
                @$this->db->free_db_resource();
            }
        }
        else if($this->uri->segment(3) == 'musicresource'){
            $body_data['feeds'] = 4;
            if($this->uri->segment(4) != "") {
                $body_data['getReqs'] = $this->Profilemodel->getSeekingRequirements($this->uri->segment(4));
            }else {
                $body_data['getReqs'] = $this->Profilemodel->getSeekingRequirements($this->session->userdata('user_id'));
            }
        }
        else if($this->uri->segment(3) == 'showevents'){
            $this->load->library('googlemaps');

            $config['center'] = '22.572,88.433';
            $config['zoom'] = '20';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] = '22.572,88.433';
            $this->googlemaps->add_marker($marker);
            $body_data['map'] = $this->googlemaps->create_map();
            $body_data['feeds'] = 5;
            if($this->uri->segment(4) != "") {
                $body_data['getShowsEvents'] = $this->Profilemodel->getShowsEventsData($this->uri->segment(4));
            }else {
                $body_data['getShowsEvents'] = $this->Profilemodel->getShowsEventsData($this->session->userdata('user_id'), $show_ev_id = 0);

            }
            @$this->db->free_db_resource();
            $countryVal = 'india';
            $body_data['cityName'] = $this->Profilemodel->getCityNameOnCountry($countryVal);
        }
        else if($this->uri->segment(3) == 'postads'){
            $body_data['feeds'] = 6;
            if($this->uri->segment(4) != "") {
                $data['records'] = $this->Profilemodel->getWantedAds($this->uri->segment(4), $ad_id = 0, $category = '', $position = 0, $items = 100);
            } else {
                $data['records'] = $this->Profilemodel->getWantedAds($this->session->userdata('user_id'), $ad_id = 0, $category = '', $position = 0, $items = 100);

            }
            $body_data['getAds'] = $data['records']['records'];
        }
        $layout_data['content_body'] = $this->load->view('menulist/profileEdit', $body_data, true);

        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function event(){
        $this->load->model('Postmodel');
        $rm_id=0;
        $event_ad_id=$this->uri->segment(3);
        $datacounter = array(
            'rm_id' => $rm_id,
            'event_ad_id' => $event_ad_id,
        );

        $body_data['updateData'] = $this->Postmodel->get_all_events_info($datacounter);
        @$this->db->free_db_resource();
        $body_data['locationdatafetch'] = $this->Postmodel->getStringLocationInfo();
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Events";
        $layout_data['meta_description'] = "Events";
        $layout_data['meta_keywords'] = "ragamix_admin,Events";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/editEvent', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function editEventsadmin(){

        $this->load->model('Postmodel');
        if(!empty($_FILES['file']['name'])){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
            $imagename = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $tmp = $_FILES['file']['tmp_name'];
            if(strlen($imagename))
            {
                $ext = strtolower($this->getExtension($imagename));
                if(in_array($ext,$valid_formats))
                {
                    if($size<(1024*1024)) // Image size max 1 MB
                    {
                        $bucketname="ragamix";
                        $fileObjUri = "ragamix/all-prod/";
                        if (!class_exists('S3'))require_once('S3.php');

                        //AWS access info
                        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                        if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                        //instantiate the class
                        $s3 = new S3(awsAccessKey, awsSecretKey);

                        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                        $actual_image_name = time().$session_ids.".".$ext;

                        if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                        {
                            $picUrl = "http://cdn.ragamix.com/ragamix/all-prod/".$actual_image_name;
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 1 MB";
                }
                else
                    echo "Invalid file format..";
            }
            else
                echo "Please select image..!";

        }
        else {
            $picUrl = $this->input->post('pic_url');
        }
        $evid = $this->input->post('event_ad_id_in');
        $data = array(
            'event_ad_id_in' => $this->input->post('event_ad_id_in'),
            'rm_id' => $this->input->post('rm_id'),
            'category' => $this->input->post('category'),
            'event_location' => $this->input->post('event_location'),
            'event_city' => $this->input->post('event_city'),
            'event_date' => $this->input->post('event_date'),
            'event_title' => htmlspecialchars($this->input->post('event_title'),ENT_QUOTES),
            'event_desc' => htmlspecialchars($this->input->post('event_desc'),ENT_QUOTES),
            'doc_youtube_url' => $this->input->post('doc_youtube_url'),
            'event_approved' => $this->input->post('approval'),
            'page_position' => 1,
            'photo_id_in' => $this->input->post('photo_id_in'),
            'pic_url' => $picUrl,
            'pic_privacy' => 1,
            'pic_height' => 100,
            'pic_width' => 100
        );

        $lastEventsId = $this->Postmodel->insertEventsadmin($data);
        $this->session->set_flashdata('messageError', 'Event Updated Successfully');
        redirect('admin/event/'.$evid);
    }

    function Ads(){
        $this->load->model('Postmodel');
        $rm_id=0;
        $ad_id=$this->uri->segment(3);
        $category_in=0;
        $offset_val=0;
        $limit_in=0;

        $datacounter = array(
            'rm_id' => $rm_id,
            'ad_id' => $ad_id,
            'category_in' => $category_in,
            'offset_val' => $offset_val,
            'limit_in' => $limit_in,
        );

        $body_data['updateData'] = $this->Postmodel->get_all_ads_info($datacounter);
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Ads";
        $layout_data['meta_description'] = "Ads";
        $layout_data['meta_keywords'] = "ragamix_admin,Ads";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/editAds', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function editadsadmin(){

        $this->load->model('Postmodel');
        if(!empty($_FILES['file']['name'])){
            $session_ids = 1;
            $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
            $imagename = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $tmp = $_FILES['file']['tmp_name'];
            if(strlen($imagename))
            {
                $ext = strtolower($this->getExtension($imagename));
                if(in_array($ext,$valid_formats))
                {
                    if($size<(1024*1024)) // Image size max 1 MB
                    {
                        $bucketname="ragamix";
                        $fileObjUri = "ragamix/all-prod/";
                        if (!class_exists('S3'))require_once('S3.php');

                        //AWS access info
                        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJ3XYGL6LANED6ABA');
                        if (!defined('awsSecretKey')) define('awsSecretKey', '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');

                        //instantiate the class
                        $s3 = new S3(awsAccessKey, awsSecretKey);

                        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                        $actual_image_name = time().$session_ids.".".$ext;

                        if($s3->putObjectFile($tmp, $bucketname , $fileObjUri.$actual_image_name, S3::ACL_PUBLIC_READ) )
                        {
                            $picUrl = "http://cdn.ragamix.com/ragamix/all-prod/".$actual_image_name;
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 1 MB";
                }
                else
                    echo "Invalid file format..";
            }
            else
                echo "Please select image..!";

        }
        else {
            $picUrl = $this->input->post('pic_url');
        }
        $adid = $this->input->post('ad_id_in');

        $data = array(
            'ad_id_in' => $adid,
            'rm_id' => $this->input->post('rm_id'),
            'category' => $this->input->post('category'),
            'ads_title' => htmlspecialchars($this->input->post('ads_title'),ENT_QUOTES),
            'ads_desc' => htmlspecialchars($this->input->post('ads_desc'),ENT_QUOTES),
            'ads_views' => $this->input->post('ads_views'),
            'ads_approved' => $this->input->post('approval'),
            'ads_status' => $this->input->post('ads_status'),
            'enable_ads' => $this->input->post('enable_ads'),
            'page_position' => 1,
            'photo_id_int' => $this->input->post('photo_id_int'),
            'pic_url' => $picUrl,
            'pic_privacy' => 1,
            'pic_height' => 100,
            'pic_width' => 100
        );

        $lastEventsId = $this->Postmodel->insertAdsadmin($data);
        $this->session->set_flashdata('messageError', 'Ads Updated Successfully');
        redirect('admin/Ads/'.$adid);
    }

    function evlist(){

        $this->load->model('Batchmodel');
        $body_data['q'] = $_GET['q'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($prev == 'y'){
            $body_data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $body_data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;

        if($body_data['q'] == 1){
            $body_data['getEventsList'] = $this->Batchmodel->getAllEventsAdminApproved($position,$items=20,$qu=1);
        }
        else if($body_data['q'] == 0){
            $body_data['getEventsList'] = $this->Batchmodel->getAllEventsAdminApproved($position,$items=20,$qu=0);
        }
        else{
            $body_data['rec'] = $this->Batchmodel->getAllEventsAdmin($position,$items=20,$rowcnt='');
            $body_data['getEventsList'] = $body_data['rec']['records'];
        }

        $layout_data['pageTitle'] = "News/Events Content";
        $layout_data['meta_description'] = "News/Events Content";
        $layout_data['meta_keywords'] = "ragamix_admin,News/Events Content";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/evlist', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    /*FEATURED PROFILE START*/
    function featured_profile(){

        $portal=$_GET['type'];

        $datalist=array(
            'section'=>$_GET['sec'],
            'portal'=>$portal,
        );


        if($_GET['sec'] == 'Homepage'){
            $body_data['featuredlist'] = $this->Batchmodel->getFeaturedProfileList($portal);
            @$this->db->free_db_resource();
        }else{
            $body_data['featuredlist'] = $this->Batchmodel->getFeaturedMusician($datalist);
            @$this->db->free_db_resource();
        }


        $countryVal = 'india';
        $body_data['cityName'] = $this->Batchmodel->getCityNameOnCountry($countryVal);
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Featured Profile";
        $layout_data['meta_description'] = "Featured Profile";
        $layout_data['meta_keywords'] = "ragamix_admin,Featured Profile";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/featured_profile', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function featured_profile_add(){

        $section=$this->input->post('section');
        $app_type=$this->input->post('app_type');

        if($this->input->post('section') == "Homepage"){
            $datalist=array(
                'user_name_in'=>$this->input->post('txtcliq_hidden'),
                'rm_id_in'=>$this->input->post('rm_id'),
                'position_in'=>$this->input->post('position'),
                'app_type'=>$app_type,
            );

            $body_data['addfeaturedprof'] = $this->Batchmodel->AddFeaturedProfile($datalist);
            @$this->db->free_db_resource();

            $this->session->set_flashdata('message', $body_data['addfeaturedprof']);
            redirect($this->agent->referrer());
        }else{
            $datalist=array(
                'display_page_in'=>$this->input->post('section'),
                'rm_id_in'=>$this->input->post('rm_id'),
                'position_in'=>$this->input->post('position'),
                'app_type'=>$app_type,
            );

            $body_data['addfeaturedprof'] = $this->Batchmodel->AddFeaturedProfileOthers($datalist);
            @$this->db->free_db_resource();

            $this->session->set_flashdata('message', $body_data['addfeaturedprof']);
            redirect($this->agent->referrer());
        }
    }

    function deleteProf(){

        $fid=$this->input->post('hidval');
        $section=$this->input->post('hidsection');

//        print_r($fid);exit();

        if($section == "Homepage"){
//            print_r($section);exit();
            $this->Batchmodel->deleteFProfile($fid);
            @$this->db->free_db_resource();
        }else{
//            print_r("Gello");exit();
            $this->Batchmodel->deleteFProfileOthers($fid);
            @$this->db->free_db_resource();
        }

        $this->session->set_flashdata('messageremove',"Data Deleted Successfully");
        redirect($this->agent->referrer());
    }

    /*FEATURED PROFILE END*/



    /*FEATURED EVENTS START*/
    function featured_events(){

        $portal=$_GET['type'];

        $body_data['featuredeventlist'] = $this->Batchmodel->get_featured_events($portal);
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Featured Events";
        $layout_data['meta_description'] = "Featured Events";
        $layout_data['meta_keywords'] = "ragamix_admin,Featured Events";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/featured_events', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function featured_events_add(){

        $datalist=array(
            'event_id'=>$this->input->post('event_id_hidden'),
            'event_title'=>$this->input->post('event_title_hidden'),
            'user_name'=>$this->input->post('username_hidden_evnt'),
            'position'=>$this->input->post('position'),
            'app_type_in'=>$this->input->post('app_type'),
        );

        $body_data['addfeaturedevents'] = $this->Postmodel->add_featured_events($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message', $body_data['addfeaturedevents']);
        redirect($this->agent->referrer());
    }
    /*FEATURED EVENTS ENDS*/




    /*FEATURED ADS START*/
    function featured_ads(){

        $portal=$_GET['type'];

        $body_data['featuredadlist'] = $this->Batchmodel->get_featured_ads($portal);
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Featured Ads";
        $layout_data['meta_description'] = "Featured Ads";
        $layout_data['meta_keywords'] = "ragamix_admin,Featured Ads";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/featured_ads', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function featured_ads_add(){

        $datalist=array(
            'ads_id'=>$this->input->post('ad_id_hidden'),
            'ads_title'=>$this->input->post('ad_title_hidden'),
            'user_name'=>$this->input->post('username_hidden_ad'),
            'position'=>$this->input->post('position'),
            'app_type_in'=>$this->input->post('app_type'),
        );

        $body_data['addfeaturedad'] = $this->Postmodel->add_featured_ads($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message', $body_data['addfeaturedad']);
        redirect($this->agent->referrer());
    }
    /*FEATURED ADS ENDS*/


    /*USERTYPE START*/
    function usertype(){

        $body_data['usertypedata'] = $this->Postmodel->get_usertype_list();
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "User Type";
        $layout_data['meta_description'] = "User Type";
        $layout_data['meta_keywords'] = "ragamix_admin,User Type";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/usertype', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function usertype_del(){

        $datalist = array(
            'usr_type_id' => $this->input->post('hidval'),
        );

        $this->Postmodel->delete_usertype_info($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }

    function usertype_save(){

        if($this->input->post('cancel') != ""){
            redirect($this->agent->referrer());
        }

            $datalist= array(
                'usr_type_id' => $this->input->post('usertypeid'),
                'usr_type' => $this->input->post('usertypedetails')
            );

            $data['usertypedata'] = $this->Postmodel->save_update_usertype($datalist);
            @$this->db->free_db_resource();

            $this->session->set_flashdata('message','Data Saved Successfully');
            redirect($this->agent->referrer());
    }
    /*USERTYPE ENDS*/

    /*Added By Nandini Starting*/
    //******************************//
    /*STRING INSTRUMENT START*/
    function stringinstrument(){

        $body_data['stringinstrumentdata'] = $this->Postmodel->get_stringinstr_list();
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "String Instrument";
        $layout_data['meta_description'] = "String Instrument";
        $layout_data['meta_keywords'] = "ragamix_admin,String Instrument";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/stringinstrument', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function stringinstr_del(){
        $data = array(
            'stringinstr_id' => $this->input->post('hidval'),
        );

        $this->Postmodel->delete_stringinstr_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }

    function stringinstr_save()
    {

        if($this->input->post('cancel') != ""){
            redirect($this->agent->referrer());
        }

            $stringinstrid= $this->input->post('stringinstrid');
            $data = array(
                'stringinstr_id' => $stringinstrid,
                'stringinstr' => $this->input->post('stringinstr')
            );

            $this->Postmodel->save_update_stringinstr($data);
            @$this->db->free_db_resource();

        $this->session->set_flashdata('message','Data Saved Successfully');
        redirect($this->agent->referrer());
    }
    /*STRING INSTRUMENT ENDS*/



    /*WIND INSTRUMENT START*/
    function windinstrument(){

        $body_data['windinstrumentdata'] = $this->Postmodel->get_windinstr_list();
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Wind Instrument";
        $layout_data['meta_description'] = "Wind Instrument";
        $layout_data['meta_keywords'] = "ragamix_admin,Wind Instrument";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/windinstrument', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function windinstr_del(){

        $data = array(
            'windinstr_id' => $this->input->post('hidval'),
        );

        $this->Postmodel->delete_windinstr_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }

    function windinstr_save(){

        if($this->input->post('cancel') != ""){
            redirect($this->agent->referrer());
        }

            $windinstrid=$this->input->post('windinstrid');
            $datalist = array(
                'windinstr_id' => $windinstrid,
                'windinstr' => $this->input->post('windinstr')
            );

            $this->Postmodel->save_update_windinstr($datalist);
            @$this->db->free_db_resource();

            $this->session->set_flashdata('message','Data Saved Successfully');
            redirect($this->agent->referrer());
    }
    /*WIND INSTRUMENT ENDS*/


    /*REED INSTRUMENT START*/
    function reedinstrument(){

        $body_data['reedinstrumentdata'] = $this->Postmodel->get_reed_list();
        @$this->db->free_db_resource();

        $layout_data['pageTitle'] = "Reed Instrument";
        $layout_data['meta_description'] = "Reed Instrument";
        $layout_data['meta_keywords'] = "ragamix_admin,Reed Instrument";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/reedinstrument', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function reedinstr_del(){

        $data = array(
            'reedinstr_id' => $this->input->post('hidval'),
        );

        $this->Postmodel->delete_reedinstr_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }

    function reedinstr_save(){

        if($this->input->post('cancel') != ""){
            redirect($this->agent->referrer());
        }

        $reedinstr_id=$this->input->post('reedinstrid');
        $data = array(
            'reedinstr_id' => $reedinstr_id,
            'reedinstr' => $this->input->post('reedinstr')
        );

        $this->Postmodel->save_update_reedinstr($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message','Data Saved Successfully');
        redirect($this->agent->referrer());
    }

    /*REED INSTRUMENT END*/

    /*GENRE START*/
    function genre()
    {

        $this->load->model('Postmodel');
        $body_data['genredata'] = $this->Postmodel->get_genre_list();

        $layout_data['pageTitle'] = "Genre";
        $layout_data['meta_description'] = "Genre";
        $layout_data['meta_keywords'] = "ragamix_admin,Genre";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/genre', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function genre_del(){

        $data = array(
            'genre_id' => $this->input->post('hidval'),
        );
        $data['genredel'] = $this->Postmodel->delete_genre_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }

    function genre_save()
    {

        if($this->input->post('cancel') != ""){
            redirect($this->agent->referrer());
        }

        $genreid = $this->input->post('genreid');
        $data = array(
            'genre_id' => $genreid,
            'genre' => $this->input->post('genre')
        );

        $this->Postmodel->save_update_genre($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message','Data Saved Successfully');
        redirect($this->agent->referrer());
    }
    /*GENRE END*/

    /*VOCAL START*/
    function vocal()
    {

        $this->load->model('Postmodel');
        $body_data['vocaldata'] = $this->Postmodel->get_vocal_list();

        $layout_data['pageTitle'] = "Vocal";
        $layout_data['meta_description'] = "Vocal";
        $layout_data['meta_keywords'] = "ragamix_admin,Vocal";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/vocal', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function vocal_del(){

        $data = array(
            'vocal_id' => $this->input->post('hidval'),
        );

        $this->Postmodel->delete_vocal_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }

    function vocal_save()
    {
        if($this->input->post('cancel') != ""){
            redirect($this->agent->referrer());
        }

            $vocalid = $this->input->post('vocalid');
            $data = array(
                'vocal_id' => $vocalid,
                'vocal' => $this->input->post('vocal')
            );

            $this->Postmodel->save_update_vocal($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message','Data Saved Successfully');
            redirect($this->agent->referrer());
    }
    /*VOCAL END*/

    /*LOCATION START*/
    function location(){

        $this->load->library('googlemaps');

        $data['locationdatafetch'] = $this->Postmodel->get_location_info();
        @$this->db->free_db_resource();


        if($this->uri->segment(3) == 'edit'){
            for($i=0;$i<sizeof($data['locationdatafetch']);$i++){
                if($data['locationdatafetch'][$i]->location_id == $this->uri->segment(4)){

                    $lat=$data['locationdatafetch'][$i]->lat;
                    $long=$data['locationdatafetch'][$i]->long;
                    $data['editlocationid'] = $data['locationdatafetch'][$i]->location_id;
                    $data['editlocationcity'] = $data['locationdatafetch'][$i]->city;
                    $data['editlocationcountry'] = $data['locationdatafetch'][$i]->country;
                    $data['editlocationzone'] = $data['locationdatafetch'][$i]->zone;
                    $data['editlocationlat'] = $lat;
                    $data['editlocationlong'] = $long;
                }
            }

            $this->session->set_userdata(
                array(
                    'locid' => $this->uri->segment(4),
                )
            );

            $config['center'] = $lat.','.$long;
            $config['zoom'] = '15';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] =  $lat.','.$long;
            $this->googlemaps->add_marker($marker);
            $data['map'] = $this->googlemaps->create_map();
        }
        elseif($this->uri->segment(5) == '1'){
            $adminlat=$this->session->userdata('adminlat');
            $adminlong=$this->session->userdata('adminlong');
            $config['center'] = $adminlat.','.$adminlong;
            $config['zoom'] = '15';
            $this->googlemaps->initialize($config);

            $marker = array();
            $marker['position'] = $adminlat.','.$adminlong;
            $this->googlemaps->add_marker($marker);
            $data['map'] = $this->googlemaps->create_map();

            $data['lat']=$adminlat;
            $data['long']=$adminlong;
        }
        else{

            $config['center'] = '20.5937,78.9629';
            $config['zoom'] = '3';
            $this->googlemaps->initialize($config);

            $data['map'] = $this->googlemaps->create_map();
        }


        $layout_data['pageTitle'] = "Location";
        $layout_data['meta_description'] = "Location";
        $layout_data['meta_keywords'] = "ragamix_admin,Location";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/location', $data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function location_save(){


        if($this->input->post('reset') != ""){

            $this->session->unset_userdata('locid');
            redirect('admin/location');
        }
        if($this->input->post('save') != ''){

            $datalist = array(
                'location_id' =>$this->input->post('locationid'),
                'city' => $this->input->post('citydetails'),
                'country' => $this->input->post('countrydetails'),
                'zone' => $this->input->post('zonedetails'),
                'lat' => $this->input->post('latdetails'),
                'long' => $this->input->post('longdetails'),
            );


            $data['locationdatafetch'] = $this->Postmodel->save_update_location_info($datalist);
            $this->session->unset_userdata('locid');

            $this->session->set_flashdata('message','Data Saved Successfully');
            redirect('admin/location');
        }
        elseif($this->input->post('find') != '') {
            if($this->input->post('formval') && $this->input->post('city')){

                $address1 = $this->input->post('country');
                $formval = $this->input->post('zone');
                $address = $this->input->post('latitude');

                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                // We convert the JSON to an array
                $geo = json_decode($geo, true);
                // If everything is cool
                if ($geo['status'] = 'OK') {
                    // We set our values


                    $latitude = $geo['results'][0]['geometry']['location']['lat'];
                    $longitude = $geo['results'][0]['geometry']['location']['lng'];

                }

                $latlong = array();
                $latlong[0] = $latitude;
                $latlong[1] = $longitude;

                $this->session->set_userdata(
                    array(
                        'adminlat' => $latlong[0],
                        'adminlong' =>  $latlong[1],
                    )
                );
                redirect("admin/location/".$formval."/".$address1."/1");
            }
            else {

                $address = $this->input->post('citydetails');

                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
                // We convert the JSON to an array
                $geo = json_decode($geo, true);
                // If everything is cool
                if ($geo['status'] = 'OK') {
                    // We set our values

                    $city = $geo['results'][0]['address_components'][0]['long_name'];
                    $country = $geo['results'][0]['address_components'][3]['long_name'];
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];
                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                }

//                $latlong = array();
//                $latlong[0] = $latitude;
//                $latlong[1] = $longitude;

                $this->session->set_userdata(
                    array(
                        'admincity' => $city,
                        'admincountry' => $country,
                        'adminlat' => $latitude,
                        'adminlong' => $longitude,
                    )
                );

                redirect("admin/location/add/0/1");
            }
        }
    }

    function location_del(){

        $datalist=array(
            'location_id'=>$this->input->post('hidval'),
        );

        $this->Postmodel->delete_location_info($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }


    function testimonials()
    {
        $data=array(
            'testimonials_id'=>0,
        );

        $body_data['testimonialsdata'] = $this->Postmodel->get_testimonials_list($data);
        @$this->db->free_db_resource();

//        print_r($body_data['testimonialsdata']);exit();

        $layout_data['pageTitle'] = "Testimonials";
        $layout_data['meta_description'] = "Testimonials";
        $layout_data['meta_keywords'] = "ragamix_admin,Testimonials";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/testimonials', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }


    public function do_upload()
    {

        $config['upload_path']          = './uploads/';

        $config['allowed_types']        = 'gif|jpg|png';
//        $config['max_size']             = 100;
//        $config['max_width']            = 1024;
//        $config['max_height']           = 768;

        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('fileToUpload'))
        {


            $picurl=$this->input->post('hiddenpic');
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $picurl=$data['upload_data']['file_name'];
        }

        $datalist = array(
            'testimonials_id' => $this->input->post('testimonials_id'),
            'full_name' => $this->input->post('fullname'),
            'location' => $this->input->post('location'),
            'detail_info' => htmlspecialchars($this->input->post('ads_desc'),ENT_QUOTES),
            'pic_url' =>$picurl,
            'prof_url' => $this->input->post('prof_url'),
        );

        $data['testimonialsdata'] = $this->Postmodel->save_update_testimonial($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message','Data Saved Successfully');
        redirect($this->agent->referrer());
    }


    function del_testimonials(){

        $datalist = array(
            'testimonials_id' => $this->input->post('hidval'),
        );

        //deleting file from the directory
        $file_path='./uploads/'.$this->input->post('hidpic');
        $files = glob($file_path);
        foreach($files as $file) {
            unlink($file);
        }

        $data['testimonialsdata'] = $this->Postmodel->delete_testimonial_info($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove','Data Deleted Successfully');
        redirect($this->agent->referrer());
    }


    function slidercontrol(){

        $datalist=array(
            'slider_id'=>0,
            'slider_type'=>$_GET['slid_type'],
            'app_type'=>$_GET['app_type'],

        );

        $body_data['sliderdata'] = $this->Postmodel->get_home_page_slider($datalist);
        @$this->db->free_db_resource();

//        print_r( $body_data['sliderdata']);exit();

        $body_data['usernamedata'] = $this->Postmodel->get_username($datalist);


        $body_data['username']=$this->session->userdata('user_name');
        $body_data['userid']=$this->session->userdata('user_id');

        $layout_data['pageTitle'] = "Slider Control";
        $layout_data['meta_description'] = "Slider Control";
        $layout_data['meta_keywords'] = "ragamix_admin,Slider Control";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/slidercontrol', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function slider_add(){


        if($this->input->post('reset') != ""){

            redirect($this->agent->referrer());
        }

        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('fileToUpload'))
        {
//            print_r("Hello");exit();
            $picurl=$this->input->post('hiddenpic');
        }
        else
        {

            $data = array('upload_data' => $this->upload->data());
            $picurl=$data['upload_data']['file_name'];
        }

        $datalist=array(
            'image_url'=>$picurl,
            'slide_desc'=>$this->input->post('slide_desc'),
            'uploaded_by'=>$this->input->post('uploaded_by'),
            'position'=>$this->input->post('position'),
            'app_type'=>$this->input->post('app_type'),
            'slider_type'=>$this->input->post('slider_type'),
        );

        $body_data['sliderdata'] = $this->Postmodel->add_home_page_slider($datalist);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message', $body_data['sliderdata']);
        redirect($this->agent->referrer());
    }

    function delslider(){

        $slider_id=$this->input->post('hidval');

        $file_path='./uploads/'.$this->input->post('hidpicval');
        $files = glob($file_path);
        foreach($files as $file) {
            unlink($file);
        }

        $data['delslider']=$this->Postmodel->delete_slider_info($slider_id);
        @$this->db->free_db_resource();


        $this->session->set_flashdata('messageremove', "Data Deleted Successfully");
        redirect($this->agent->referrer());
    }

    /*LOCATION END*/
//***************************************//
    /*Added By Nandini Ending*/


    /*ROLES & RIGHTS START*/
    function roles_rights(){

        $datalist=array(
            'group_id'=>'pptur42lj',
        );

        $body_data['rolesrightdata'] = $this->Postmodel->get_all_user_groups();
        @$this->db->free_db_resource();

        $data['roles'] = $this->Postmodel->get_assigned_users($datalist);
        @$this->db->free_db_resource();

        $body_data['initialroles']='Admin';

        $layout_data['pageTitle'] = "Roles & Rights";
        $layout_data['meta_description'] = "Roles & Rights";
        $layout_data['meta_keywords'] = "ragamix_admin,Roles & Rights";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/rolesrights', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }
    /*ROLES & RIGHTS ENDS*/


    function profileList(){

        $body_data['q'] = $_GET['q'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($prev == 'y'){
            $body_data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $body_data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;
        if($body_data['q'] == 1){
            $usercat = 'Musician';
        }
        elseif($body_data['q'] == 2){
            $usercat = 'Singer';
        }
        elseif($body_data['q'] == 3){
            $usercat = 'Band';
        }
        elseif($this->uri->segment(3) == 'delete'){

            $body_data['deleteprof']= $this->Postmodel->delete_profile_info($this->uri->segment(4));
            $this->session->set_flashdata('messageError', 'Deleted...');
            redirect($this->agent->referrer());
        }
        else {
            $this->session->set_flashdata('messageError', 'Could not find the resource');
            redirect($this->agent->referrer());
        }
        $body_data['newrecords'] = $this->Batchmodel->autoCompSearch($completeStringToSearch='',$usercat,$city='',$position, $items=24);
        $body_data['getAllSearchM'] = $body_data['newrecords']['records'];

        $body_data['base_url_main']=$this->config->item('base_url_main');


        $layout_data['pageTitle'] = "Profile Content";
        $layout_data['meta_description'] = "Profile Content";
        $layout_data['meta_keywords'] = "ragamix_admin,Profile Content";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/profileList', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function adlist(){
        $body_data['q'] = $_GET['q'];
        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];
        if($prev == 'y'){
            $body_data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $body_data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;

        if($body_data['q'] == 1){
            $body_data['getAdsList'] = $this->Batchmodel->getAllAdsAdminApproved($position,$items=20,$qu=1);
        }
        else if($body_data['q'] == 0){
            $body_data['getAdsList'] = $this->Batchmodel->getAllAdsAdminApproved($position,$items=20,$qu=0);
        }
        else{
            $body_data['rec'] = $this->Batchmodel->getAllAdsAdmin($position,$items=20,$rowcnt='');
            $body_data['getAdsList'] = $body_data['rec']['records'];
        }

        $layout_data['pageTitle'] = "Ads Content";
        $layout_data['meta_description'] = "Ads Content";
        $layout_data['meta_keywords'] = "ragamix_admin,Ads Content";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/adlist', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function seeking(){

//        print_r($this->session->userdata('user_name'));exit();


        $data = array(
            'rm_id' => 0,
            'offset' => 0,
            'limit' => 50,
        );

        $body_data['seekinglist'] = $this->Postmodel->getSeekingRequirements($data);
        @$this->db->free_db_resource();



//        $body_data['username']=$this->session->userdata('user_name');

        $layout_data['pageTitle'] = "Seeking";
        $layout_data['meta_description'] = "Seeking";
        $layout_data['meta_keywords'] = "ragamix_admin,Seeking";
        $layout_data['meta_classification'] = "home";
        $layout_data['content_body'] = $this->load->view('menulist/seeking', $body_data, true);
        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function save_seeking(){


        if($this->input->post('reset') != ""){
            redirect($this->agent->referrer());
        }

        if($this->input->post('rm_id_take') != ""){
            $rm_id=$this->input->post('rm_id_take');
        }else{
            $rm_id=$this->session->userdata('user_id');
        }

        $data = array(
            'serial_no_in' => trim($this->input->post('serial_no')),
            'rm_id_in' => $rm_id,
            'keyword' => trim($this->input->post('keyword')),
            'location' => trim($this->input->post('location')),
            'description' => htmlspecialchars($this->input->post('description'),ENT_QUOTES),
            'hashtags' => trim($this->input->post('hashtags'))
        );

//        print_r($data);exit();

        $lastId =$this->Postmodel->postSeekRequirements($data);
        @$this->db->free_db_resource();

        if(!empty($lastId)){
            $this->session->set_flashdata('message', 'Requirement Posted Successfully');
            redirect($this->agent->referrer());
        }
        else {
            $this->session->set_flashdata('message', 'There was an error while posting..Please try again');
            redirect($this->agent->referrer());
        }
    }

    function del_seeking(){

    $serial_no=$this->input->post('hidval');

        $data['delseeking']=$this->Postmodel->delete_seeking_info($serial_no);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('message', ' Data Deleted Successfully ');
        redirect($this->agent->referrer());

    }


    /*AJAX CALL FUNCTION STARTING*/
    function getlistview(){

        $datalist=array(
            'from_date'=>0,
            'to_date'=>0,
            'time_interval'=>$this->input->post('time_interval'),
        );

        $body_data['console_data'] = $this->Postmodel->get_all_user_activity($datalist);
        @$this->db->free_db_resource();

        $this->load->view('ajaxcontent/gridlistactivityconsole',$body_data);
    }

    function  getsectionlistview(){

        $datalist=array(
            'section'=>$this->input->post('section'),
        );

        if($this->input->post('section') == "Homepage"){

            $data['featuredlist'] = $this->Batchmodel->getFeaturedProfileList();
            @$this->db->free_db_resource();

            $this->load->view('ajaxcontent/gridlistfeaturedprofile',$data);
        }else{
            $data['featuredlist'] = $this->Batchmodel->getFeaturedMusician($datalist);
            @$this->db->free_db_resource();

            $this->load->view('ajaxcontent/gridlistfeaturedprofile',$data);
        }
    }

    function searchEvent(){

        $datalist=array(
            'event_title'=>htmlspecialchars($this->input->post('event_title'),ENT_QUOTES),
            'posted_by'=>htmlspecialchars($this->input->post('posted_by'),ENT_QUOTES),
        );

        $data['findevents'] = $this->Batchmodel->get_events_for_featured($datalist);
        @$this->db->free_db_resource();

        $findevents=json_encode($data['findevents']);
        echo $findevents;
    }

    function deleteEvent(){

        $data=array(
            'featured_id'=>$this->input->post('hidval'),
        );

        $data['rmvfeaturedevnts']=$this->Postmodel->delete_featured_events_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove',$data['rmvfeaturedevnts']);
        redirect($this->agent->referrer());
    }

    function deleteAds(){

        $data=array(
            'featured_id'=>$this->input->post('hidval'),
        );

//        print_r($data);exit();

        $data['rmvfeaturedads']=$this->Postmodel->delete_featured_ads_info($data);
        @$this->db->free_db_resource();

        $this->session->set_flashdata('messageremove',$data['rmvfeaturedads']);
        redirect($this->agent->referrer());
    }


    function adsearch(){

        $datalist=array(
            'ad_title'=>$this->input->post('ad_title'),
            'posted_by'=>"",
            'app_type_in'=>0,
        );

        $body_data['addfeaturedad'] = $this->Postmodel->get_ads_for_featured($datalist);
        @$this->db->free_db_resource();

        $this->load->view('ajaxcontent/displayAdSearch', $body_data);
    }

    function eventsearch(){

        $datalist=array(
            'event_title'=>$this->input->post('evnt_title'),
            'posted_by'=>"",
            'app_type_in'=>0,
        );

        $body_data['addfeaturedprof'] = $this->Batchmodel->get_events_for_featured($datalist);
        @$this->db->free_db_resource();

        $this->load->view('ajaxcontent/displayEventSearch', $body_data);
    }

    function seekingsearch(){

        $testBoxString = $this->input->post('search');
        $exString = explode(' ',$testBoxString);
        $chars = array();
        for($i=0;$i<sizeof($exString);$i++){
            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
        }
        $completeStringToSearch = implode(' ',$chars);
        $data['sv'] = $testBoxString;

        if($this->input->post('usercat') == ""){
            $usercat='Musician';
        }else{
            $usercat = $this->input->post('usercat');
        }
        $data['city'] = $city = $this->input->post('location');
        $data['records'] = $this->Batchmodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position = 0,$items=3);
        $data['searchListM'] = $data['records']['records'];

        $this->load->view('ajaxcontent/displaySearch', $data);

    }

    function search(){

        $testBoxString = $this->input->post('search');
        $exString = explode(' ',$testBoxString);
        $chars = array();
        for($i=0;$i<sizeof($exString);$i++){
            $chars[$i] = strtolower(str_replace(" ", " ", $exString[$i])).'*';
        }
        $completeStringToSearch = implode(' ',$chars);
        $data['sv'] = $testBoxString;

        if($this->input->post('usercat') == ""){
            $usercat='Musician';
        }else{
            $usercat = $this->input->post('usercat');
        }
        $data['city'] = $city = $this->input->post('location');
        $data['records'] = $this->Batchmodel->autoCompSearch($completeStringToSearch,$usercat,$city,$position = 0,$items=3);
        $data['searchListM'] = $data['records']['records'];

        $this->load->view('ajaxcontent/displaySearch', $data);
    }

    function rolesrightdropdownajax(){

        $abc=$this->input->post('selval');

        $data = array(
            'group_id' => $abc
        );

        $data['roles'] = $this->Postmodel->get_assigned_users($data);
        @$this->db->free_db_resource();
        $data['rolesuserdata'] = $this->Postmodel->get_users_for_group();
    }

    function gettingtestimonials(){

        $testimonials_id=$this->input->post('testimonials_id');

//        print_r($testimonials_id);exit();

        $data = array(
            'testimonials_id' => $testimonials_id,
        );

        $data['testimonialsdata'] = $this->Postmodel->get_testimonials_list($data);
        @$this->db->free_db_resource();

        $jsn=json_encode($data['testimonialsdata']);

        echo $jsn;
    }

    /*AJAX CALL FUNCTION END*/

    function gettingsliders(){
        $sliders_id=$this->input->post('sliders_id');

//        print_r($testimonials_id);exit();

        $data = array(
            'sliders_id' => $sliders_id,
        );

        $data['slidersdata'] = $this->Postmodel->get_home_page_slider($data);
        @$this->db->free_db_resource();

        $jsn=json_encode($data['slidersdata']);

        echo $jsn;

    }

    function seekingajax(){

        $data = array(
            'rm_id' => $this->input->post('rm_id'),
            'offset' => 0,
            'limit' => 10,
        );

       $data['seekinglist'] = $this->Postmodel->getSeekingRequirements($data);
        @$this->db->free_db_resource();

        $findseekings=json_encode($data['seekinglist']);
        echo $findseekings;
    }




}
?>
