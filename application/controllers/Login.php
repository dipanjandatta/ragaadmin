<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

class Login extends CI_Controller {

    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        $this->data = array(
            'layoutmode' => $this->config->item('layoutconfigdev')
        );
        $this->load->model('Postmodel');
    }

    //function for login into the page
    function login(){
        try{
            $datalist = array(
                'user_name_in' => trim($this->input->post('username')),
                'password_in' => trim($this->input->post('password')),
                'fb_login' => 0
            );

            $data['retVal'] = $this->Postmodel->postLoginDetails($datalist);
            @$this->db->free_db_resource();

            if($data['retVal'][0]->rm_out_param > 0 && $data['retVal'][0]->active_out_param == 1){
             if($data['retVal'][0]->user_type_id == 3) {
                    $this->session->set_userdata(
                        array(
                            'user_id' => $data['retVal'][0]->rm_out_param,
                            'user_name' => $data['retVal'][0]->user_name_param,
                            'user_type_id' => $data['retVal'][0]->user_type_id,
                            'user_group_id' => $data['retVal'][0]->user_group_id,
                        )
                    );
                    redirect('admin/dashboard');
                }

                redirect($this->agent->referrer());

            }
            else{
                $this->session->set_flashdata('messageError', 'Mismatch in Data. Please Retry');
                redirect($this->agent->referrer());
            }
        }
        catch (Exception $ex){
            print_r(show_error($message, $status_code, $heading = 'An Error Was Encountered'));
        }


    }

    function logout(){

        $this->session->sess_destroy();
        redirect('welcome');
    }


}
?>