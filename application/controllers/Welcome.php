<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	function __construct(){
		parent::__construct(); // needed when adding a constructor to a controller
		$this->data = array(
			'layoutmode' => $this->config->item('layoutconfigdev')
		);
	}

	public function index()
	{
		//basic info for the header
		$layout_data['pageTitle'] = "Login";
		$layout_data['meta_description'] = "Login";
		$layout_data['meta_keywords'] = "ragamix_admin,Login";
		$layout_data['meta_classification'] = "home";
		$body_data[] = '';
		$layout_data['content_body'] = $this->load->view('login/dologin', $body_data, true);
//		print_r($layout_data);exit();
		$this->load->view($this->data['layoutmode'], $layout_data);
	}
}
