
<?php foreach($addfeaturedprof as $val): ?>

    <div class='getSystemUserVal' id="<?php echo $val->event_ad_id; ?>">

        <input type='hidden' id='event_id<?php echo $val->event_ad_id; ?>' value='<?php echo $val->event_ad_id; ?>' />
        <input type='hidden' id='event_title<?php echo $val->event_ad_id; ?>' value='<?php echo $val->event_title; ?>' />
        <input type='hidden' id='event_city<?php echo $val->event_ad_id; ?>' value='<?php echo $val->event_city; ?>' />
        <input type='hidden' id='event_date<?php echo $val->event_ad_id; ?>' value='<?php echo $val->event_date; ?>' />
        <input type='hidden' id='user_name<?php echo $val->event_ad_id; ?>' value='<?php echo $val->user_name; ?>' />
        <input type='hidden' id='rm_id<?php echo $val->rm_id; ?>' value='<?php echo $val->rm_id; ?>' />

        <div class="display_box">
            <div class="coverClass searchProfImgHolder">
                <img src="<?php echo $val->pic_url; ?>" class="pofimg" />
            </div>
            <div class="placehold" >
                <?php echo $val->event_title; ?>
                <span class="otlabtext">
                    Posted By:<?php echo $val->user_name; ?>
                </span>
            </div>

        </div>
    </div>
<?php endforeach; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".getSystemUserVal").click(function(){

            var element = $(this);
            var I = element.attr("id");
            var event_id = $("#event_id"+I).val();
            var event_title = $("#event_title"+I).val();
            var event_city = $("#event_city"+I).val();
            var event_date = $("#event_date"+I).val();
            var user_name = $("#user_name"+I).val();
            var rm_id = $("#rm_id"+I).val();


            $("#event_title_txtfld").val(event_title);
            $("#event_loc").val(event_city);
            $("#event_date").val(event_date);
            $("#posted_by_txtfld").val(user_name);


            $("#event_id_hidden").val(event_id);
            $("#event_title_hidden").val(event_title);
            $("#username_hidden_evnt").val(user_name);
            $("#rm_id_hidden").val(rm_id);

            /*Display the username inside searchbox*/
            $("#event_search").val(event_title);

            $("#displaysevents").hide();

        });
    });
</script>
