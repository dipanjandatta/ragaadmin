<table id="exampleajax" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <TH>Time</TH>
        <TH>Date</TH>
        <TH>Activity</TH>
        <TH>User ID</TH>
        <TH>Page</TH>
        <TH>User Type</TH>
        <TH>City</TH>
        <TH>Pincode</TH>
        <TH>Mail ID</TH>
        <TH>Mobile No</TH>
        <TH>Keyword</TH>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach($console_data as $val):
        ?>

        <tr>
            <td><?php echo substr($val->activity_at_time, 0, -3); ?></td>
            <td style="width: 7% !important;"><?php echo date('d M y', strtotime($val->activity_at_date)); ?></td>
            <?php if($val->activity_name == 'EVENT POSTED'){?>
                <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/event/<?php echo $val->event_ad_id ?>"><?php echo $val->activity_name?></a></td>
            <?php }
            elseif($val->activity_name == 'ADS POSTED'){?>
                <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/Ads/<?php echo $val->ad_id ?>"><?php echo $val->activity_name?></a></td>
            <?php }
            elseif($val->activity_name == 'SIGNUP'){?>
                <td style="width: 9% !important;"><a href="<?php echo $base_url_main ?>dashboard/activity_console/signup/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
            <?php }
            elseif($val->activity_name == 'LOGIN'){?>
                <td style="width: 9% !important;"><a href="<?php echo $base_url_main ?>dashboard/activity_console/login/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
            <?php }
            elseif($val->activity_name == 'UPDATE'){?>
                <?php if($val->page_name == 'event'){?>
                    <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/event/<?php echo $val->event_ad_id ?>"><?php echo $val->activity_name?></a></td>
                <?php }
                elseif($val->page_name == 'Ads'){?>
                    <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/Ads/<?php echo $val->ad_id ?>"><?php echo $val->activity_name?></a></td>
                <?php }
                else{?>
                    <td style="width: 9% !important;"><a href="<?php echo $base_url_main ?>profile/profileEdit/basic/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
                <?php } ?>
            <?php }
            else{?>
                <td style="width: 9% !important;"><?php echo $val->activity_name?></td>
            <?php }?>
            <td style="width: 9% !important;"><?php echo $val->user_name; ?></td>
            <td><?php echo $val->page_name; ?></td>
            <td><?php echo $val->user_category; ?></td>
            <td><?php echo $val->city_name; ?></td>
            <td><?php echo $val->pincode; ?></td>
            <td><?php echo $val->email_id; ?></td>
            <td><?php echo $val->mobile_no; ?></td>
            <td></td>
        </tr>

        <?php
    endforeach;
    ?>

    </tbody>
</table>


<script>

    $(document).ready(function() {
        $('#exampleajax').DataTable(
            {
                "aaSorting": [],
                "scrollY": "555px",
                "fixedHeader": {
                    "header": true
                },
                language : {
                    sLengthMenu: "Show _MENU_"
                },
                "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
                "sDom": 'lfiptB',
                buttons: [{
                    extend: 'excelHtml5',
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];

                        // Loop over the cells in column `F`
                        $('row c[r^="F"]', sheet).each( function () {
                            // Get the value and strip the non numeric characters
                            if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                                $(this).attr( 's', '20' );
                            }
                        });
                    }
                }]
            }
        );
    });

</script>
