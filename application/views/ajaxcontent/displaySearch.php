<div class="dispSearchLabel">
    Profiles
</div>
<?php foreach($searchListM as $val): ?>

    <div class='getSystemUserVal' id="<?php echo $val->rm_id; ?>">

        <input type='hidden' id='testname<?php echo $val->rm_id; ?>' value='<?php echo $val->user_name; ?>' />
        <input type='hidden' id='user_category<?php echo $val->rm_id; ?>' value='<?php echo $val->user_category; ?>' />
        <input type='hidden' id='full_name<?php echo $val->rm_id; ?>' value='<?php echo $val->full_name; ?>' />
        <input type='hidden' id='city_name<?php echo $val->rm_id; ?>' value='<?php echo $val->city_name; ?>' />
        <input type='hidden' id='rm_id<?php echo $val->rm_id; ?>' value='<?php echo $val->rm_id; ?>' />

        <div class="display_box">
            <div class="coverClass searchProfImgHolder">
                <img src="<?php echo $val->profile_pic_url; ?>" class="pofimg" />
            </div>
            <div class="placehold" >
                <?php echo $val->user_name; ?>
                <span class="otlabtext">
                    <?php echo $val->user_category; ?>, <?php echo $val->city_name; ?>
                </span>
            </div>

        </div>
    </div>
<?php endforeach; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".getSystemUserVal").click(function(){

            var element = $(this);
            var I = element.attr("id");
            var username = $("#testname"+I).val();
            var user_category = $("#user_category"+I).val();
            var full_name = $("#full_name"+I).val();
            var city_name = $("#city_name"+I).val();
            var rm_id = $("#rm_id"+I).val();

            $("#txtcliq").val(username);
            $("#category").val(user_category);
            $("#fullname").val(full_name);
            $("#cityname").val(city_name);

            $("#txtcliq_hidden").val(username);
            $("#rm_id").val(rm_id);

            /*Display the username inside searchbox*/
            $("#search").val(username);

            //Display in seeking
            $("#rm_id_take").val(rm_id);
            $("#full_name_seek").val(full_name);
            $("#search1").val(username);

            $("#displays").hide();

        });
    });
</script>
