<?php echo form_open('profile/searchTop'); ?>
<div class="dd-select">
    <i class="fa fa-map-marker smap" style="color:#fa6e59;"></i>
    <input type="text" name="locname" class="typeahead" placeholder="Location" />
</div>
<div class="input-group">
    <input type="text" name="searchbox" class="search bigInput cookiesearch" id="searchbox" placeholder="Search Ragamix" autocomplete="off" style="width: 425px !important;" /><!-- Change inline in next commit-->
    <span class="btnicon">
        <input type="submit" name="submit" value="Search" class="sbtn ssbtn"/>
    </span>
</div>
<?php echo form_close(); ?>
<div class="recentsearched">
    <div class="dispSearchLabel">
        You Recently Searched for
    </div>
    <?php echo form_open('profile/searchTop/1'); ?>
    <span class="iconer" style="float: none !important;"><i class="fa fa-search fafix"></i></span><input type="text" id="cookiebox" name="searchbox" class="stbox" />
    <input type="submit" name="cooksubmit" value="Search Again" class="cooksb sbtn" />
    <?php echo form_close(); ?>
    <div id="display" class="disp"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".cookiesearch").focus(function(){
            $('.recentsearched').show();
            var cn = Cookies.get('name');
            $('#cookiebox').val(cn);
        });
               
        $('.recentsearched').hide();
       
        $(".search").keyup(function()
        {
            
            var min_length = 0;
            $('#display').removeClass('disp');
            var searchbox = $(this).val();
            Cookies.set('name', searchbox);
            var dataString = 'searchword='+ searchbox;
            if(searchbox.length > min_length)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>profile/search",
                    data: dataString,
                    cache: false,
                    beforeSend: function(){
                        $('#display').html("<img src='<?php echo base_url(); ?>images/loading.gif' align='absmiddle' />");
                    },
                    success: function(html)
                    {
                        $("#display").html(html).show();
                    }
                });
            }
            else
            {
                $("#display").hide();
            }return false; 
        }); 
        
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        var states = [<?php foreach($cityName as $val): ?>"<?php echo $val->city; ?>", <?php endforeach; ?>];

        $('.typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength: 0
            },
            {
                name: 'states',
                source: substringMatcher(states)
            });
    });
</script>