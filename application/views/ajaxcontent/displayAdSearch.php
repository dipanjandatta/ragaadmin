
<?php foreach($addfeaturedad as $val): ?>

    <div class='getSystemUserVal' id="<?php echo $val->ad_id; ?>">

        <input type='hidden' id='ad_id<?php echo $val->ad_id; ?>' value='<?php echo $val->ad_id; ?>' />
        <input type='hidden' id='ad_title<?php echo $val->ad_id; ?>' value='<?php echo $val->ads_title; ?>' />
        <input type='hidden' id='ad_posted_on<?php echo $val->ad_id; ?>' value='<?php echo $val->created_on; ?>' />
        <input type='hidden' id='user_name<?php echo $val->ad_id; ?>' value='<?php echo $val->user_name; ?>' />
        <input type='hidden' id='rm_id<?php echo $val->ad_id; ?>' value='<?php echo $val->rm_id; ?>' />

        <div class="display_box">
            <div class="coverClass searchProfImgHolder">
                <img src="<?php echo $val->pic_url; ?>" class="pofimg" />
            </div>
            <div class="placehold" >
                <?php echo $val->ads_title; ?>
                <span class="otlabtext">
                    Posted By:<?php echo $val->user_name; ?>
                </span>
            </div>

        </div>
    </div>
<?php endforeach; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".getSystemUserVal").click(function(){

            var element = $(this);
            var I = element.attr("id");
            var ad_id = $("#ad_id"+I).val();
            var ad_title = $("#ad_title"+I).val();
            var ad_posted_on = $("#ad_posted_on"+I).val();
            var user_name = $("#user_name"+I).val();
            var rm_id = $("#rm_id"+I).val();


            $("#ad_title_txtfld").val(ad_title);
            $("#ad_posted_on").val(ad_posted_on);
            $("#posted_by_txtfld_ad").val(user_name);


            $("#ad_title_hidden").val(ad_title);
            $("#ad_id_hidden").val(ad_id);
            $("#username_hidden_ad").val(user_name);
            $("#rm_id_hidden").val(rm_id);

            /*Display the username inside searchbox*/
            $("#ad_search").val(ad_title);

            $("#displaysad").hide();

        });
    });
</script>
