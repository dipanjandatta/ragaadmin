<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            MENU LIST
        </div>
    </div>
    <div class="bordHell" style="width: 92px !important;"></div>
    <div class="col-md-12 noPad">
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        ACTIVITY CONSOLE
                    </div>
                    <div class="panel-body" id="columnTwo">

                        <li class="sidebar-item"><a href="<?php base_url();?>activity_console">Data Extraction & Excel</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        User Priviledge
                    </div>
                    <div class="panel-body" id="columnThree">
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/roles_rights">Roles and Right</a></li>
                        <li class="sidebar-item"><a href="">Subscription Status</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Featured Category
                    </div>
                    <div class="panel-body" id="columnFour">
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/featured_profile?sec=Homepage&type=0">Featured Profiles</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/featured_events?type=0">Featured Events</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/featured_ads?type=0">Featured Ads</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Preloaded data settings

                    </div>
                    <div class="panel-body" id="columnOne">
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/usertype">User Type/Category</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/stringinstrument">String Instrument</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/reedinstrument">Reed Instrument</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/windinstrument">Wind Instrument</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/genre">Genre</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/vocal">Vocal</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/location">Location</a></li>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-12 noPad">
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Content Controls
                    </div>
                    <div class="panel-body" id="columnSix">
                        <li class="sidebar-item"><a href="<?php echo base_url()?>admin/evlist?q=2&next=1&clicks=0&page_number=0">News/Events</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url(); ?>admin/profileList?q=1&next=1&clicks=0&page_number=0">Profiles</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url(); ?>admin/adlist?q=2&next=1&clicks=0&page_number=0">Ads</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/testimonials">Testimonials</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/slidercontrol?slid_type=0&app_type=0">Slider Control</a></li>
                        <li class="sidebar-item"><a href="<?php echo base_url();?>admin/seeking">Seeking</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Search Settings
                    </div>
                    <div class="panel-body" id="columnSeven">
                        <li class="sidebar-item"><a href="">List Editing</a></li>
                        <li class="sidebar-item"><a href="">Keyword Tagging</a></li>
                        <li class="sidebar-item"><a href="">Keyword Indexing</a></li>
                        <li class="sidebar-item"><a href="">Search Pattern Settings</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Message Controls
                    </div>
                    <div class="panel-body" id="columnEight">
                        <li class="sidebar-item"><a href="">Mailing list extraction/generation</a></li>
                        <li class="sidebar-item"><a href="">Batch mail from template</a></li>
                        <li class="sidebar-item"><a href="">Batch sms</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Subscription Manager
                    </div>
                    <div class="panel-body" id="columnNine">
                        <li class="sidebar-item"><a href="">Rates and schemes</a></li>
                        <li class="sidebar-item"><a href="">Audit reports</a></li>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-12 noPad">
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Reports & Queries
                    </div>
                    <div class="panel-body" id="columnTen">
                        <li class="sidebar-item"><a href="">Profile matching-query based on seeking</a></li>
                        <li class="sidebar-item"><a href="">Mailing list extraction/generation</a></li>
                        <li class="sidebar-item"><a href="">Transactional reports</a></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Support Chat
                    </div>
                    <div class="panel-body" id="columnEleven">
                        <li class="sidebar-item"><a href="">Notification</a></li>
                        <li class="sidebar-item"><?php echo  anchor('http://www.ragamix.com/ragasupportchat/public/user.php?source=sadmin&un='.$this->session->userdata('user_name'), 'Inbox', array('target'=>'_blank')); ?></li>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        Batch Upload
                    </div>
                    <div class="panel-body" id="columnTwelve">
                        <li class="sidebar-item"><a href="">Update Facebook Count</a></li>
                        <li class="sidebar-item"><a href="">Update Youtube Count</a></li>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>