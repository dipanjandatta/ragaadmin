<div>
  <div class="container padTopBody">
    <div class="row">
      <div class="col-md-12 noPad">
        <div class="col-md-6 centerAlign noPad">
          <img src="<?php echo base_url();?>/images/ragamixlatest.png" class="subLogo" />&nbsp;
          <span><h3 style="color: white;margin-top: 0px;">RAGAMIX ADMIN PANEL</h3></span>
        </div>

        <?php echo form_open('Login/login');?>

        <div class="col-md-4 col-sm-offset-1">
          <div class="panel panel-default noBord">
            <div class="panel-heading panelLogin remwhitebord">
              Login Credentials
            </div>

            <?php
            if($this->session->flashdata('messageError') != "")
            {
              ?>

              <div class = "alert alert-warning" id="error-alert">Not a Valid Username & Password</div>

              <?php
            }
            ?>

            <div class="panel-body panCol" style="background: #D0D4D5 !important;border: none;">
              <div class="control-group form-group">
                <div class="controls">
                  <input type="text" name="username" class="form-control dhlControls" placeholder="Username">
                </div>
              </div>
              <div class="control-group form-group">
                <div class="controls">
                  <input type="password" name="password" class="form-control dhlControls" placeholder="Password">
                </div>
              </div>
              <div class="control-group form-group buttonAlignment">
                <button type="reset" class="btn btn-warning modwarn">CANCEL</button>

                <button type="submit" class="btn btn-success modsucc">SUBMIT</button>
              </div>
              <div class="control-group form-group">&nbsp;</div>
            </div>


          </div>
        </div>

        <?php echo form_close();?>

      </div>
    </div>
  </div>
</div>
