<div id="header">
    <div class="topheader">


        <div class="container">


            <!-- SIDEBAR -->
            <div data-sidebar="true" class="sidebar-trigger">
                <a class="sidebar-toggle" href="#">
                    <span class="sr-only">Sidebar toggle</span>
                    <i class="fa fa-sidebar-toggle"></i>
                </a>
                <div class="sidebar-wrapper sidebar-default">
                    <div class="sidebar-scroller">
                        <ul class="sidebar-menu">

                            <li class="sidebar-group" ><span ><a style="color: white !important;" href="<?php echo base_url();?>admin/dashboard">Menu List</a></span>
                                <!--                            <li class="sidebar-group" ><span ><a style="color: white !important;" href="--><?php //echo base_url();?><!--admin/logout">Logout</a></span>-->

                            <li class="sidebar-group"><span>Activity Console</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="<?php base_url();?>activity_console">Data Extraction & Excel</a></li>
                                </ul>
                            </li>

                            <li class="sidebar-group"><span>Featured Category</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/featured_profile?sec=Homepage&type=0">Featured Profiles</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/featured_events?type=0">Featured Events</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/featured_ads?type=0">Featured Ads</a></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Support Chat</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="">Notification</a></li>
                                    <li class="sidebar-item"><?php echo  anchor('http://www.ragamix.com/ragasupportchat/public/user.php?source=sadmin&un='.$this->session->userdata('user_name'), 'Inbox', array('target'=>'_blank')); ?></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Preloaded data settings</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/usertype">User Type/Category</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/stringinstrument">String Instrument</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/reedinstrument">Reed Instrument</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/windinstrument">Wind Instrument</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/genre">Genre</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/vocal">Vocal</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/location">Location</a></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Content Controls</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="<?php echo base_url()?>admin/evlist?q=2&next=1&clicks=0&page_number=0">News/Events</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url(); ?>admin/profileList?q=1&next=1&clicks=0&page_number=0">Profiles</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url(); ?>admin/adlist?q=2&next=1&clicks=0&page_number=0">Ads</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/testimonials">Testimonials</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/slidercontrol?slid_type=0&app_type=0">Slider Control</a></li>
                                    <li class="sidebar-item"><a href="<?php echo base_url();?>admin/seeking">Seeking</a></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Search Settings</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="">List Editing</a></li>
                                    <li class="sidebar-item"><a href="">Keyword Tagging</a></li>
                                    <li class="sidebar-item"><a href="">Keyword Indexing</a></li>
                                    <li class="sidebar-item"><a href="">Search Pattern Settings</a></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Message Controls</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="">Mailing list </a></li>
                                    <li class="sidebar-item"><a href="">Batch mail from template</a></li>
                                    <li class="sidebar-item"><a href="">Batch sms</a></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Subscription Manager</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="">Rates and schemes</a></li>
                                    <li class="sidebar-item"><a href="">Audit reports</a></li>
                                </ul>
                            </li>
                            <li class="sidebar-group"><span>Reports & Queries</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="">Profile matching-query based on seeking</a></li>
                                    <li class="sidebar-item"><a href="">Mailing list extraction/generation</a></li>
                                    <li class="sidebar-item"><a href="">Transactional reports</a></li>
                                </ul>
                            </li>

                            <li class="sidebar-group"><span>Batch Upload</span>
                                <ul class="sidebar-group-menu">
                                    <li class="sidebar-item"><a href="">Update Facebook Count</a></li>
                                    <li class="sidebar-item"><a href="">Update Youtube Count</a></li>
                                    <li class="sidebar-item"><a href="">Transactional reports</a></li>
                                </ul>
                            </li>

                            <li class="sidebar-group"><a href="<?php echo base_url(); ?>Login/logout">Logout</a></li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
