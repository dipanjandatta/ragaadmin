<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta name="description" content="<?= $meta_description ?>"><meta name="keywords" content="<?= $meta_keywords ?>"><meta http-equiv="expires" content="0"><meta name="classification" content="<?= $meta_classification ?>"><meta name="Robots" content="index,follow"><meta name="revisit-after" content="2 Days"><meta name="language" content="en-us"><title><?= $pageTitle ?></title><link href="<?php echo base_url(); ?>css/compiled.min.css" rel="stylesheet" type="text/css" media="screen" title="default"><link href="<?php echo base_url(); ?>css/autocomplete-0.3.0.min.css" rel="stylesheet" type="text/css" media="screen" title="default"><link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css" type="text/css" media="screen"></head><?php
function full_flush() {
    ob_flush();
    flush();
}
?><body id="home"><div id="topheader"></div><div id="response"></div><?php if($this->uri->segment(2) == ""){?><div id="middle_column" class="row-md-12" style="background: #1b3f79; height:100%"><?php }else{ ?><div id="middle_column"><?php } ?><div id="overlay" class="animate"><div class="spinner"><div class="dot1"></div><div class="dot2"></div></div></div></div><?php if($this->uri->segment(2) == ""){?><?php }else{ ?><?php 
        $header_pagelet = $this->load->view('layouts/header', '', TRUE);
        $pageletone = array('id' => "topheader", "html" => $header_pagelet, "css" => "", "js" => "");
        ?><?php } ?><?php
    $pageletthree = array('id' => "middle_column", "html" => $content_body, "css" => "", "js" => "");
    ?><?php if ($this->session->flashdata('messageError') != '') { ?><script type="text/javascript">$(document).ready(function() {
                $.jGrowl("<?php echo $this->session->flashdata('messageError') ?>");
            });</script><?php } ?></div></body><script type="text/javascript" src="<?php echo base_url(); ?>js/autocomplete-0.3.0.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/main.min.js"></script><script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/hammer.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/dataTables.bootstrap.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/buttons.html5.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/dataTables.buttons.min.js"></script><script type="text/javascript" src="<?php echo base_url(); ?>js/jszip.min.js"></script><script>var pipe = new OpenPipe();</script><script>pipe.onPageletArrive(<?php echo json_encode($pageletone); ?>);</script><?php full_flush(); ?><script>pipe.onPageletArrive(<?php echo json_encode($pageletthree); ?>);</script><?php full_flush(); ?><script>pipe.close();</script><?php
if(isset($map)) {
    echo $map['js'];
}
else {
    ?><script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyAd1xMYT1bt99qtFWQEzXiRBvORDWHgPtk"></script><script>var map = new google.maps.Map(document.getElementById('googleMap'), {
            <?php if($this->session->userdata('userlat')) { ?>
            center: {lat: <?php echo $this->session->userdata('userlat'); ?>, lng: <?php echo $this->session->userdata('userlong'); ?>},
            <?php } else { ?>
            center: {lat: 22.5141379, lng: 88.3953893},
            <?php } ?>
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });


        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();
            //console.log(places);
            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            places.forEach(function(place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }

                document.getElementById("latlng").value = place.geometry.location;

            });
            map.fitBounds(bounds);
        });</script><?php
}
?></html>