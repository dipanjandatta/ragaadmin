<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            FEATURED ADS
        </div>
    </div>
    <div class="bordHell" style="width: 115px !important;"></div>
    <div class="col-md-12 noPad " >

        <?php
        if($this->session->flashdata('message') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
        }

        if($this->session->flashdata('messageremove') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo "Data Deleted Successfully"; ?>
            </div>
            <?php
        }
        ?>

        <?php echo form_open('Admin/deleteAds');?>

        <div class="popupdisp " id="modaldisp">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()"  class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
            <input type="hidden" id="hidval" name="hidval">
        </div>

        <?php echo form_close(); ?>

        <?php echo form_open('Admin/featured_ads_add');?>



        <div class="col-md-8 noPad">

            <div class="loader" id="loading" style="display: none"></div>

            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px;">
                <div class="col-md-2 noPad">
                    <select name="app_type" id="app_type" class="selclass" style="height: 26px; width: 134px; display: inline-block;" onchange="sectionlistads()">
                        <option value=0<?php if($_GET['type'] == 0){?> selected="selected" <?php }?>>Web Portal</option>
                        <option value=1<?php if($_GET['type'] == 1){?> selected="selected" <?php }?>>Mobile Portal</option>
                        <option value=2<?php if($_GET['type'] == 2){?> selected="selected" <?php }?>>Mobile App</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        FEATURED ADS
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" style="margin-bottom: 4px;">
                            <thead style="background: #2a6188!important;color: white;font-size: 11px;">
                            <tr>
                                <TH>Ads Title</TH>
                                <TH>Ads Date</TH>
                                <TH>Posted By</TH>
                                <TH>Position</TH>
                                <TH>Action</TH>
                            </tr>
                            </thead>
                            <tbody style="background: white;" id="listview">
                            <?php foreach($featuredadlist as $val):?>
                                <tr>
                                    <td ><?php echo $val->ads_title; ?></td>
                                    <td><?php echo date('d M y', strtotime($val->event_date)); ?></td>
                                    <td><?php echo $val->user_name; ?></td>
                                    <td><?php echo $val->position; ?></td>
                                    <td><div onclick="return delAds(<?php echo $val->featured_id; ?>)"><a style="cursor: pointer;color: #2a6188;">remove</a></div></td>
                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 noPad">

            <div class="col-md-12 noPad">
                <div class="col-md-12 noPad">
                    <div class="col-md-12 noPad" style="margin-bottom: 10px;margin-top: 10px;">

                        <div class="col-md-7" style="z-index: 99;">
                            <input type="text" id="ad_search" placeholder="Ads Title" onkeyup="adssearch()" class="form-control eilmlitecontrols more" />
                            <div id="displaysad" class="disp " style="display: block; z-index: 999999999;" ></div>

                        </div>
                        <!--                        <div class="col-md-3 noPad" >-->
                        <!--                            <input type="text" id="posted_by" placeholder="Posted By" class="form-control eilmlitecontrols more" />-->
                        <!--                        </div>-->
                        <!--                        <div class="col-md-1 noPad" >-->
                        <!--                            <button type="button" class="icon"><i class="fa fa-search" onclick="searchitem()"></i></button>-->
                        <!--                        </div>-->
                    </div>
                </div>

                <div class="col-md-12 pageoverlap">
                    <div class="panel panel-default panelFixer">
                        <div class="panel-heading panelBlue">
                            Ads Details
                        </div>
                        <div class="panel-body formPadder">

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Ads Title
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" name="ad_title_txtfld" id="ad_title_txtfld" class="form-control eilmlitecontrols more initdis" disabled>
                                    <input type="hidden" name="ad_title_hidden" id="ad_title_hidden" class="form-control eilmlitecontrols more initdis" >

                                    <input type="hidden" name="rm_id_hidden" id="rm_id_hidden" class="form-control eilmlitecontrols more initdis" />
                                    <input type="hidden" name="ad_id_hidden" id="ad_id_hidden" class="form-control eilmlitecontrols more initdis" />

                                </div>
                            </div>

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Ads Posted On
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="ad_posted_on" name="ad_posted_on" class="form-control eilmlitecontrols more" disabled>
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Posted By
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="posted_by_txtfld_ad" name="posted_by_txtfld_ad" class="form-control eilmlitecontrols more" disabled>
                                    <input type="hidden" id="username_hidden_ad" name="username_hidden_ad" class="form-control eilmlitecontrols more" >
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Position
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="positionadd" name="position" class="form-control eilmlitecontrols more" >
                                </div>
                            </div>

                            <div class="control-group form-group formFix" style="margin-top: 2px;">
                                <div class="col-md-4 padRgtFix padLeftFiver">
                                </div>

                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="reset" name="cancel" value="RESET" class="eilmbutton canclcls" id="btncancelrpd"/>
                                </div>
                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="add" value="ADD" onclick="return savevalidbtnfeaturedadd()" class="eilmbutton savecls" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo form_close(); ?>

    </div>
</div>



