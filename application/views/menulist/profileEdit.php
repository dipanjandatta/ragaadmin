<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-8 zindexer">
                <ul class="cc">
                    <?php if($this->uri->segment(3) == 'basic'){ ?>
                <li class="cc1 activec">
                <?php } else { ?>
                    <li class="cc1">
                        <?php } ?>
                        <?php if($this->uri->segment(4) == ''){?>
                            <?php echo anchor('profile/profileEdit/basic', '1', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Basic Profile
                            </span>
                        <?php }
                        else{?>
                            <?php echo anchor('profile/profileEdit/basic/'.$this->uri->segment(4), '1', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Basic Profile
                            </span>
                        <?php } ?>
                    </li>
                    <?php if($this->uri->segment(3) == 'music'){ ?>
                <li class="cc1 activec">
                <?php } else { ?>
                    <li class="cc1">
                        <?php } ?>
                        <?php if($this->uri->segment(4) == ''){?>
                            <?php echo anchor('profile/profileEdit/music', '2', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Music Profile
                            </span>
                        <?php }
                        else{?>
                            <?php echo anchor('profile/profileEdit/music/'.$this->uri->segment(4), '2', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Music Profile
                            </span>
                        <?php } ?>
                    </li>
                    <?php if($this->uri->segment(3) == 'photo'){ ?>
                <li class="cc1 activec">
                <?php } else { ?>
                    <li class="cc1">
                        <?php } ?>
                        <?php if($this->uri->segment(4) == ''){?>
                            <?php echo anchor('profile/profileEdit/photo', '3', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Photo Gallery
                            </span>
                        <?php }
                        else{?>
                            <?php echo anchor('profile/profileEdit/photo/'.$this->uri->segment(4), '3', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Photo Gallery
                            </span>
                        <?php } ?>
                    </li>
                    <?php if($this->uri->segment(3) == 'audiovideo'){ ?>
                <li class="cc1 activec">
                <?php } else { ?>
                    <li class="cc1">
                        <?php } ?>
                        <?php if($this->uri->segment(4) == ''){?>
                            <?php echo anchor('profile/profileEdit/audiovideo', '4', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Audio /Video
                            </span>
                        <?php }
                        else{?>
                            <?php echo anchor('profile/profileEdit/audiovideo/'.$this->uri->segment(4), '4', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Audio /Video
                            </span>
                        <?php }?>
                    </li>
                    <?php if($this->uri->segment(3) == 'musicresource'){ ?>
                <li class="cc1 activec">
                <?php } else { ?>
                    <li class="cc1">
                        <?php } ?>
                        <?php if($this->uri->segment(4) == ''){?>
                            <?php echo anchor('profile/profileEdit/musicresource', '5', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Seek Music Resource
                            </span>
                        <?php }
                        else{?>
                            <?php echo anchor('profile/profileEdit/musicresource/'.$this->uri->segment(4), '5', array('class'=>'circleLink')); ?>
                            <span class="pf">
                                Seek Music Resource
                            </span>
                        <?php }?>
                    </li>
                    <?php if($this->uri->segment(4) == ''){?>
                        <?php if($this->uri->segment(3) == 'showevents'){ ?>
                            <li class="cc1 activec">
                        <?php } else { ?>
                            <li class="cc1">
                        <?php } ?>
                        <!--                          --><?php //if($this->uri->segment(4) == ''){?>
                        <?php echo anchor('profile/profileEdit/showevents', '6', array('class'=>'circleLink')); ?>
                        <span class="pf">
                            Shows /Events
                        </span>

                        </li>
                    <?php }
                    else{?>

                    <?php } ?>

                    <?php if($this->uri->segment(4) == ''){?>
                        <?php if($this->uri->segment(3) == 'postads'){ ?>
                            <li class="cc1 activec">
                        <?php } else { ?>
                            <li class="cc1">
                        <?php } ?>
                        <!--                      --><?php //if($this->uri->segment(4) == ''){?>
                        <?php echo anchor('profile/profileEdit/postads', '7', array('class'=>'circleLink')); ?>
                        <span class="pf">
                            Post Ads
                        </span>

                        </li>
                    <?php }
                    else{?>

                    <?php } ?>

                </ul>
            </div>

        </div>
    </div>
    <?php 
    if($feeds == 0){
        ?>
        <?php
        $attr = array('id'=>'basic');
        echo form_open('profile/savebasic',$attr); 
        ?>

        <div class="col-md-4 forced" style="padding-right: 0 !important;">
            <div class="col-md-8" style="padding-right: 0 !important; text-align: right;">
                <?php echo anchor('profile/preview?t=ms&p=previewlink&basedirect='.base64_encode(current_url()),'PREVIEW', array('class'=>'btn btnfix yelbtn', 'style'=>'width: 40%;')); ?>
            </div>
            <div class="col-md-4 noPad" style="padding-right: 0 !important; text-align: right;">
                <button type="submit" class="btn btnfix greenbtn">SAVE & UPDATE</button>
            </div>
            <?php if($this->uri->segment(4) != 0){?>
                <div class="col-md-4 noPad" style="padding: 5px !important;text-align: left;margin-left: 220px;">
                    <?php echo anchor('dashboard/activity_console','Back to Dashboard'); ?>
                </div>
            <?php }
            else{?>

            <?php } ?>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12 noPad">
                <div class="col-md-4 noPad">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelMaroon">
                            Your Basic Profile
                        </div>
                        <div class="panel-body panFormer banban">
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" value="<?php echo $basicprofiledata[0]->user_name; ?>" class="form-control ragacontrols textFormControls" placeholder="Ragamix ID" disabled="disabled">
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix" style="margin-bottom: 15px !important;">
                                <div class="controls">
                                    <input type="text" name="email" value="<?php echo $basicprofiledata[0]->email; ?>" class="form-control ragacontrols textFormControls" placeholder="Email">
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">

                                <?php if($this->uri->segment(4) != ""){?>
                                    <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                <?php }
                                else{?>
                                    <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                <?php }?>
                                <!--                            <div class="col-md-3 padLftFix padRgtFormInline ragaformarFix">
                                <input type="text" class="form-control ragacontrols textFormControls" placeholder="+91 - IN" disabled>
                            </div>
                            <div class="col-md-9 padRgtFix padLftFormInline">-->
                                <div class="controls">
                                    <p>
                                        <input type="text" name="mobile_no" id="mobile_no" value="<?php echo $basicprofiledata[0]->mobile_no; ?>" class="form-control ragacontrols textFormControls" placeholder="Mobile Number">
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <!--                            <div class="col-md-3 padLftFix padRgtFormInline ragaformarFix">
                              <input type="text" class="form-control ragacontrols textFormControls" placeholder="+91 - IN" disabled>
                            </div>
                            <div class="col-md-9 padRgtFix padLftFormInline">-->
                                <div class="controls">
                                    <input type="text" name="mobile_no_alt" value="<?php echo $basicprofiledata[0]->mobile_no_alt; ?>" class="form-control ragacontrols textFormControls" placeholder="Alt Mobile Number">
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix" style="margin-bottom: 15px !important;">
                                <div class="controls">
                                    <p>
                                        <?php
                                        $selectedAttr = array($basicprofiledata[0]->country_name);
                                        $selId = 'class=selclass id=country_name onchange="getcitylist(this.options[this.selectedIndex].value);"';
                                        echo form_dropdown('country_name',$countryName,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="hidden" name="cv" id="cv" />
                                    <p>
                                        <?php
                                        $selectedAttr = array($basicprofiledata[0]->city_name);
                                        $selId = 'class=selclass id=city_name';
                                        echo form_dropdown('city_name',$cityName,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <input type="text" name="full_name" id="full_name" value="<?php echo $basicprofiledata[0]->full_name; ?>" class="form-control ragacontrols textFormControls" placeholder="Name">
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" name="age" value="<?php echo $basicprofiledata[0]->age; ?>" class="form-control ragacontrols textFormControls" placeholder="Age">
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" name="gender" value="<?php echo $basicprofiledata[0]->gender; ?>" class="form-control ragacontrols textFormControls" placeholder="Gender">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 noPad">
                    <div class="col-md-12">
                        <div class="panel panel-default noBord">
                            <div class="panel-heading panelMaroon">
                                Select Your Category
                            </div>
                            <div class="panel-body panFormer banban">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <p>
                                            <?php
                                            $selectedAttr = array($basicprofiledata[0]->user_category);
                                            $selId = 'class=selclass id=user_category';
                                            echo form_dropdown('usr_type',$userTypeList,$selectedAttr,$selId);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <?php
                                        $selectedAttrAdditional = array($basicprofiledata[0]->user_category_1);
                                        $prevVal = $basicprofiledata[0]->user_category;
                                        $selId = array(
                                            'class' => 'selclass',
                                            'id'       => 'user_category_1',
                                            'onChange' => 'showValid(this.value);'
                                        );
                                        echo form_dropdown('user_category_1',$userTypeListTwo,$selectedAttrAdditional,$selId);
                                        ?>
                                    </div>
                                    <!-- The Modal -->
                                    <div id="myModal" class="modal">
                                        <div class="modal-content">
                                            <span class="close">×</span>
                                            <p>Primary And Secondary Category cannot be same</p>
                                        </div>

                                    </div>



                                </div>
                                <div class="control-group form-group ragaformarFix">
                                    <div class="control-group form-group ragaformarFix">
                                        <input type="text" name="category_other" value="<?php echo $basicprofiledata[0]->category_other; ?>" class="form-control ragacontrols textFormControls" placeholder="Other Category">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default noBord">
                            <div class="panel-heading panelMaroon">
                                Social Media Contacts
                            </div>
                            <div class="panel-body panFormer banban">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <input type="text" name="facebook_url" value="<?php echo $basicprofiledata[0]->facebook_url; ?>" class="form-control ragacontrols textFormControls" placeholder="Facebook ID">
                                    </div>
                                </div>
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <input type="text" name="gmail_id" value="<?php echo $basicprofiledata[0]->gmail_id; ?>" class="form-control ragacontrols textFormControls" placeholder="Google ID">
                                    </div>
                                </div>
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <input type="text" name="twitter_id" value="<?php echo $basicprofiledata[0]->twitter_id; ?>" class="form-control ragacontrols textFormControls" placeholder="Twitter ID">
                                    </div>
                                </div>
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <input type="text" name="website_addr" value="<?php echo $basicprofiledata[0]->website_addr; ?>" class="form-control ragacontrols textFormControls" placeholder="Website">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 noPad">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelMaroon">
                            Location
                        </div>
                        <div class="panel-body panFormer banban">
                            <input id="pac-input" class="controlsMap" type="text" placeholder="Search Box">
                            <div id="googleMap" style="width: 400px; height: 200px;"></div>
                            <?php if($basicprofiledata[0]->loc_lat == '' || $basicprofiledata[0]->loc_lang == '') { ?>
                                <input type="text" name="userlatlng" id="latlng" />
                            <?php } else { ?>
                                <input type="text" name="userlatlng" id="latlng" value="<?php echo '('.$basicprofiledata[0]->loc_lat.','.$basicprofiledata[0]->loc_lang.')'; ?>" />
                            <?php } ?>    
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <select name="sc" class="selclass">
                                        <option value="">Studio Address</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <input type="text" name="address" id="address" value="<?php echo $basicprofiledata[0]->address; ?>" class="form-control ragacontrols textFormControls" placeholder="Address">
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" name="locality" value="<?php echo $basicprofiledata[0]->locality; ?>" class="form-control ragacontrols textFormControls" placeholder="Locality">
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <input type="text" name="pincode" id="pincode" value="<?php echo $basicprofiledata[0]->pincode; ?>" class="form-control ragacontrols textFormControls" placeholder="Pincode">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
        <?php echo form_close(); ?>
        <?php
    }
    if($feeds == 1){
        $musicattr = array('id'=>'musicprof');
        echo form_open('profile/saveMusicProfile',$musicattr);
        ?>
        <div class="col-md-4 forced" style="padding-right: 0 !important;">
            <div class="col-md-8" style="padding-right: 0 !important; text-align: right;">
                <?php echo anchor('profile/preview?t=ms&p=previewlink&basedirect='.base64_encode(current_url()),'PREVIEW', array('class'=>'btn btnfix yelbtn', 'style'=>'width: 40%;')); ?>
            </div>
            <div class="col-md-4 noPad" style="padding-right: 0 !important; text-align: right;">
                <button type="submit" class="btn btnfix greenbtn" onclick="return loadSubmit()">SAVE & UPDATE</button>
            </div>
            <?php if($this->uri->segment(4) != 0){?>
                <div class="col-md-4 noPad" style="padding: 5px !important;text-align: left;margin-left: 220px;">
                    <?php echo anchor('dashboard/activity_console','Back to Dashboard'); ?>
                </div>
            <?php }
            else{?>

            <?php } ?>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12 noPad">
                <div class="panel panel-default noBord">
                    <div class="panel-heading panelMaroon">
                        Music Profile Details
                    </div>
                    <div class="panel-body panCreamer banban noPad">
                        <div class="col-md-4" style="padding-top: 15px;">
                            <textarea name="user_desc" cols="55" rows="11" placeholder="About Yourself...Describe your music career achievements"><?php echo $musicdata[0]->user_desc; ?></textarea>
                        </div>

                        <?php if($this->uri->segment(4) != ""){?>
                            <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                        <?php }
                        else{?>
                            <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                        <?php }?>

                        <div class="col-md-2" style="padding-top: 15px;">
                            <span class="pedit fmarg ffm">Experience Details</span>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" name="experience_in_years" value="<?php echo $musicdata[0]->experience_in_years; ?>" class="form-control ragacontrols textFormControls" placeholder="Experience In years">
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <?php
                                        $options = array(
                                            '' => 'Studio or Band Manager',
                                            'studio' => 'Studio',
                                            'Band Manager' => 'Band Manager'
                                        );
                                        $selectedAttr = array($musicdata[0]->studio_band_member);
                                        $selId = 'class=selclass id=studio_band_member';
                                        echo form_dropdown('studio_band_member',$options,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <?php
                                        $options = array(
                                            '' => 'Commitment Level',
                                            'For Fun' => 'For Fun',
                                            'Moderately committed' => 'Moderately committed',
                                            'Dedicated' => 'Dedicated'
                                        );
                                        $selectedAttr = array($musicdata[0]->commitment_lvl);
                                        $selId = 'class=selclass id=commitment_lvl';
                                        echo form_dropdown('commitment_lvl',$options,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <?php
                                        $options = array(
                                            '' => 'Freelance Or Professional',
                                            'Freelance' => 'Freelance',
                                            'Professional' => 'Professional'
                                        );
                                        $selectedAttr = array($musicdata[0]->freelance_professional);
                                        $selId = 'class=selclass id=freelance_professional';
                                        echo form_dropdown('freelance_professional',$options,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" name="equipment_owned" value="<?php echo $musicdata[0]->equipment_owned; ?>" class="form-control ragacontrols textFormControls" placeholder="Equipment Owned">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="padding-top: 15px;">
                            <?php
                            //Exploding the value 
                            $genreExplode = explode(',', $musicdata[0]->genre);
                            ?>
                            <span class="pedit ffm">Genre / Speciality</span>
                            <div class="control-group form-group ragaformarFix">
                                <label class="forcedLabel">Primary</label>
                                <div class="controls">
                                    <p>
                                        <?php
                                        $selectedAttr = array($genreExplode[0]);
                                        $selId = 'class=selclass id=genre';
                                        echo form_dropdown('genre[]',$getGenre,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <label class="forcedLabel">Secondary</label>
                                <div class="controls">
                                    <p>
                                        <?php
                                        $selectedAttr = array($genreExplode[1]);
                                        $selId = 'class=selclass id=genre';
                                        echo form_dropdown('genre[]',$getGenre,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <label class="forcedLabel">Tertiary</label>
                                <div class="controls">
                                    <p>
                                        <?php
                                        $selectedAttr = array($genreExplode[2]);
                                        $selId = 'class=selclass id=genre';
                                        echo form_dropdown('genre[]',$getGenre,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls col-sm-12 noPad">
                                        <input type="text" name="genre_other" value="<?php echo $musicdata[0]->genre_other; ?>" class="form-control ragacontrols textFormControls" placeholder="Other Category">
                                    </div>
                                </div>

                            </div> 
                        </div>
                        <div class="col-md-3" style="    padding-top: 15px;">
                            <span class="pedit">Music Preferences</span>
                            <textarea name="music_influence_desc" cols="38" rows="10" placeholder="Type, Genres, Influences etc..."><?php echo $musicdata[0]->music_influence_desc; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 noPad">
                <div class="col-md-3 noPad">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelMaroon">
                            If a Musician
                        </div>
                        <div class="panel-body panFormer banban noPad">
                            <div class="col-md-6 minorPad">
                                <div class="control-group form-group ragaformarFix">
                                    <label class="forcedLabel">Select String Instruments</label>
                                    <div class="controls">

                                        <?php
                                        //Exploding Strings Instrument
                                        $stringsExplode = explode(',', $musicdata[0]->stringinstr);
                                        $selectedAttr = $stringsExplode;
                                        $selId = 'class=selclass id=strins';
                                        echo form_multiselect('strins[]',$getStrIns,$selectedAttr,$selId);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 minorPad">
                                <div class="control-group form-group ragaformarFix">
                                    <label class="forcedLabel">Select Reed Instruments</label>
                                    <div class="controls">

                                        <?php
                                        $reedExplode = explode(',', $musicdata[0]->reedinstr);
                                        $selectedAttr = $reedExplode;
                                        $selId = 'class=selclass id=reedinstr';
                                        echo form_multiselect('reedinstr[]',$getReedIns,$selectedAttr,$selId);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 minorPad">
                                <div class="control-group form-group ragaformarFix">
                                    <label class="forcedLabel">Select Percussions</label>
                                    <div class="controls">

                                        <?php
                                        $percussionExplode = explode(',', $musicdata[0]->percussions);
                                        $selectedAttr = $percussionExplode;
                                        $selId = 'class=selclass id=percussions';
                                        echo form_multiselect('percussions[]',$getPer,$selectedAttr,$selId);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 minorPad">
                                <div class="control-group form-group ragaformarFix">
                                    <label class="forcedLabel">Select Wind Instruments</label>
                                    <div class="controls">

                                        <?php
                                        $windExplode = explode(',', $musicdata[0]->windinstr);
                                        $selectedAttr = $windExplode;
                                        $selId = 'class=selclass id=windinstr';
                                        echo form_multiselect('windinstr[]',$getWindIns,$selectedAttr,$selId);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls col-sm-12 noPad">
                                        <input type="text" name="stringinstr_other" value="<?php echo $musicdata[0]->stringinstr_other; ?>" class="form-control ragacontrols textFormControls" placeholder="Other Instruments">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 noPad">
                    <div class="col-md-12">
                        <div class="panel panel-default noBord">
                            <div class="panel-heading panelMaroon">
                                If a Vocal Artist
                            </div>
                            <div class="panel-body panFormer banban">
                                <?php
                                $vocalsExplode = explode(',', $musicdata[0]->vocal);
                                ?>
                                <div class="control-group form-group ragaformarFix">
                                    <label class="forcedLabel">Primary Language</label>
                                    <div class="controls">
                                        <p>
                                            <?php
                                            $selectedAttr = array($vocalsExplode[0]);
                                            $selId = 'class=selclass id=vocal';
                                            echo form_dropdown('vocal[]',$getVocals,$selectedAttr,$selId);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="control-group form-group ragaformarFix">
                                    <label class="forcedLabel">Secondary Language</label>
                                    <div class="controls">
                                        <p>
                                            <?php
                                            $selectedAttr = array($vocalsExplode[1]);
                                            $selId = 'class=selclass id=vocaltwo';
                                            echo form_dropdown('vocal[]',$getVocals,$selectedAttr,$selId);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <!--                        <div class="control-group form-group ragaformarFix">
                              <div class="controls">
                                  <select name="intrument_secondary" class="selclass">
                                      <option value="">Type</option>
                                  </select>
                              </div>
                        </div>-->
                                <div class="control-group form-group ragaformarFix">
                                    <div class="control-group form-group ragaformarFix">
                                        <div class="controls col-sm-12 noPad">
                                            <input type="text" name="vocal_other" value="<?php echo $musicdata[0]->vocal_other; ?>" class="form-control ragacontrols textFormControls" placeholder="Other Languages">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 noPad" style="padding-right: 15px !important;">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelMaroon">
                            Band
                        </div>
                        <div class="panel-body panFormer banban">
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <?php
                                        $options = array(
                                            '' => 'Instrument / Equipment',
                                            'Instrument' => 'Instrument',
                                            'Equipment' => 'Equipment'
                                        );
                                        $selectedAttr = array($musicdata[0]->band_type);
                                        $selId = 'class=selclass id=band_type';
                                        echo form_dropdown('band_type',$options,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <textarea name="band_desc" cols="37" rows="5" placeholder="Member / Group / Other Details"><?php echo $musicdata[0]->band_desc; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 noPad">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelMaroon">
                            Educators
                        </div>
                        <div class="panel-body panFormer banban">
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <p>
                                        <?php
                                        $options = array(
                                            '' => 'Teacher / Training Institutes',
                                            'Teacher' => 'Teacher',
                                            'Training Institutes' => 'Training Institutes'
                                        );
                                        $selectedAttr = array($musicdata[0]->educator_type);
                                        $selId = 'class=selclass id=educator_type';
                                        echo form_dropdown('educator_type',$options,$selectedAttr,$selId);
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <textarea name="educator_desc" cols="39" rows="3" placeholder="Class / Course Details"><?php echo $musicdata[0]->educator_desc; ?></textarea>
                                </div>
                            </div>
                            <div class="control-group form-group ragaformarFix">
                                <div class="controls">
                                    <input type="text" name="educator_certification" value="<?php echo $musicdata[0]->educator_desc; ?>" class="form-control ragacontrols textFormControls" placeholder="Certifications If Any..">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo form_close();
    }
    if($feeds == 2){
        ?>
        <div class="col-md-4 forced" style="padding-right: 0 !important;">
            <div class="col-md-8" style="padding-right: 0 !important; text-align: right;">
                <?php echo anchor('profile/preview?t=ms&p=previewlink&basedirect='.base64_encode(current_url()),'PREVIEW', array('class'=>'btn btnfix yelbtn', 'style'=>'width: 40%;')); ?>
            </div>
            <?php if($this->uri->segment(4) != 0){?>
                <div class="col-md-4 noPad" style="padding: 5px !important;text-align: left;margin-left: 220px;">
                    <?php echo anchor('dashboard/activity_console','Back to Dashboard'); ?>
                </div>
            <?php }
            else{?>

            <?php } ?>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12 panFormer">
                <div class="col-md-3">
                    <div class="optionName">
                        Upload Image
                    </div>
                    <form id="uploadimage" action="<?php echo base_url(); ?>profile/ajaximage" method="post" enctype="multipart/form-data">
                        <?php if($this->uri->segment(4) != ""){?>
                            <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                        <?php }
                        else{?>
                            <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                        <?php }?>
                        <div id="image_preview"><img id="previewing" src="<?php echo base_url(); ?>images/noimage1.png" /></div>
                        <div id="selectImage">
                            <input type="file" class="custom-file-input" name="file" id="file" style="height: 33px;" required />
                            <button type="submit" value="Upload" class="submit btn btn-primary" style="width: 100%;" onclick="return loadSubmit()">Upload</button>
                        </div>                   
                    </form>

                </div>
                <div class="col-md-9 noPad">
                    <div class="optionName">
                        Uploaded Images
                    </div>
                    <div class="col-md-12">
                        <?php
                        if(empty($getPhotos)){
                            ?>
                            <div class="noNots">
                                No Images
                            </div>
                            <?php
                        }
                        else{
                            foreach($getPhotos as $val):
                                ?>
                                <div class="col-md-3 noPad stillBord">
                                    <div class="media">
                                        <div style="height:210px; max-width: 100%; background-color: #e9ebee;  background-position: center 30%; background-repeat: no-repeat; display: block; background-image: url('<?php echo $val->pic_url; ?>');"></div>
                                        <div class="media__body">
                                            <?php
                                            $str = str_replace($getCdnUrl,"",$val->pic_url);
                                            $conStr = explode('.',$str);
                                            echo anchor('profile/photoview/'.$val->photo_id.'/'.$conStr[0].'/'.$conStr[1], 'View', array('class'=>'vl majax')); ?><br /><br />
                                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#demo<?php echo $val->photo_id; ?>">Delete</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="demo<?php echo $val->photo_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h1 class="modal-title">Confirm Action</h1>
                                            </div>
                                            <?php echo form_open('profile/deletePhoto'); ?>
                                            <?php if($this->uri->segment(4) != ""){?>
                                                <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                            <?php }
                                            else{?>
                                                <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                            <?php }?>
                                            <div class="modal-body">
                                                Are You sure to delete this Photo ?
                                                <input type="hidden" name="photoid" value="<?php echo base64_encode($val->photo_id); ?>" />
                                                <input type="hidden" name="photourl" value="<?php echo base64_encode($val->pic_url); ?>" />
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger" onclick="return loadSubmit()">Yes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    if($feeds == 3){
        ?>
        <div class="col-md-4 forced" style="padding-right: 0 !important;">
            <div class="col-md-8" style="padding-right: 0 !important; text-align: right;">
                <?php echo anchor('profile/preview?t=ms&p=previewlink&basedirect='.base64_encode(current_url()),'PREVIEW', array('class'=>'btn btnfix yelbtn', 'style'=>'width: 40%;')); ?>
            </div>
            <?php if($this->uri->segment(4) != 0){?>
                <div class="col-md-4 noPad" style="padding: 5px !important;text-align: left;margin-left: 220px;">
                    <?php echo anchor('dashboard/activity_console','Back to Dashboard'); ?>
                </div>
            <?php }
            else{?>

            <?php } ?>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12 noPad">
                <div class="col-md-6">
                    <div class="col-md-12 noPad">
                        <div class="panel panel-default noBord">
                            <div class="panel-heading panelMaroon">
                                Audio Uploads
                            </div>
                            <div class="panel-body panFormer banban">
                                <form action="<?php echo base_url(); ?>profile/audioupload" method="post" enctype="multipart/form-data">

                                    <?php if($this->uri->segment(4) != ""){?>
                                        <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                    <?php }
                                    else{?>
                                        <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                    <?php }?>

                                    <div class="col-md-8 noPad">
                                        <div class="control-group form-group ragaformarFix">
                                            <div class="controls">
                                                <input type="file" name="file" id="file" style="height: 33px;" />
                                            </div>
                                        </div>
                                        <div class="control-group form-group ragaformarFix">
                                            <div class="controls">
                                                <textarea rows="3" placeholder="Add title / Brief Description" style="width: 100%;" name="title"></textarea>
                                            </div>
                                        </div>
                                        <div class="control-group form-group ragaformarFix">
                                            <button type="submit" class="btn btnfix hazyGreen smallWidth" onclick="return loadSubmit()">ADD AUDIO</button>
                                        </div>
                            
                                    </div>
                                </form>
                                <div class="col-md-4">&nbsp;</div>
                                <div class="col-md-12 noPad">
                                    <?php if(empty($getAllAudios)) { ?>
                                        <div class="noNots">
                                            No Audios Uploaded
                                        </div>
                                    <?php } else { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>TITLE</th>
                                                    <th>SIZE</th>
                                                    <th>UPLOADED</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($getAllAudios as $val): ?>
                                                    <tr>
                                                        <td>
                                                            <a href = "javascript:void(0)" onclick = "document.getElementById('player<?php echo $val->serial_no; ?>').play();document.getElementById('light<?php echo $val->serial_no; ?>').style.display='block';document.getElementById('fade<?php echo $val->serial_no; ?>').style.display='block'"><i class="fa fa-play"></i></a>&nbsp;&nbsp;<?php echo $val->audio_title; ?>
                                                            <div id="light<?php echo $val->serial_no; ?>" class="white_content">
                                                                <audio id="player<?php echo $val->serial_no; ?>" controls>
                                                                    <source src="<?php echo $val->audio_url; ?>" type="audio/mpeg">
                                                                    Your browser does not support the audio element.
                                                                </audio>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $kbSize = (($val->size)/1024);
                                                            $mbSize = $kbSize/1024;
                                                            echo round($mbSize,2)." Mb";
                                                            ?>
                                                        </td>
                                                        <td><?php echo date('d M Y', strtotime($val->updated_on)); ?></td>
                                                        <td><button type="button" class="noneBg" data-toggle="modal" data-target="#aud<?php echo $val->serial_no; ?>"><i class="fa fa-close"></i></button></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php foreach($getAllAudios as $kal): ?>
                                    <div class="modal fade" id="aud<?php echo $kal->serial_no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h1 class="modal-title">Confirm Action</h1>
                                                </div>
                                                <?php echo form_open('profile/deleteAudio'); ?>
                                                <?php if($this->uri->segment(4) != ""){?>
                                                    <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                                <?php }
                                                else{?>
                                                    <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                                <?php }?>
                                                <div class="modal-body">
                                                    Are You sure to delete this Audio ?
                                                    <input type="hidden" name="audioid" value="<?php echo base64_encode($kal->serial_no); ?>" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-danger" onclick="return loadSubmit()">Yes</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <!--                        <div class="col-md-12 notiText">-->
                                <!--                            You Have Exhausted your free upload limit-->
                                <!--                        </div>-->
                                <!--                        <div class="col-md-12 text-center">-->
                                <!--                            <button type="button" class="btn btn-primary btnBlueSmallMarg">SUBSCRIBE</button>-->
                                <!--                        </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12 noPad">
                        <div class="panel panel-default noBord">
                            <div class="panel-heading panelGrey">
                                Video Links
                            </div>
                            <div class="panel-body panFormer banban">
                                <?php echo form_open('profile/addVideo'); ?>
                                <?php if($this->uri->segment(4) != ""){?>
                                    <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                <?php }
                                else{?>
                                    <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                <?php }?>

                                <div class="col-md-6 noPad">
                                    <div class="control-group form-group ragaformarFix">
                                        <div class="controls">
                                            <input type="text" name="videourl" class="form-control ragacontrols textFormControls" placeholder="Paste youtube video Link" id="search-txt">
                                            <div id="video-data-1"></div>
                                            <input type="hidden" name="viewcounter" id="viewcounter" />
                                            <input type="hidden" name="likecounter" id="likecounter" />
                                            <input type="hidden" name="dislikescounter" id="dislikescounter" />
                                            <input type="hidden" name="yttile" id="yttile" />
                                            <input type="hidden" name="ytpicwidth" id="ytpicwidth" />
                                            <input type="hidden" name="ytpicheight" id="ytpicheight" />
                                        </div>
                                    </div>
                                    <div class="control-group form-group ragaformarFix">
                                        <div class="controls">
                                            <textarea rows="3" name="videotitle" placeholder="Add title / Brief Description" style="width: 100%;"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group form-group ragaformarFix">
                                        <button type="submit" class="btn btnfix hazyGreen smallWidth" id="search-btn" onclick="return loadSubmit()">ADD VIDEO</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                                <div class="col-md-4">&nbsp;</div>
                                <div class="col-md-12">
                                    <?php if(empty($getAllVideos)) { ?>
                                        <div class="noNots">
                                            No Videos Uploaded
                                        </div>
                                    <?php } else { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>TITLE</th>
                                                    <th>UPLOADED</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach($getAllVideos as $val):
                                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
                                                        $vid = $match[1];
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <iframe width="200" height="120" src="http://www.youtube.com/embed/<?php echo $vid; ?>" style="float: left; margin-right: 10px;" allowfullscreen></iframe>
                                                            <?php echo $val->video_desc; ?>
                                                        </td>
                                                        <td><?php echo date('d M Y', strtotime($val->updated_on)); ?></td>
                                                        <td><button type="button" class="noneBg" data-toggle="modal" data-target="#vid<?php echo $val->serial_no; ?>"><i class="fa fa-close"></i></button></td>
                                                    </tr>
                                                <?php endforeach ; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php foreach($getAllVideos as $bal): ?>
                                    <div class="modal fade" id="vid<?php echo $bal->serial_no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h1 class="modal-title">Confirm Action</h1>
                                                </div>
                                                <?php echo form_open('profile/deleteVideo'); ?>
                                                <?php if($this->uri->segment(4) != ""){?>
                                                    <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                                <?php }
                                                else{?>
                                                    <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                                <?php }?>
                                                <div class="modal-body">
                                                    Are You sure to delete this Video ?
                                                    <input type="hidden" name="vidid" value="<?php echo base64_encode($bal->serial_no); ?>" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-danger" onclick="return loadSubmit()">Yes</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <!--                        <div class="col-md-12 notiText">-->
                                <!--                            You Have Exhausted your free upload limit-->
                                <!--                        </div>-->
                                <!--                        <div class="col-md-12 text-center">-->
                                <!--                            <button type="button" class="btn btn-primary btnBlueSmallMarg">SUBSCRIBE</button>-->
                                <!--                        </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    if($feeds == 4){
        $attr = array('id'=>'seeker');
        if(empty($getReqs)) {
            echo form_open('profile/seekRequirement/1',$attr);
        }
        else{
            echo form_open('profile/seekRequirement/2',$attr);
        }
        ?>
        <div class="col-md-4 forced" style="padding-right: 0 !important;">
            <div class="col-md-8" style="padding-right: 0 !important; text-align: right;">
                <?php echo anchor('profile/preview?t=ms&p=previewlink&basedirect='.base64_encode(current_url()),'PREVIEW', array('class'=>'btn btnfix yelbtn', 'style'=>'width: 40%;')); ?>
            </div>
            <div class="col-md-4 noPad" style="padding-right: 0 !important; text-align: right;">
                <button type="submit" class="btn btnfix greenbtn">SAVE & UPDATE</button>
            </div>
            <?php if($this->uri->segment(4) != 0){?>
                <div class="col-md-4 noPad" style="padding: 5px !important;text-align: left;margin-left: 220px;">
                    <?php echo anchor('dashboard/activity_console','Back to Dashboard'); ?>
                </div>
            <?php }
            else{?>

            <?php } ?>
        </div>
        <div class="col-md-4 col-md-offset-4 noPad" style="margin-top: 20px;">
            <div class="panel panel-default noBord">
                <div class="panel-heading panelMaroon">
                    Post Your Requirement
                </div>
                <?php
                if(empty($getReqs)) {
                    ?>
                    <input type="hidden" name="serial_no_in" value="0" />
                    <?php
                }
                else {
                    ?>        
                    <input type="hidden" name="serial_no_in" value="<?php echo $getReqs[0]->serial_no; ?>" />
                    <?php    
                }
                ?>
                <div class="panel-body panFormer banban">
                    <div class="control-group form-group ragaformarFix">
                        <div class="controls">
                            <p>
                                <input type="text" name="keyword" id="keyword" value="<?php echo $getReqs[0]->keyword; ?>" class="form-control ragacontrols textFormControls" placeholder="Keywords">
                            </p>
                        </div>
                    </div>
                    <div class="control-group form-group ragaformarFix">
                        <div class="controls">
                            <p>
                                <input type="text" name="location" id="location" value="<?php echo $getReqs[0]->location; ?>" class="form-control ragacontrols textFormControls" placeholder="Location">
                            </p>
                        </div>
                    </div>
                    <div class="control-group form-group ragaformarFix">
                        <div class="controls">
                            <p>
                                <textarea rows="3" name="description" id="description" placeholder="Description" style="width: 100%;"><?php echo $getReqs[0]->description; ?></textarea>
                            </p>
                        </div>
                    </div>
                    <div class="control-group form-group ragaformarFix">
                        <div class="controls">
                            <p>
                                <input type="text" name="hashtags" id="hastags" value="<?php echo $getReqs[0]->hashtags; ?>" class="form-control ragacontrols textFormControls" placeholder="Hashtags #">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo form_close();
    }
    if($feeds == 5){
        ?>
        <div class="col-md-12 noPad" style="margin-top: 20px;">
            <div class="panel panel-default noBord">
                <div class="panel-heading panelLightOrange">
                    POST - Events, News, Reviews
                </div>
                <div class="panel-body panFry banban">
                    <?php 
                    $attrev = array('id'=>'eventsform','enctype'=>'multipart/form-data');
                    echo form_open('profile/addShowsEvents',$attrev);
                    ?>
                    <?php if($this->uri->segment(4) != ""){?>
                        <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                    <?php }
                    else{?>
                        <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                    <?php }?>

                    <div class="col-md-4 noPad">
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <p>
                                    <select name="category" class="selclass" id="category">
                                        <option value="">Select Type -- Events / News / Reviews</option>
                                        <option value="Events">Events</option>
                                        <option value="Newsbits">News</option>
                                        <option value="Reviews">Reviews</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-12 noPad">
                            <div class="col-md-4" style="padding-left: 0px !important;">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <input type="text" name="event_location" class="form-control ragacontrols textFormControls" placeholder="Venue / Location">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding-left: 0px !important;">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <select name="event_city" class="selclass" id="event_city">
                                            <option value="">City</option>
                                            <?php foreach($cityName as $val): ?>
                                                <option value="<?php echo $val->city; ?>"><?php echo $val->city; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 noPad">
                                <div class="control-group form-group ragaformarFix">
                                    <div class="controls">
                                        <p>
                                            <input type="text" id="event_date" name="event_date" class="form-control ragacontrols textFormControls" placeholder="Date of Event">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 noPad" style="margin-bottom: 10px;">
                                <?php echo $map['html']; ?>
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <p style="margin-top: 265px !important;">
                                    <input type="text" id="event_title" name="event_title" class="form-control ragacontrols textFormControls" placeholder="Heading">
                                </p>
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <textarea rows="3" name="event_desc" placeholder="Details" style="width: 100%;"></textarea>
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="text" name="doc_youtube_url" class="form-control ragacontrols textFormControls" placeholder="Add Youtube Link">
                            </div>
                        </div>
                        <div class="col-md-12 noPad">
                            <div class="col-md-4 noPad">
                                Upload Picture
                            </div>
                            <div class="col-md-8 noPad">                            
                                <div id="image_preview"><img id="previewing" src="<?php echo base_url(); ?>images/noimage1.png" /></div>	 
                                <div id="selectImage">
                                    <input type="file" class="custom-file-input" name="file" id="file" style="height: 33px;" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 noPad">
                            <div class="col-md-6">
                                <button type="submit" class="btn btnfix yelbtn" style="width: 100%;">PREVIEW</button>
                            </div>
                            <?php if($this->uri->segment(4) != 0){?>
                                <div class="col-md-4 noPad" style="padding: 5px !important;text-align: left;margin-left: 220px;">
                                    <?php echo anchor('dashboard/activity_console','Back to Dashboard'); ?>
                                </div>
                            <?php }
                            else{?>

                            <?php } ?>
                            <div class="col-md-6">
                                <button type="submit" class="btn btnfix greenbtn" style="width: 100%;">POST EVENT</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div class="col-md-8" style="padding-right: 0 !important;">
                        <div class="panel panel-default noBord">
                            <div class="panel-heading panelMaroon">
                                POSTED EVENTS
                            </div>
                            <div class="panel-body panFry banban noPad">
                                <div class="cont list">
                                    <div class="cardRow">
                                        <?php if(empty($getShowsEvents)) { ?>
                                            <div class="noNots">
                                                No Shows / Events To List
                                            </div>
                                        <?php } else {
                                            foreach($getShowsEvents as $val):
                                                ?>
                                                <div class="card">
                                                    <div class="title">
                                                        <h2 class="ng-binding"><?php echo $val->event_title; ?>@<?php echo $val->event_location; ?></h2>
                                                        <!--<span class="ng-binding">Following: 21,456</span>-->
                                                    </div>
                                                    <div class="title">
                                                        <h2 class="ng-binding">
                                                            <?php echo date('d M Y', strtotime($val->event_date)); ?>
                                                        </h2>
                                                    </div>
                                                    <div class="title">
                                                        <h2 class="ng-binding"><?php echo $val->event_location; ?></h2>
                                                        <span class="ng-binding"><?php echo $val->event_city; ?></span>
                                                    </div>
                                                    <div class="title">
                                                        <?php if($val->event_approved == 1) { ?>
                                                            <h2 class="ng-binding">Approved</h2>
                                                        <?php } else { ?>
                                                            <h2 class="ng-binding">Pending</h2>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="buttons">
                                                        <?php echo anchor('profile/editEvents/'.base64_encode($val->event_ad_id),'<i class="fa fa-edit"></i>', array('class' => 'rajax')); ?>&nbsp;<a data-toggle="modal" data-target="#ev<?php echo $val->event_ad_id; ?>" style="cursor:pointer"><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="ev<?php echo $val->event_ad_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h1 class="modal-title">Confirm Action</h1>
                                                            </div>
                                                            <?php echo form_open('profile/deleteEvents'); ?>

                                                            <?php if($this->uri->segment(4) != ""){?>
                                                                <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                                                            <?php }
                                                            else{?>
                                                                <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                                                            <?php }?>
                                                            <div class="modal-body">
                                                                Are You sure to delete this Event ?
                                                                <input type="hidden" name="evid" value="<?php echo base64_encode($val->event_ad_id); ?>" />
                                                                <input type="hidden" name="photoid" value="<?php echo base64_encode($val->photo_id); ?>" />
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-danger" onclick="return loadSubmit()">Yes</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                            <?php echo form_close(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    if($feeds == 6){
        ?>
        <div class="col-md-12 noPad" style="margin-top: 20px;">
            <div class="col-md-4">
                <div class="panel panel-default noBord">
                    <div class="panel-heading panelLightOrange">
                        POST -Ads
                    </div>
                    <?php
                    $attr = array('id'=>'adsform','enctype'=>'multipart/form-data');
                    echo form_open('profile/addAds',$attr);
                    ?>

                    <?php if($this->uri->segment(4) != ""){?>
                        <input type="hidden" size="100" name="hiddenval" value="<?php echo $this->uri->segment(4)?>"/>
                    <?php }
                    else{?>
                        <input type="hidden" name="hiddenval" size="100" value="<?php echo $this->session->userdata('user_id')?>"/>
                    <?php }?>
                    <div class="panel-body panFry banban">
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <p>
                                    <select name="category" class="selclass" id="category">
                                        <option value="">Type - Offer, Buy, Sell, Wanted</option>
                                        <option value="Offers">Offer</option>
                                        <option value="Buy/Sell">Buy/Sell</option>
                                        <option value="Wanted">Wanted</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <p>
                                    <input type="text" name="ads_title" id="ads_title" class="form-control ragacontrols textFormControls" placeholder="Heading">
                                </p>
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <p>
                                    <textarea rows="3" name="ads_desc" id="ads_desc" placeholder="Details" style="width: 100%;"></textarea>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-12 noPad">
                            <div class="col-md-4 noPad">
                                Upload Picture
                            </div>
                            <div class="col-md-8 noPad">
                                <div id="image_preview"><img id="previewing" src="<?php echo base_url(); ?>images/noimage1.png" /></div>	 
                                <div id="selectImage">
                                    <input type="file" class="custom-file-input" name="file" id="file" style="height: 33px;" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 noPad">
                            <div class="col-md-6">
                                <button type="submit" class="btn btnfix yelbtn" style="width: 100%;">PREVIEW</button>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btnfix greenbtn" style="width: 100%;">POST AD</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="col-md-8">
                <div class="table-responsive">
                    <?php if(empty($getAds)) { ?>
                        <div class="noNots">
                            No Ads Posted
                        </div>
                    <?php } else { ?>
                        <table class="table table-striped tabmarun">
                            <thead>
                            <tr>
                                <th class="wf">Posted Ads</th>
                                <th class="wf">Status</th>
                                <th class="wf">Ad Title</th>
                                <th class="wf">Views</th>
                                <th class="wf">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($getAds as $val): ?>  
                                <tr>
                                    <td style="width: 30%;"><?php echo date('d M Y',strtotime($val->created_on)); ?> <img src="<?php echo $val->pic_url; ?>" style="max-width: 60%;" /></td>
                                    <td>
                                        <?php if($val->ads_approved == 1) { ?>
                                            Approved
                                        <?php } else { ?>
                                            Pending
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $val->ads_desc; ?></td>
                                    <td><?php echo $val->ads_views; ?></td>
                                    <td><button type="submit" class="btn btnfix yelbtn" style="width: 80%;">Disable Ad</button>&nbsp;<i class="fa fa-close" style="float: right; margin-top: 10px;"></i></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div style="visibility:hidden;" id="loader-wrapper">
    <div id="loader"></div>
</div>