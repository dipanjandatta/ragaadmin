<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            ROLES & RIGHTS
        </div>
    </div>
    <div class="bordHell" style="width: 146px !important;"></div>
    <div class="col-md-12 noPad" >
        <div class="col-md-8 noPad">

            <div class="loader" id="loading" style="display: none"></div>

            <?php
            if($this->session->flashdata('messageError') != "")
            {
                ?>

                <div class = "loadingnewloader" id="error-alert"><?php echo $this->session->flashdata('messageError');?></div>
                <?php
            }
            ?>

            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px;">
                <div class="col-md-3">
                    <select id="section" class="selclass" style="height: 26px; display: inline-block;" onchange="rolesrightschanges()">
                        <option >Select A Group</option>

                        <?php foreach($rolesrightdata as $val):?>
                            <option value="<?php echo $val->group_id?>"<?php if( $initialroles == "Admin"){?> selected="selected" <?php }?>><?php echo $val->group_name?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        ROLES & RIGHTS
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" style="margin-bottom: 4px;">
                            <thead style="background: #2a6188!important;color: white;font-size: 11px;">
                            <tr>
                                <TH>User Name</TH>
                            </tr>
                            </thead>
                            <tbody style="background: white;" id="listview">
                            <?php foreach($roles as $val):?>
                                <tr>
                                    <td ><?php echo $val->user_name; ?></td>
                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 noPad">


            <?php echo form_open('admin/featured_profile_add');?>
            <div class="col-md-12 noPad">
                <div class="col-md-12 noPad">
                    <div class="col-md-12 noPad" style="margin-bottom: 10px;margin-top: 10px;">
                        <div class="col-md-3">
                            <select id="locationname" class="selclass" style="height: 26px; width: 88px; display: inline-block;" >
                                <option value="">Location</option>
                                <?php foreach($cityName as $val):?>
                                    <option value="<?php echo $val->city; ?>"><?php echo $val->city; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select id="usercat" class="selclass" style="height: 26px; width: 88px; display: inline-block;" >
                                <option value="">Categories</option>
                                <option value="Musician">Musicians</option>
                                <option value="Singer">Singers</option>
                                <option value="Band">Bands</option>
                            </select>
                        </div>

                        <div class="col-md-6" style="z-index: 99;">
                            <input type="text" id="search" placeholder="Search" class="form-control eilmlitecontrols more " >
                            <div id="displays" class="disp " style="display: block; z-index: 999999999;" ></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 pageoverlap">
                    <div class="panel panel-default panelFixer">
                        <div class="panel-heading panelBlue">
                            Profile Details
                        </div>
                        <div class="panel-body formPadder">

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    User Name
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" name="txtcliq" id="txtcliq" class="form-control eilmlitecontrols more initdis" disabled>
                                    <input type="hidden" name="txtcliq_hidden" id="txtcliq_hidden" class="form-control eilmlitecontrols more initdis" />

                                </div>

                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Category
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="category" class="form-control eilmlitecontrols more" disabled>
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Position
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" name="position" class="form-control eilmlitecontrols more" >
                                    <input type="hidden" name="rm_id" id="rm_id" class="form-control eilmlitecontrols more" >

                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Full Name
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="fullname" class="form-control eilmlitecontrols more" disabled>
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    City
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="cityname" class="form-control eilmlitecontrols more" disabled>
                                </div>
                            </div>

                            <div class="control-group form-group formFix" style="margin-top: 2px;">
                                <div class="col-md-4 padRgtFix padLeftFiver">
                                    <!--								<input type="submit" name="cancel" value="CANCEL" class="eilmbutton" disabled/>-->
                                </div>

                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="reset" name="cancel" value="CANCEL" class="eilmbutton canclcls" id="btncancelrpd"/>
                                </div>
                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="add" value="ADD" class="eilmbutton savecls" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php form_close();?>
        </div>
    </div>
</div>



