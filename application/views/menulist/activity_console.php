<div class="container stanPad">
    <div class="col-md-12 noPad spAtt">
        <div class="col-md-5 noPad">
            <?php echo form_open('admin/activity_console')?>
            <div class="largetext center boldtext" style="color: white;">From
                <input id="from_date" class="form-control" type="date" name="from_date" value="<?php echo set_value('from_date')?>"  style="height: 30px; width: 165px; display: inline-block;">
                To <input id="to_date" class="form-control" style="height: 30px; width: 165px; display: inline-block;" type="date" value="<?php echo set_value('to_date')?>"  name="to_date">
                <input class="btn btn-red btn-radius" style="height: 30px;background: #013a65;color: white;    width: 30px;padding: 0;" type="submit" name="go" value="GO" onclick="return seldatechnge()" >
            </div>
            <?php echo form_close();?>
        </div>
        <div class="col-md-4">
            <select id="mySelect" onchange="seltimechnge()" name="mySelect" class="form-control" style="height: 30px; width: 165px; display: inline-block; padding: 3px;" >
                <option >Select Time Interval</option>
                <option value="0">All</option>
                <option value="1"<?php if ($time_interval == '1') echo ' selected="selected"'; ?>>Last 1hrs</option>
                <option value="2"<?php if ($time_interval == '2') echo ' selected="selected"'; ?>>Last 2hrs</option>
                <option value="4"<?php if ($time_interval == '4') echo ' selected="selected"'; ?>>Last 4hrs</option>
                <option value="8"<?php if ($time_interval == '8') echo ' selected="selected"'; ?>>Last 8hrs</option>
                <option value="24"<?php if ($time_interval == '24') echo ' selected="selected"'; ?>>Last 24hrs</option>
            </select>
        </div>
    </div>

    <div class="col-md-12 noPad">
        <div class="col-md-12">
            <div id="showingalert" class = "alert alert-success" style="display: none">
                Please Select Date
            </div>

            <div class="loader" id="loading" style="display: none"></div>

            <div class="panel panel-default panelFixer">
                <div class="panel-heading panelBlue">
                    ACTIVITY CONSOLE
                </div>
                <div class="panel-body" style="padding-top: 5px !important;">
                    <div class="col-md-12 noPad" id="tableview">

                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <TH>Time</TH>
                                <TH>Date</TH>
                                <TH>Activity</TH>
                                <TH>User ID</TH>
                                <TH>Page</TH>
                                <TH>User Type</TH>
                                <TH>City</TH>
                                <TH>Pincode</TH>
                                <TH>Mail ID</TH>
                                <TH>Mobile No</TH>
                                <TH>Keyword</TH>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            foreach($console_data as $val):
                                ?>

                                <tr>
                                    <td><?php echo substr($val->activity_at_time, 0, -3); ?></td>
                                    <td style="width: 7% !important;"><?php echo date('d M y', strtotime($val->activity_at_date)); ?></td>
                                    <?php if($val->activity_name == 'EVENT POSTED'){?>
                                        <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/event/<?php echo $val->event_ad_id ?>"><?php echo $val->activity_name?></a></td>
                                    <?php }
                                    elseif($val->activity_name == 'ADS POSTED'){?>
                                        <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/Ads/<?php echo $val->ad_id ?>"><?php echo $val->activity_name?></a></td>
                                    <?php }
                                    elseif($val->activity_name == 'SIGNUP'){?>
                                        <td style="width: 9% !important;"><a href="<?php echo $base_url_main ?>dashboard/activity_console/signup/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
                                    <?php }
                                    elseif($val->activity_name == 'LOGIN'){?>
                                        <td style="width: 9% !important;"><a href="<?php echo $base_url_main ?>dashboard/activity_console/login/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
                                    <?php }
                                    elseif($val->activity_name == 'UPDATE'){?>
                                        <?php if($val->page_name == 'event'){?>
                                            <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/event/<?php echo $val->event_ad_id ?>"><?php echo $val->activity_name?></a></td>
                                        <?php }
                                        elseif($val->page_name == 'Ads'){?>
                                            <td style="width: 9% !important;"><a href="<?php echo base_url() ?>admin/Ads/<?php echo $val->ad_id ?>"><?php echo $val->activity_name?></a></td>
                                        <?php }
                                        else{?>
                                            <td style="width: 9% !important;"><a href="<?php echo $base_url_main ?>profile/profileEdit/basic/<?php echo $val->rm_id ?>"><?php echo $val->activity_name?></a></td>
                                        <?php } ?>
                                    <?php }
                                    else{?>
                                        <td style="width: 9% !important;"><?php echo $val->activity_name?></td>
                                    <?php }?>
                                    <td style="width: 9% !important;"><?php echo $val->user_name; ?></td>
                                    <td><?php echo $val->page_name; ?></td>
                                    <td><?php echo $val->user_category; ?></td>
                                    <td><?php echo $val->city_name; ?></td>
                                    <td><?php echo $val->pincode; ?></td>
                                    <td><?php echo $val->email_id; ?></td>
                                    <td><?php echo $val->mobile_no; ?></td>
                                    <td></td>
                                </tr>

                                <?php
                            endforeach;
                            ?>

                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <a id="counterCtrl" class="counterCss">
        <i class="fa fa-user-plus" style="font-size: 30px;"></i>
    </a>
    <div class="showCountr">
        <?php
        foreach($abc as $val):
            ?>

            <ul class='consoleul' style="list-style-type: none">
                <li style="padding: 5px;">Signups <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->signup_cnt?></span></li>
                <li style="padding: 5px;">Updates <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->update_cnt?></span></li>
                <li style="padding: 5px;">Uploads <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->upload_cnt?></span></li>
                <li style="padding: 5px;">Deletions <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->delete_cnt?></span></li>
                <li style="padding: 5px;">Ad Submit <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->adssubmit_cnt?></span></li>
                <li style="padding: 5px;">Event Submit <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->eventsubmit_cnt?></span></li>
                <li style="padding: 5px;">Messages Sent <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->msgsent_cnt?></span></li>
                <li style="padding: 5px;">Current Logins <span style="width: 25px;text-align: center;float: right; padding: 3px; background: #bbaa10; font-size: 15px; border-radius: 10px; color: white;"><?php echo $val->currlogin_cnt?></span></li>
            </ul>

            <?php
        endforeach;
        ?>
    </div>
</div>



