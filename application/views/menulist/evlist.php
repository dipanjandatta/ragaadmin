<div class="container">
    <div class="pagingholder stanPad">
        <div class="filterOps ">
            <a href="<?php echo base_url(); ?>admin/evlist?q=2&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">All</a>
            <a href="<?php echo base_url(); ?>admin/evlist?q=1&&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Approved</a>
            <a href="<?php echo base_url(); ?>admin/evlist?q=0&&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Unapproved</a>
        </div>
        <?php if($page_number > 0){ ?>
            <a href="<?php echo base_url(); ?>admin/evlist?q=<?php echo $q; ?>&next=<?php echo $next; ?>&clicks=<?php echo ($clicks-1); ?>&page_number=<?php echo ($page_number-1); ?>&prev=y">
                <i class="fa fa-chevron-left pageicon"></i>
            </a>
        <?php } ?>
        <a href="<?php echo base_url(); ?>admin/evlist?q=<?php echo $q; ?>&next=<?php echo $next; ?>&clicks=<?php echo $clicks; ?>&page_number=<?php echo $page_number; ?>">
            <i class="fa fa-chevron-right pageicon"></i>
        </a>
    </div>
    <?php
    foreach($getEventsList as $val):
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
            $vid = $match[1];
            $displayedYoutubeVideo = "<iframe width='240' height='165' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
        }
        ?>
        <a href="<?php echo base_url(); ?>admin/event/<?php echo $val->event_ad_id; ?>">
            <div class="col-md-2-3 noPad stillBord">
                <div class="demo-section k-content stillHeight">
                    <div class="coverClass blurim">
                        <?php if($val->doc_youtube_url != '') { ?>
                            <?php echo $displayedYoutubeVideo; ?>
                        <?php } else { ?>

                            <img src="<?php echo $val->pic_url;?>" class="newImg" />

                            <?php
                        }
                        ?>
                    </div>
                    <div class="title grey" style="padding-bottom: 0px !important;">
                        <?php echo $val->event_title; ?>
                    </div>
                    <div class="uploadedbystyle">
                        Uploaded By:<?php echo $val->uploaded_by; ?>
                        <div class="datestyle">Date:<?php echo date('d M y', strtotime($val->created_on)); ?></div>
                    </div>
                    <div class="stnComm greyblack">
                        <?php
                        if(strlen($val->event_desc) > 100) {
                            // truncate string
                            $stringCut = substr($val->event_desc, 0, 100);

                            // make sure it ends in a word so assassinate doesn't become ass...
                            $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                        }
                        else {
                            $string = $val->event_desc;
                        }
                        echo $string;
                        ?>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach; ?>
</div>