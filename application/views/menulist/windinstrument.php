<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            WIND INSTRUMENT
        </div>
    </div>
    <div class="bordHell" style="width: 138px !important;"></div>
    <div class="col-md-12 noPad" >

        <?php
        if($this->session->flashdata('message') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
        }

        if($this->session->flashdata('messageremove') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo "Data Deleted Successfully"; ?>
            </div>
            <?php
        }
        ?>

        <?php echo form_open('Admin/windinstr_del');?>
        <div class="popupdisp " id="modaldisp">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()"  class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
            <input type="hidden" id="hidval" name="hidval">
        </div>
        <?php echo form_close(); ?>


        <?php echo form_open('admin/windinstr_save'); ?>

        <div class="col-md-8 noPad">

            <div class="loader" id="loading" style="display: none"></div>

            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        WIND INSTRUMENT
                    </div>
                    <div class="panel-body">
                        <table id="windinstrdatatable" class="table table-bordered" style="margin-bottom: 4px;">
                            <thead style="background: #2a6188!important;color: white;font-size: 11px;">
                            <tr>
                                <TH>Wind Instrument</TH>
                                <TH>Edit</TH>
                                <TH>Delete</TH>
                            </tr>
                            </thead>
                            <tbody style="background: white;" id="listview">
                            <?php foreach($windinstrumentdata as $val):?>
                                <tr>
                                    <td ><?php echo $val->windinstr; ?></td>
                                    <td style="font-size: 16px !important;text-align: center"><div onclick="gettingwindinstrument('<?php echo $val->windinstr; ?>',<?php echo $val->windinstr_id; ?>)"><i class="fa fa-pencil-square-o" style="cursor: pointer;" aria-hidden="true"></div></i></td>
                                    <td style="font-size: 16px !important;text-align: center;"><a onclick="delAds(<?php echo $val->windinstr_id; ?>)"><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4 noPad">

            <div class="col-md-12 noPad">
                <div class="col-md-12 ">
                    <div class="panel panel-default panelFixer">
                        <div class="panel-heading panelBlue">
                            WindInstrument Details
                        </div>
                        <div class="panel-body formPadder">

                            <div class="control-group form-group formFix">
                                <div class="col-md-4 labelFix">
                                    Wind Instrument
                                </div>
                                <div class="col-md-8 padRgtFix padLeftFiver">
                                    <input type="hidden" name="windinstrid" id="windinstrumentid" class="form-control eilmlitecontrols more initdis">
                                    <input type="text" name="windinstr" id="windinstrumentdetails" class="form-control eilmlitecontrols more initdis">

                                </div>

                            </div>

                            <div class="control-group form-group formFix" style="margin-top: 2px;">
                                <div class="col-md-4 padRgtFix padLeftFiver">
                                </div>

                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="cancel" value="CANCEL" class="eilmbutton canclcls" id="btncancelrpd"/>
                                </div>
                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="save" value="SAVE" onclick="return savevalidbtnwindinstr()"  class="eilmbutton savecls" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>
</div>



