<div class="container stanPad">
	<div class="pageTitle">
		<div class="acPageTile">
			FEATURED PROFILES
		</div>
	</div>
	<div class="bordHell" style="width: 146px !important;"></div>
	<div class="col-md-12 noPad" >

		<?php
		if($this->session->flashdata('message') != "")
		{
			?>
			<div id="notification" style="display: none;">
				<?php echo $this->session->flashdata('message'); ?>
			</div>
			<?php
		}

		if($this->session->flashdata('messageremove') != "")
		{
			?>
			<div id="notification" style="display: none;">
				<?php echo $this->session->flashdata('messageremove'); ?>
			</div>
			<?php
		}
		?>

		<?php echo form_open('Admin/deleteProf');?>

		<div class="popupdisp " id="modaldisp">
			<div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()" class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
			<input type="hidden" id="hidval" name="hidval">
			<input type="hidden" id="hidsection" name="hidsection">
		</div>

		<?php echo form_close(); ?>


		<?php echo form_open('admin/featured_profile_add');?>

		<div class="col-md-8 noPad">

			<div class="loader" id="loading" style="display: none"></div>

			<div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px;">
				<div class="col-md-2 noPad">
					<select name="app_type" id="app_type" class="selclass" style="height: 26px; width: 134px; display: inline-block;" onchange="sectionlistchange()">
<!--						<option >Select An Application</option>-->
						<option value=0<?php if($_GET['type'] == 0){?> selected="selected" <?php }?>>Web Portal</option>
						<option value=1<?php if($_GET['type'] == 1){?> selected="selected" <?php }?>>Mobile Portal</option>
						<option value=2<?php if($_GET['type'] == 2){?> selected="selected" <?php }?>>Mobile App</option>
					</select>
				</div>
				<div class="col-md-2">
					<select id="section" name="section" class="selclass" style="height: 26px; width: 88px; display: inline-block;" onchange="sectionlistchange()">
						<option >Section</option>
						<option value="Homepage"<?php if($_GET['sec'] == "Homepage"){?> selected="selected" <?php }?>>Homepage</option>
						<option value="Musician"<?php if($_GET['sec'] == "Musician"){?> selected="selected" <?php }?>>Musicians</option>
						<option value="Singer"<?php if($_GET['sec'] == "Singer"){?> selected="selected" <?php }?>>Singers</option>
						<option value="Band"<?php if($_GET['sec'] == "Band"){?> selected="selected" <?php }?>>Bands</option>
					</select>
				</div>
			</div>
			<div class="col-md-12 noPad">
				<div class="panel panel-default panelFixer">
					<div class="panel-heading panelBlue">
						FEATURED PROFILES
					</div>
					<div class="panel-body">
						<table class="table table-bordered" style="margin-bottom: 4px;">
							<thead style="background: #2a6188!important;color: white;font-size: 11px;">
							<tr>
								<TH>User Name</TH>
								<TH>Full Name</TH>
								<TH>Category</TH>
								<TH>City</TH>
								<TH>Position</TH>
								<TH>Action</TH>
							</tr>
							</thead>
							<tbody style="background: white;" id="listview">
							<?php foreach($featuredlist as $val):?>
								<tr>
									<td ><?php echo $val->user_name; ?></td>
									<td><?php echo $val->full_name; ?></td>
									<td><?php echo $val->user_category; ?></td>
									<td><?php echo $val->city_name; ?></td>
									<td><?php echo $val->position; ?></td>
									<?php
									if($_GET['sec'] == 'Homepage'){ ?>
										<td><div onclick="delAds(<?php echo $val->featured_id; ?>)"><a style="cursor: pointer;color: #2a6188;">remove</a></div></td>
									<?php }else{ ?>
										<td><div onclick="delAds(<?php echo $val->serial_no; ?>)"><a style="cursor: pointer;color: #2a6188;">remove</a></div></td>
									<?php } ?>
								</tr>
							<?php endforeach;?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 noPad">
			<div class="col-md-12 noPad">

				<div class="col-md-12 noPad">
					<div class="col-md-12 noPad" style="margin-bottom: 10px;margin-top: 10px;">
						<div class="col-md-3">
							<select id="locationname" class="selclass" style="height: 26px; width: 88px; display: inline-block;" >
								<option value="">Location</option>
								<?php foreach($cityName as $val):?>
									<option value="<?php echo $val->city; ?>"><?php echo $val->city; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-3">
							<select id="usercat" class="selclass" style="height: 26px; width: 88px; display: inline-block;" >
<!--								<option value="">Categories</option>-->
								<option value="Musician">Musicians</option>
								<option value="Singer">Singers</option>
								<option value="Band">Bands</option>
							</select>
						</div>

						<div class="col-md-6" style="z-index: 99;">
							<input type="text" id="search" placeholder="Search" class="form-control eilmlitecontrols more " >
							<div id="displays" class="disp " style="display: block; z-index: 999999999;" ></div>
						</div>
					</div>
				</div>

				<div class="col-md-12 pageoverlap">
					<div class="panel panel-default panelFixer">
						<div class="panel-heading panelBlue">
							Profile Details
						</div>
						<div class="panel-body formPadder">

							<div class="control-group form-group formFix">
								<div class="col-md-3 labelFix">
									User Name
								</div>
								<div class="col-md-9 padRgtFix padLeftFiver">
									<input type="text" name="txtcliq" id="txtcliq" class="form-control eilmlitecontrols more initdis" disabled>
									<input type="hidden" name="txtcliq_hidden" id="txtcliq_hidden" class="form-control eilmlitecontrols more initdis" />

								</div>

							</div>
							<div class="control-group form-group formFix">
								<div class="col-md-3 labelFix">
									Category
								</div>
								<div class="col-md-9 padRgtFix padLeftFiver">
									<input type="text" id="category" class="form-control eilmlitecontrols more" disabled>
								</div>
							</div>

							<div class="control-group form-group formFix">
								<div class="col-md-3 labelFix">
									Full Name
								</div>
								<div class="col-md-9 padRgtFix padLeftFiver">
									<input type="text" id="fullname" class="form-control eilmlitecontrols more" disabled>
								</div>
							</div>
							<div class="control-group form-group formFix">
								<div class="col-md-3 labelFix">
									City
								</div>
								<div class="col-md-9 padRgtFix padLeftFiver">
									<input type="text" id="cityname" class="form-control eilmlitecontrols more" disabled>
								</div>
							</div>
							<div class="control-group form-group formFix">
								<div class="col-md-3 labelFix">
									Position
								</div>
								<div class="col-md-9 padRgtFix padLeftFiver">
									<input type="text" name="position" id="positionprof" class="form-control eilmlitecontrols more" >
									<input type="hidden" name="rm_id" id="rm_id" class="form-control eilmlitecontrols more" >
								</div>
							</div>

							<div class="control-group form-group formFix" style="margin-top: 2px;">
								<div class="col-md-4 padRgtFix padLeftFiver">
									<!--								<input type="submit" name="cancel" value="CANCEL" class="eilmbutton" disabled/>-->
								</div>

								<div class="col-md-3 padRgtFix padLeftFiver">
									<input type="reset" name="cancel" value="RESET" class="eilmbutton canclcls" id="btncancelrpd"/>
								</div>
								<div class="col-md-3 padRgtFix padLeftFiver">
									<input type="submit" name="add" value="ADD" onclick="return savevalidbtnfeaturedprofile()" class="eilmbutton savecls" />
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<?php form_close();?>

	</div>
</div>



