<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            SLIDER CONTROL
        </div>
    </div>
    <div class="bordHell" style="width: 128px !important;"></div>
    <div class="col-md-12 noPad" >

        <?php
        if($this->session->flashdata('message') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
        }

        if($this->session->flashdata('messageremove') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('messageremove'); ?>
            </div>
            <?php
        }
        ?>

        <?php echo form_open('Admin/delslider');?>

        <div class="popupdisp " id="modaldisp">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()" class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
            <input type="hidden" id="hidval" name="hidval">
            <input type="hidden" id="hidpicval" name="hidpicval">
        </div>

        <?php echo form_close(); ?>


        <?php echo form_open_multipart('admin/slider_add'); ?>

        <div class="col-md-8 noPad">

            <div class="loader" id="loading" style="display: none"></div>

            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px;">
                <div class="col-md-2 noPad">
                    <select name="app_type" id="app_type" class="selclass" style="height: 26px; width: 134px; display: inline-block;" onchange="slidelistchange()">
                        <!--						<option >Select An Application</option>-->
                        <option value=0<?php if($_GET['app_type'] == 0){?> selected="selected" <?php }?>>Web Portal</option>
                        <option value=1<?php if($_GET['app_type'] == 1){?> selected="selected" <?php }?>>Mobile Portal</option>
                        <option value=2<?php if($_GET['app_type'] == 2){?> selected="selected" <?php }?>>Mobile App</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select id="slider_type" name="slider_type" class="selclass" style="height: 26px; width: 88px; display: inline-block;" onchange="slidelistchange()">
                        <option value=0<?php if($_GET['slid_type'] == 0){?> selected="selected" <?php }?>>Slider</option>
                        <option value=1<?php if($_GET['slid_type'] == 1){?> selected="selected" <?php }?>>Ads</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        SLIDER CONTROL
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" style="margin-bottom: 4px;" id="slidercontrol">
                            <thead style="background: #2a6188!important;color: white;font-size: 11px;">
                            <tr>
                                <TH>Image</TH>
                                <TH>Description</TH>
                                <TH>Uploaded By</TH>
                                <TH>Position</TH>
                                <TH>Delete</TH>
                            </tr>
                            </thead>
                            <tbody style="background: white;" id="listview">
                            <?php foreach($sliderdata as $val):?>

                                <input type="hidden" id="hidpicname" name="hidpicname"  value="<?php echo $val->image_url; ?>">
                                <tr>
                                    <td>
                                        <div class="imgCov" style="width: 50px;">
                                            <a> <div style="height:50px;max-width: 100%;background-color: #e9ebee;background-position: center 30%;background-size: cover;background-repeat: no-repeat;display: block;background-image: url(<?php echo base_url();?>uploads/<?php echo $val->image_url;?>);"></div></a>
                                        </div>
                                    </td>
                                    <td><?php echo $val->desc_text; ?></td>
                                    <td><?php echo $val->full_name; ?></td>
                                    <td><?php echo $val->position; ?></td>
                                    <td style="font-size: 16px !important;text-align: center;"><div onclick="delAds(<?php echo $val->slider_id; ?>,'<?php echo $val->pic_url?>')"><a><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;" aria-hidden="true"></i></a></div></td>
                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 noPad">
            <div class="col-md-12 noPad">

                <div class="col-md-12 pageoverlap">
                    <div class="panel panel-default panelFixer">
                        <div class="panel-heading panelBlue">
                            Slider Details
                        </div>
                        <div class="panel-body formPadder">

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Description
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <textarea rows="3" name="slide_desc" id="slide_desc" style="width: 100%;"></textarea>
                                </div>
                            </div>

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Slider Image
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="file" class="custom-file-input" name="fileToUpload" id="fileToUpload" style="height: 33px;" />
                                </div>
                            </div>

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Uploaded By
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text"  value="<?php echo $username; ?>" class="form-control eilmlitecontrols more" disabled>
                                    <input type="hidden" name="uploaded_by" id="uploaded_by" value="<?php echo $userid; ?>">
                                </div>
                            </div>

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Position
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" name="position" id="position" class="form-control eilmlitecontrols more" >
                                    <input type="hidden" name="slider_id" id="slider_id" class="form-control eilmlitecontrols more" >
                                </div>
                            </div>

                            <div class="control-group form-group formFix" style="margin-top: 2px;">
                                <div class="col-md-4 padRgtFix padLeftFiver">
                                    <!--								<input type="submit" name="cancel" value="CANCEL" class="eilmbutton" disabled/>-->
                                </div>

                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="reset" value="RESET" class="eilmbutton canclcls" id="btncancelrpd"/>
                                </div>
                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="add" value="ADD" onclick="return validate_slider()" class="eilmbutton savecls" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php form_close();?>

    </div>
</div>



