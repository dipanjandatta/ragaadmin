
<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            TESTIMONIALS
        </div>
    </div>
    <div class="bordHell" style="width: 115px !important;"></div>
    <div class="col-md-12 noPad" >


        <?php
        if($this->session->flashdata('message') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
        }

        if($this->session->flashdata('messageremove') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo "Data Deleted Successfully"; ?>
            </div>
            <?php
        }
        ?>

        <?php echo form_open('Admin/del_testimonials');?>

        <div class="popupdisp" id="modaldisp">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()" class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
            <input type="hidden" id="hidval" name="hidval">
            <input type="hidden" id="hidpic" name="hidpic">
        </div>

        <?php echo form_close(); ?>

        <div class="col-md-4 noPad">

                <div class="loader" id="loading" style="display: none"></div>

            <?php echo form_open_multipart('admin/do_upload'); ?>
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        TESTIMONIALS
                    </div>
                    <div class="panel-body formPadder">
                        <div class="control-group form-group formFix">
                            <div class="col-md-2 labelFix">
                                Full Name
                            </div>
                            <div class="col-md-9 padRgtFix padLeftFiver">
                                <input type="text" name="fullname" id="fullname" class="form-control eilmlitecontrols more">
                                <input type="hidden" name="testimonials_id" id="testimonials_id"  class="form-control eilmlitecontrols more">
                            </div>
                        </div>
                        <div class="control-group form-group formFix">
                            <div class="col-md-2 labelFix">
                                Location
                            </div>
                            <div class="col-md-9 padRgtFix padLeftFiver">
                                <input type="text" name="location" id="location" class="form-control eilmlitecontrols more">
                            </div>
                        </div>
                        <div class="control-group form-group formFix">
                            <div class="col-md-2 labelFix">
                                Profile Url
                            </div>
                            <div class="col-md-9 padRgtFix padLeftFiver">
                                <input type="text" name="prof_url" id="prof_url" class="form-control eilmlitecontrols more">
                                <p style="font-size: 10px; color: red;">**please provide full and proper url <br>(If the profile is not in ragamix leave the field blank)</p>
                            </div>
                        </div>

                        <div class="control-group form-group formFix">
                            <div class="col-md-2 labelFix">
                                Details
                            </div>
                            <div class="controls">
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <p>
                                        <textarea rows="3" name="ads_desc" id="ads_desc" style="width: 100%;"></textarea>
                                    </p>
                                </div>
                            </div>


                            <div class="control-group form-group formFix">
                                <div class="col-md-2 labelFix noPad">
                                    Upload Pic
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <div id="selectImage">
                                        <input type="file" class="custom-file-input" name="fileToUpload" id="fileToUpload" style="height: 33px;" />
                                        <input type="hidden" class="custom-file-input" name="hiddenpic" id="hiddenpic" style="height: 33px;" />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="control-group form-group formFix" style="margin-top: 2px;">
                            <div class="col-md-4 padRgtFix padLeftFiver">
                            </div>

                            <div class="col-md-3 padRgtFix padLeftFiver">
                                <input type="submit" onclick="return validate_testi()" name="submit" value="SAVE"  class="eilmbutton savecls" />
                            </div>

                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            </div>
         </div>

        <div class="col-md-8">
            <div class="panel panel-default panelFixer">
                <div class="panel-heading panelBlue">
                     TESTIMONIALS DETAILS
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" style="margin-bottom: 4px;" id="testimonial">
                        <thead style="background: #2a6188!important;color: white;font-size: 11px;">
                        <tr>
                            <TH>Full Name</TH>
                            <TH>Location</TH>
                            <TH >Details</TH>
                            <th>Created On</th>
                            <TH>Picture</TH>
                            <TH>Edit</TH>
                            <TH>Delete</TH>
                        </tr>
                        </thead>
                        <tbody style="background: white;" id="listview">
                        <?php foreach($testimonialsdata as $val):?>
                            <tr>
                                <td ><?php echo $val->full_name; ?></td>
                                <td><?php echo$val->location; ?></td>
                                <td style="width:351px !important;"><?php echo $val->detail_info; ?></td>
                                <td><?php echo date('d M Y',strtotime($val->created_on)); ?></td>
                                <td>
                                    <div class="imgCov" style="width: 50px;">
                                        <a href="<?php echo $val->profile_url?>"> <div style="height:50px;max-width: 100%;background-color: #e9ebee;background-position: center 30%;background-size: cover;background-repeat: no-repeat;display: block;background-image: url(<?php echo base_url();?>uploads/<?php echo $val->pic_url;?>);"></div></a>
                                    </div>
                                </td>
                                <td style="font-size: 16px !important;text-align: center;"><a onclick="testimonialsval(<?php echo $val->testimonials_id;?>)"><i class="fa fa-pencil-square-o" style="cursor: pointer;" aria-hidden="true"></i></a></td>
                                <td style="font-size: 16px !important;text-align: center;"><div onclick="delTesti(<?php echo $val->testimonials_id; ?>,'<?php echo $val->pic_url?>')"><a><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;" aria-hidden="true"></i></a></div></td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

