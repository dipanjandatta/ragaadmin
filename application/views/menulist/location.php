<div class="container stanPad" xmlns="http://www.w3.org/1999/html">
    <div class="pageTitle">
        <div class="acPageTile">
            LOCATION
        </div>
    </div>
    <div class="bordHell" style="width: 86px !important;"></div>
    <div class="col-md-12 noPad" >

        <?php
        if($this->session->flashdata('message') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
        }

        if($this->session->flashdata('messageremove') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo "Data Deleted Successfully"; ?>
            </div>
            <?php
        }
        ?>

        <?php echo form_open('Admin/location_del');?>
        <div class="popupdisp " id="modaldisp">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()"  class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
            <input type="hidden" id="hidval" name="hidval">
        </div>
        <?php echo form_close(); ?>

        <?php echo form_open('admin/location_save'); ?>

        <div class="col-md-6 noPad">

            <div class="loader" id="loading" style="display: none"></div>

            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        LOCATION
                    </div>

                    <div class="panel-body" style="margin-top: 25px;">
                        <table  id="locdatatable" class="table table-bordered" style="margin-bottom: 4px;">
                            <thead  style="background: #2a6188!important;color: white;font-size: 11px;">
                            <tr>
                                <TH>City</TH>
                                <TH>Country</TH>
                                <TH>Zone</TH>
                                <TH>Latitude</TH>
                                <TH>Longitude</TH>
                                <TH>Edit</TH>
                                <TH>Delete</TH>

                            </tr>
                            </thead>
                            <tbody style="background: white;" >
                            <?php foreach($locationdatafetch as $val):?>
                                <tr>
                                    <td ><?php echo $val->city; ?></td>
                                    <td ><?php echo $val->country; ?></td>
                                    <td ><?php echo $val->zone; ?></td>
                                    <td ><?php echo $val->lat; ?></td>
                                    <td ><?php echo $val->long; ?></td>
                                    <td style="font-size: 16px !important"><div ><a href="<?php echo base_url();?>admin/location/edit/<?php echo $val->location_id; ?>"><i class="fa fa-pencil-square-o" style="cursor: pointer;" aria-hidden="true"></a></div></i></td>
                                    <td style="font-size: 16px !important"><a onclick="return delAds(<?php echo $val->location_id; ?>)"><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6 noPad">
            <div class="col-md-12 noPad">
                <div class="col-md-12 ">
                    <div class="panel panel-default panelFixer">
                        <div class="panel-heading panelBlue">
                            Manage Location/City
                        </div>
                        <div class="panel-body formPadder">

                            <div class="col-md-6 ">

                                <?php if($this->uri->segment(3) == 'edit'){ ?>

                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            City
                                        </div>
                                        <div class="col-md-7 padRgtFix padLeftFiver">
                                            <input type="text"  name="citydetails" id="citydetails" value="<?php echo $editlocationcity?>" class="form-control eilmlitecontrols more" >
                                            <input type="hidden"  name="locationid" id="locationid" value="<?php echo $editlocationid?>" />
                                        </div>
                                        <button type="submit" name="find" value="find" onclick="return searchloc()">
                                            <i class="fa fa-search"></i>
                                        </button>

                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Country
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text"  name="countrydetails" id="countrydetails" value="<?php echo $editlocationcountry?>" id="countrydetails"class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Zone
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <select class="form-control" id="zonedetails" name="zonedetails" style="display: table-row-group;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);" name="zone">
                                                <option value="">Please Select</option>
                                                <option value="EZ"<?php if ($editlocationzone == 'EZ') echo ' selected="selected"'; ?>>East</option>
                                                <option value="WZ"<?php if ($editlocationzone == 'WZ') echo ' selected="selected"'; ?>>West</option>
                                                <option value="NZ"<?php if ($editlocationzone == 'NZ') echo ' selected="selected"'; ?>>North</option>
                                                <option value="SZ"<?php if ($editlocationzone == 'SZ') echo ' selected="selected"'; ?>>South</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Latitude
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text" value="<?php echo $editlocationlat; ?>" class="form-control eilmlitecontrols more" disabled>
                                            <input type="hidden" name="latdetails" id="latdetails" value="<?php echo $editlocationlat; ?>" class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Longitude
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text" value="<?php echo $editlocationlong; ?>" class="form-control eilmlitecontrols more" disabled>
                                            <input type="hidden" name="longdetails" id="longdetails" value="<?php echo $editlocationlong; ?>" class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>

                                <?php }elseif($this->uri->segment(3) == 'add'){ ?>

                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            City
                                        </div>
                                        <div class="col-md-7 padRgtFix padLeftFiver">
                                            <input type="text"  name="citydetails" id="citydetails" value="<?php echo $this->session->userdata('admincity');?>" class="form-control eilmlitecontrols more" >
                                            <input type="hidden"  name="locationid" value="<?php echo $this->session->userdata('locid'); ?>"/>
                                        </div>
                                        <div class="col-md-2 padRgtFix padLeftFiver">
                                            <!--                                            <input id="Button1" type="submit" name=find value="find" />-->
                                            <button type="submit" name="find" value="find" onclick="return searchloc()">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>

                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Country
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text"  name="countrydetails"  value="<?php echo $this->session->userdata('admincountry');?>" id="countrydetails"class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Zone
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <select class="form-control" name="zonedetails" id="zonedetails" style="display: table-row-group;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);" name="zone">
                                                <option value="">Please Select</option>
                                                <option value="EZ"<?php /*if ($editlocationzone == 'EZ') echo ' selected="selected"'; */?>>East</option>
                                                <option value="WZ"<?php /*if ($editlocationzone == 'WZ') echo ' selected="selected"'; */?>>West</option>
                                                <option value="NZ"<?php /*if ($editlocationzone == 'NZ') echo ' selected="selected"'; */?>>North</option>
                                                <option value="SZ"<?php /*if ($editlocationzone == 'SZ') echo ' selected="selected"'; */?>>South</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Latitude
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text"  value="<?php echo $this->session->userdata('adminlat');?>" class="form-control eilmlitecontrols more" disabled>
                                            <input type="hidden" name="latdetails" id="latdetails" value="<?php echo $this->session->userdata('adminlat');?>" class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Longitude
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text"  value="<?php echo $this->session->userdata('adminlong');?>" class="form-control eilmlitecontrols more" disabled>
                                            <input type="hidden" name="longdetails" id="longdetails" value="<?php echo $this->session->userdata('adminlong');?>" class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>

                                <?php }else{ ?>

                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            City
                                        </div>
                                        <div class="col-md-7 padRgtFix padLeftFiver">
                                            <input type="text"  name="citydetails" id="citydetails" value="<?php echo $editlocationcity?>" class="form-control eilmlitecontrols more" >
                                            <input type="hidden"  name="locationid" id="locationid" />
                                        </div>
                                        <div class="col-md-2 padRgtFix padLeftFiver">
                                            <button type="submit" name="find" value="find" onclick="return searchloc()">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>

                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Country
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text"  name="countrydetails"  id="countrydetails" class="form-control eilmlitecontrols more" >
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Zone
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <select class="form-control" id="zonedetails" name="zonedetails" style="display: table-row-group;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);" name="zone">
                                                <option value="">Please Select</option>
                                                <option value="EZ">East</option>
                                                <option value="WZ">West</option>
                                                <option value="NZ">North</option>
                                                <option value="SZ">South</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Latitude
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text" name="latdetails" id="latdetails" class="form-control eilmlitecontrols more" disabled>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-3 labelFix">
                                            Longitude
                                        </div>
                                        <div class="col-md-9 padRgtFix padLeftFiver">
                                            <input type="text" id="longdetails" name="longdetails" class="form-control eilmlitecontrols more" disabled>
                                        </div>
                                    </div>

                                <?php } ?>

                                <div class="control-group form-group formFix" style="margin-top: 2px;">
                                    <div class="col-md-4 padRgtFix padLeftFiver">
                                    </div>

                                    <div class="col-md-3 padRgtFix padLeftFiver">
                                        <input type="submit" name="reset" value="RESET" class="eilmbutton canclcls" id="btncancelrpd"/>
                                    </div>
                                    <div class="col-md-3 padRgtFix padLeftFiver">
                                        <input type="submit" name="save" value="SAVE" onclick="return savevalid()" class="eilmbutton savecls" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div><?php echo $map['html']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo form_close();?>

    </div>
</div>



