<div class="container stanPad">
    <div class="pageTitle">
        <div class="acPageTile">
            SEEKING PROFILES
        </div>
    </div>
    <div class="bordHell" style="width: 136px !important;"></div>
    <div class="col-md-12 noPad" >

        <?php
        if($this->session->flashdata('message') != "")
        {
            ?>
            <div id="notification" style="display: none;">
                <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php
        }
        ?>

        <?php echo form_open('Admin/del_seeking');?>

        <div class="popupdisp " id="modaldisp">
            <div class="modal-dialog"><div class="modal-content"><div class="modal-header"><div class="bootstrap-dialog-header"><div class="bootstrap-dialog-close-button" style="display: none;"><button class="close" aria-label="close">×</button></div><div class="bootstrap-dialog-title" id="4a2133cf-f108-4306-929a-910072cbc3c4_title">Information</div></div></div><div class="modal-body"><div class="bootstrap-dialog-body"><div class="bootstrap-dialog-message">Are You sure you want to delete?</div></div></div><div class="modal-footer" style="display: block;"><div class="bootstrap-dialog-footer"><div class="bootstrap-dialog-footer-buttons"><button onclick="return retcancel()" class="btn btn-default" id="657d7d0f-cbe4-4ba5-a9ae-cf02c07f039a">NO</button><button type="submit" onclick="return okcheck()" class="btn btn-primary" id="9d43c01d-9fd0-44ae-9d76-265e2f8d5db8">YES</button></div></div></div></div></div>
            <input type="hidden" id="hidval" name="hidval">
            <input type="hidden" id="hidsection" name="hidsection">
        </div>

        <?php echo form_close(); ?>


        <?php echo form_open('admin/save_seeking');?>

        <div class="col-md-8 noPad">

            <div class="loader" id="loading" style="display: none"></div>

            <div class="col-md-12" style="margin-bottom: 38px;margin-top: 10px;">
            </div>
            <div class="col-md-12 noPad">
                <div class="panel panel-default panelFixer">
                    <div class="panel-heading panelBlue">
                        SEEKING PROFILES
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" style="margin-bottom: 4px;" id="seekingdata">
                            <thead style="background: #2a6188!important;color: white;font-size: 11px;">
                            <tr>
                                <TH>Image</TH>
                                <TH>Keywords</TH>
                                <TH>Location</TH>
                                <TH>Description</TH>
                                <TH>Hastags</TH>
                                <TH>Edit</TH>
                                <TH>Delete</TH>
                            </tr>
                            </thead>
                            <tbody style="background: white;" id="listview">
                            <?php foreach($seekinglist as $val):?>
                                <tr>
                                    <td>
                                        <div class="imgCov" style="width: 50px;">
                                            <a><div style="height:50px;max-width: 100%;background-color: #e9ebee;background-position: center 30%;background-size: cover;background-repeat: no-repeat;display: block;background-image: url(<?php echo $val->profile_pic_url;?>);"></div></a>
                                        </div>
                                    </td>
                                    <td ><?php echo $val->keyword; ?></td>
                                    <td><?php echo $val->location; ?></td>
                                    <td><?php echo $val->description; ?></td>
                                    <td><?php echo $val->hashtags; ?></td>
                                    <td style="font-size: 16px !important;text-align: center;"><a onclick="seekval('<?php echo $val->rm_id;?>')"><i class="fa fa-pencil-square-o" style="cursor: pointer;" aria-hidden="true"></i></a></td>
                                    <td><a onclick="delAds(<?php echo $val->serial_no; ?>)" style="cursor: pointer;color: #2a6188;"><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;margin-top: 3px;margin-left: 12px;font-size: 16px;" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 noPad">
            <div class="col-md-12 noPad">

                <div class="col-md-12 noPad">
                    <div class="col-md-12 noPad" style="margin-bottom: 10px;margin-top: 10px;">
                        <div class="col-md-3">
                            <select id="usercat" name="usercat" class="selclass" style="height: 26px; width: 88px; display: inline-block;" onchange="showhide()" >
                                <option value="Ragamix">Ragamix</option>
                                <option value="Users">Users</option>
                            </select>
                        </div>

                        <div class="col-md-6" style="z-index: 99;">
                            <input type="text" id="search1" name="search1" onkeyup="seekingkeyup()" placeholder="Search" class="form-control eilmlitecontrols more " >
                            <div id="displays" class="disp " style="display: block; z-index: 999999999;" ></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 pageoverlap">
                    <div class="panel panel-default panelFixer">
                        <div class="panel-heading panelBlue">
                            Seeking Profile Details
                        </div>
                        <div class="panel-body formPadder">

                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    User Name
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="full_name_seek" name="full_name_seek" class="form-control eilmlitecontrols more" disabled>
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Keyword
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" id="keyword" name="keyword" class="form-control eilmlitecontrols more">
                                    <input type="hidden" name="serial_no" id="serial_no" class="form-control eilmlitecontrols more" >
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Location
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" name="location" id="location" class="form-control eilmlitecontrols more" >
                                    <input type="hidden" id="rm_id_take" name="rm_id_take"  class="form-control eilmlitecontrols more" >
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    Description
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
<!--                                    <input type="text" name="description" id="description" class="form-control eilmlitecontrols more" >-->
                                    <p style="margin-bottom: -4px !important;">
                                        <textarea rows="3" name="description" id="description" style="width: 100%;"></textarea>
                                    </p>
                                </div>
                            </div>
                            <div class="control-group form-group formFix">
                                <div class="col-md-3 labelFix">
                                    HashTag
                                </div>
                                <div class="col-md-9 padRgtFix padLeftFiver">
                                    <input type="text" name="hashtags" id="hashtags" class="form-control eilmlitecontrols more" >
                                </div>
                            </div>

                            <div class="control-group form-group formFix" style="margin-top: 2px;">
                                <div class="col-md-4 padRgtFix padLeftFiver">
                                </div>

                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="reset" value="RESET" class="eilmbutton canclcls" id="btncancelrpd"/>
                                </div>
                                <div class="col-md-3 padRgtFix padLeftFiver">
                                    <input type="submit" name="add" value="ADD" onclick="return savevalidbtnseek()" class="eilmbutton savecls" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php form_close();?>

    </div>
</div>



