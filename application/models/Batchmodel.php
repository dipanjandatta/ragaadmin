<?php 
/**
 * @author Subhajit
 * @copyright 2016
 */
class Batchmodel extends CI_Model {
 
    function Model() {
        // Call the Model constructor
        parent::__construct();
    }
    
    
    function getFbLinkList($position,$offset){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$offset';");
        $query = $this->db->query("CALL get_fb_link_list(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
            'records'=>$result,
            'count' => $resultout[0]->out_param
        );
    }
    
    function getYoutubeList($position,$offset){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$offset';");
        $query = $this->db->query("CALL get_youtube_url_list(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
            'records'=>$result,
            'count' => $resultout[0]->out_param
        );
    }
    
    function fbUpdateLikeCount($basicid,$lCount){
        $this->db->query("CALL update_facebook_cnt('$basicid','$lCount');");
        return true;
    }
    
    function youtubeUpdateViewCount($basicid,$lCount){
        $this->db->query("CALL update_youtube_like_cnt('$basicid','$lCount');");
        return true;
    }
    
    function getAllEventsAdmin($position,$items,$rowcnt){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_admin_events_info(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
            'records'=>$result,
            'count' => $resultout[0]->out_param
        );
    }

    function getAllAdsAdmin($position,$items,$rowcnt){
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $this->db->query("SET @totalRowCnt := '$rowcnt';");
        $query = $this->db->query("CALL get_admin_ads_info(@offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
            'records'=>$result,
            'count' => $resultout[0]->out_param
        );
    }

    function getAllAdsAdminApproved($position,$items,$qu){
        if($qu == 1){
            $typeQuery = 1;
        }
        if($qu == 0){
            $typeQuery = 0;
        }
        $query = $this->db->query("select a.ad_id,a.rm_id,a.category,a.ads_title, a.ads_desc, a.ads_approved, a.photo_id, a.created_on,a.updated_on,p.pic_url,p.pic_privacy, u.user_name as uploaded_by from ads a left join photos p on a.photo_id = p.photo_id left join users u on u.rm_id = a.rm_id where a.ads_approved = ? order by a.ad_id desc LIMIT $position, $items", array('a.ads_approved'=>$typeQuery));
        $result = $query->result();
        return $result;
    }

    function getAllEventsAdminApproved($position,$items,$qu){
        if($qu == 1){
            $typeQuery = 1;
        }
        if($qu == 0){
            $typeQuery = 0;
        }
        $query = $this->db->query("select a.event_ad_id,a.rm_id,a.category,a.event_location,a.event_city,a.event_date,a.event_title, a.event_desc, a.doc_youtube_url, a.event_approved, a.photo_id, a.created_on,a.updated_on,p.pic_url,p.pic_privacy, u.user_name as uploaded_by from events a left join photos p on a.photo_id = p.photo_id left join users u on u.rm_id = a.rm_id where a.event_approved = ? order by a.event_ad_id desc LIMIT $position, $items", array('a.event_approved'=>$typeQuery));
        $result = $query->result();
        return $result;
    }

    function AddFeaturedProfile($data){
        $this->db->query("SET @statusMsg := '';");
        $p = $data['user_name_in'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['position_in'];
        $p3 = $data['app_type'];
        $this->db->query("CALL add_featured_profile_info('$p','$p1','$p2','$p3',@statusMsg);");
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @statusMsg as out_param;');
        $resultout = $out_param_query->result();
        return $resultout[0]->out_param;
    }
    
    function AddFeaturedProfileOthers($data){
        $this->db->query("SET @statusMsg := '';");
        $p = $data['display_page_in'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['position_in'];
        $p3 = $data['app_type'];
        $this->db->query("CALL add_featured_listing_info('$p','$p1','$p2','$p3',@statusMsg);");
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @statusMsg as out_param;');
        $resultout = $out_param_query->result();
        return $resultout[0]->out_param;
    }
    
    function deleteFProfile($fid){
        $this->db->query("CALL delete_featured_profile_info('$fid');");
        return true;
    }
    
    function deleteFProfileOthers($fid){
        $this->db->query("CALL delete_featured_listing_info('$fid');");
        return true;
    }

    function getFeaturedProfileList($data){
        $query = $this->db->query("CALL get_featured_profile_list('$data');");
        $result = $query->result();
        return $result;
    }

    function getFeaturedMusician($data){
        $p0=$data['section'];
        $p1=$data['portal'];
        $query = $this->db->query("CALL get_featured_musicians('$p0','$p1');");
        $result = $query->result();
        return $result;
    }

    function get_profiles_for_featured($cat, $uname){
        $query = $this->db->query("CALL get_profiles_for_featured('$cat','$uname');");
        $result = $query->result();
        return $result;
    }

    function getCityNameOnCountry($countryVal){
        $query = $this->db->query("CALL get_city_list_for_country('$countryVal');");
        $result = $query->result();
        return $result;
    }

    function autoCompSearch($searchVal,$usercat,$city,$position,$items){
        $this->db->query("SET @search_text := '$searchVal';");
        $this->db->query("SET @user_category_in := '$usercat';");
        $this->db->query("SET @city_name_in := '$city';");
        $this->db->query("SET @offset_val := '$position';");
        $this->db->query("SET @limit_val := '$items';");
        $query = $this->db->query("CALL search_profiles(@search_text, @user_category_in, @city_name_in, @offset_val, @limit_val, @totalRowCnt);");
        $result = $query->result();
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @totalRowCnt as out_param;');
        $resultout = $out_param_query->result();
        return array(
            'records'=>$result,
            'count' => $resultout[0]->out_param
        );
    }

    function get_featured_events($data){

        $success=$this->db->query("CALL get_featured_events('$data');");
        $result=$success->result();
        return $result;
    }

    function get_featured_ads($data){

        $success=$this->db->query("CALL get_featured_ads('$data');");
        $result=$success->result();
        return $result;
    }

    function get_events_for_featured($data){
        $p0 = $data['event_title'];
        $p1 = $data['posted_by'];
        $p2 = $data['app_type_in'];
        $success=$this->db->query("CALL get_events_for_featured('$p0','$p1','$p2');");
        $result=$success->result();
        return $result;
    }
}
?>