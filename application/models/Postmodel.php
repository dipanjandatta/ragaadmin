<?php 
/**
 * @author Subhajit
 * @copyright 2016
 */

class Postmodel extends CI_Model {
 
    function Model() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function postLoginDetails($data){

        $p1 = $data['user_name_in'];
        $p2 = $data['password_in'];
        $p3 = $data['fb_login'];
        $this->db->query("SET @user_name_in := '$p1';");
        $this->db->query("CALL login(@rm_id_val,@user_name_in,'$p2','$p3',@active_user,@user_type_id,@user_group_id,@profile_pic_url);");
        $out_param_query = $this->db->query('select @rm_id_val as rm_out_param, @active_user as active_out_param, @user_name_in as user_name_param, @user_type_id as user_type_id, @user_group_id as user_group_id, @profile_pic_url as profile_pic;');
        $result = $out_param_query->result();

        return $result;
    }
    
    function searchTestModel($searchval){
        $query = $this->db->query("select user_name from users where user_name like '%$searchval%' limit 5");
        $result = $query->result();
        $i = 0;
        $test = array();
        foreach($result as $row){
            $test[$i]['id'] = $row->id;
            $test[$i]['name'] = $row->user_name;
            $i++;
        }
        return $test;
    }
    
    function postFacebookData($user){
        $this->db->insert('users',$user);
        return $this->db->insert_id();
    }

    function checkFbIdExistence($userFbId){
        $query = $this->db->query("select rm_id, facebook_id from users where facebook_id = ?", array('facebook_id'=>$userFbId));
        $result = $query->result();
        if($query->num_rows() == 1){
            return $result[0]->rm_id;
        }
        else{
            return 0;
        }
    }
    
    function checkUserName($uname){
        $query = $this->db->query("select user_name from users where user_name = ?", array('user_name'=>$uname));
        $result = $query->result();
        if($query->num_rows() == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    function getMsgCnt($userid){
        $query = $this->db->query("select a.user_name, b.email_tran_id, b.body_text from users a, mailbox b where b.from_rm_id = a.rm_id and b.to_rm_id = ? and b.has_read = ?", array('b.to_rm_id' => $userid,'b.has_read'=>0));
        $result = $query->result();
        if($query->num_rows() > 0){
            return array(
                'count' => count($result)
            );
        }
        else {
            return 0;
        }
    }
    
    function getSignUp($data){       
        $p1 = $data['user_name_in'];
        $p2 = $data['user_cat_in'];
        $p3 = $data['facebook_id_in'];
        $p4 = $data['password_in'];
        $p5 = $data['email_in'];
        $p6 = $data['mobile_no_in'];
        $p7 = $data['city_name_in'];
        $p8 = $data['country_name_in'];
        //$p9 = $data['view_count'];
        $p10 = $data['user_type_in'];
        $p11 = $data['signup_date'];
        $p12 = $data['updated_on'];
        $p13 = $data['activation_key_in'];
        $success = $this->db->query("CALL signup(@rm_id_val,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p10','$p13');");
        $out_param_query = $this->db->query('select @rm_id_val as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }
    
    function updateActivation($keyCode){
        $param = $keyCode['activation_key_in'];
        $success = $this->db->query("CALL activate_user('$param');");
        return $success;
    }
    
    function sendEmailActivation($json,$item){
        $ch = curl_init($item.'user/activationmail');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($json))                                                                       
        );
        $result = curl_exec($ch);
        return $result;
    }
    
    function logOutUser($userId){
        $this->db->query("CALL logout_app('$userId');");
        return true;
    }
    
    
    
    
/*ACTIVITY CONSOLE START*/

    function get_all_user_activity($data)
    {
        $p1 = $data['from_date'];
        $p2 = $data['to_date'];
        $p3 = $data['time_interval'];
//        $this->db->trans_start();
        $success = $this->db->query("CALL get_all_user_activity('$p1','$p2','$p3');");
//        $this->db->trans_complete();
        $result = $success->result();
        return $result;
    }

    function  get_activity_counter($data)
    {
        $this->db->trans_start();

        $p0 = $data['from_date'];
        $p1 = $data['to_date'];
        $p2 = $data['time_interval'];

        $this->db->query("SET @from_date := '$p0';");
        $this->db->query("SET @to_date	 := '$p1';");
        $this->db->query("SET @time_interval := '$p2';");
        $this->db->query("CALL `get_activity_counter` (@from_date , @to_date , @time_interval , @p3 , @p4 , @p5 , @p6 , @p7 , @p8 , @p9 , @p10);");
        $out_param_query = $this->db->query('select @p3 AS  `signup_cnt` , @p4 AS  `update_cnt` , @p5 AS  `upload_cnt` , @p6 AS  `delete_cnt` , @p7 AS  `adssubmit_cnt` , @p8 AS  `eventsubmit_cnt` , @p9 AS  `msgsent_cnt` , @p10 AS  `currlogin_cnt`');
        $result = $out_param_query->result();
        $this->db->trans_complete();

        return $result;
    }

    /*ACTIVITY CONSOLE END*/


    function get_genre_list()
    {
//        $this->db->trans_start();
        $success = $this->db->query("CALL get_genre_list();");
//        $this->db->trans_complete();

        $result = $success->result();
        return $result;
    }

    function save_update_genre($data)
    {
//        $this->db->trans_start();
        $g1 = $data['genre_id'];
        $g2 = $data['genre'];
        $this->db->query("CALL save_update_genre('$g1','$g2');");
//        $this->db->trans_complete();
    }

    function delete_genre_info($data)
    {

//        $this->db->trans_start();
        $g1 = $data['genre_id'];
        $success = $this->db->query("CALL delete_genre_info('$g1');");
//        $this->db->trans_complete();
        return $success;
    }

    /*VOCAL START*/
    function get_vocal_list()
    {
//        $this->db->trans_start();
        $success = $this->db->query("CALL get_vocal_list();");
//        $this->db->trans_complete();
        $result = $success->result();
        return $result;
    }

    function save_update_vocal($data)
    {
//        $this->db->trans_start();
        $g1 = $data['vocal_id'];
        $g2 = $data['vocal'];
        $success = $this->db->query("CALL save_update_vocal('$g1','$g2');");
//        $this->db->trans_complete();
        return $success;
    }

    function delete_vocal_info($data)
    {
//        $this->db->trans_start();
        $g1 = $data['vocal_id'];
        $this->db->query("CALL delete_vocal_info('$g1');");
//        $this->db->trans_complete();
    }
    /*VOCAL END*/

    /*WIND INSTRUMENT START*/
    function get_windinstr_list()
    {
//        $this->db->trans_start();
        $success = $this->db->query("CALL get_windinstr_list();");
//        $this->db->trans_complete();
        $result = $success->result();
        return $result;
    }

    function save_update_windinstr($data)
    {
//        $this->db->trans_start();
        $g1 = $data['windinstr_id'];
        $g2 = $data['windinstr'];
        $this->db->query("CALL save_update_windinstr('$g1','$g2');");
//        $this->db->trans_complete();
    }

    function delete_windinstr_info($data)
    {
//        $this->db->trans_start();
        $g1 = $data['windinstr_id'];
        $this->db->query("CALL delete_windinstr_info('$g1');");
//        $this->db->trans_complete();
    }
    /*WIND INSTRUMENT END*/

    /*REED INSTRUMENT START*/
    function get_reed_list()
    {
//        $this->db->trans_start();
        $success = $this->db->query("CALL get_reedinstr_list();");
//        $this->db->trans_complete();
        $result = $success->result();
        return $result;
    }

    function save_update_reedinstr($data)
    {
//        $this->db->trans_start();
        $g1 = $data['reedinstr_id'];
        $g2 = $data['reedinstr'];
        $this->db->query("CALL save_update_reedinstr('$g1','$g2');");
//        $this->db->trans_complete();
    }


    function delete_reedinstr_info($data)
    {
//        $this->db->trans_start();
        $g1 = $data['reedinstr_id'];
        $this->db->query("CALL delete_reedinstr_info('$g1');");
//        $this->db->trans_complete();
    }
    /*REED INSTRUMENT END*/

    /*STRING INSTRUMENT START*/
    function get_stringinstr_list()
    {
//        $this->db->trans_start();
        $success = $this->db->query("CALL get_stringinstr_list();");
//        $this->db->trans_complete();
        $result = $success->result();
        return $result;
    }

    function save_update_stringinstr($data)
    {
//        $this->db->trans_start();
        $g1 = $data['stringinstr_id'];
        $g2 = $data['stringinstr'];
        $this->db->query("CALL save_update_stringinstr('$g1','$g2');");
//        $this->db->trans_complete();
    }

    function delete_stringinstr_info($data)
    {
//        $this->db->trans_start();
        $g1 = $data['stringinstr_id'];
        $this->db->query("CALL delete_stringinstr_info('$g1');");
//        $this->db->trans_complete();
    }
    /*STRING INSTRUMENT END*/

    /*USERTYPE START*/
    function get_usertype_list()
    {
        $g1 = 0;
        $success = $this->db->query("CALL get_usertype_list('$g1');");
        $result = $success->result();
        return $result;
    }

    function save_update_usertype($data)
    {
//        $this->db->trans_start();
        $g1 = $data['usr_type_id'];
        $g2 = $data['usr_type'];

        $this->db->query("SET @usr_type_id := '$g1';");
        $this->db->query("SET @usr_type := '$g2';");
        $success = $this->db->query("CALL save_update_usertype(@usr_type_id ,@usr_type);");
        $out_param_query = $this->db->query('select @usr_type_id AS `usr_type_id_in`');

        $result = $out_param_query->result();
//        $this->db->trans_complete();
        return  $result;
    }


    function delete_usertype_info($data)
    {
//        $this->db->trans_start();
        $g1 = $data['usr_type_id'];
        $success = $this->db->query("CALL delete_usertype_info('$g1');");
//        $result = $success->result();
//        $this->db->trans_complete();
        return $success;
    }
    /*USERTYPE END*/

    /*Roles Right Start*/
    function get_all_user_groups()
    {
        $success = $this->db->query("CALL get_all_user_groups();");
        $result = $success->result();
        return $result;
    }

    function get_assigned_users($data)
    {
        $p1 = $data['group_id'];
        $success = $this->db->query("CALL get_assigned_users('$p1');");
        $result = $success->result();
        return $result;
    }

    function add_user_group($data)
    {
        $p1 = $data['group_id'];
        $p2 = $data['group_name'];
        $p3 = $data['group_type'];

        $success = $this->db->query("CALL add_user_group('$p1','$p2','$p3');");
        return 0;
    }

    function set_user_group($data){

        $p1 = $data['group_id'];
        $p2 = $data['group_type'];
        $p3 = $data['rm_id'];

        $success = $this->db->query("CALL set_user_group('$p1','$p2','$p3');");
        return 0;
    }

    function get_users_for_group()
    {
        $success = $this->db->query("CALL get_users_for_group();");
        $result = $success->result();
        return $result;
    }
    /*Roles Right End*/


    /*LOCATION START*/
    function get_location_info()
    {
        $p1 = 0;
        $success = $this->db->query("CALL get_location_info('$p1');");
        $result = $success->result();
        return $result;
    }

    function getStringLocationInfo(){
        $return[''] = 'Select City';
        $query = $this->db->query("CALL get_location_info('0');");
        foreach($query->result_array() as $row){
            $return[$row['city']] = $row['city'];
        }
        return $return;
    }

    function save_update_location_info($data)
    {
        $p0 = $data['location_id'];
        $p1 = $data['city'];
        $p2 = $data['country'];
        $p3 = $data['zone'];
        $abc = $data['lat'];
        $p4=(float)$abc;
        $p5 = $data['long'];

        $this->db->query("SET @location_id := '$p0';");
        $this->db->query("SET @city	 := '$p1';");
        $this->db->query("SET @country := '$p2';");
        $this->db->query("SET @zone := '$p3';");
        $this->db->query("SET @lat := '$p4';");
        $this->db->query("SET @long := '$p5';");

        $this->db->query("CALL save_update_location_info(@location_id , @city , @country , @zone , @lat , @long);");

        $out_param_query = $this->db->query('select @location_id AS `location_id`');
        $result = $out_param_query->result();
        return $result;
    }

    function delete_location_info($data)
    {
//        $this->db->trans_start();
        $g1 = $data['location_id'];
        $this->db->query("CALL delete_location_info('$g1');");
//        $this->db->trans_complete();
    }

    /*LOCATION End*/

    /*EVENT START*/
    function get_all_events_info($data){
        $p0 = $data['rm_id'];
        $p1 = $data['event_ad_id'];

        $success=$this->db->query("CALL get_all_events_info('$p0','$p1');");
        $result = $success->result();
        return $result;
    }


    function insertEventsadmin($data){

        $p = $data['event_ad_id_in'];
        $p1 = $data['rm_id'];
        $p2 = $data['category'];
        $p3 = $data['event_location'];
        $p4 = $data['event_city'];
        $p5 = $data['event_date'];
        $p6 = $data['event_title'];
        $p7 = $data['event_desc'];
        $p8 = $data['doc_youtube_url'];
        $p9 = $data['event_approved'];
        $p10 = $data['page_position'];
        $p11 = $data['photo_id_in'];
        $p12 = $data['pic_url'];
        $p13 = $data['pic_privacy'];
        $p14 = $data['pic_height'];
        $p15 = $data['pic_width'];

        $this->db->query("SET @event_ad_id_in := '$p';");
        $this->db->query("SET @photo_id_in := '$p11';");

        $this->db->query("CALL save_update_events_info(@event_ad_id_in,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10',@photo_id_in,'$p12','$p13','$p14','$p15');");
        $out_param_query = $this->db->query('select @event_ad_id_in as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }
    /*EVENT END*/

    /*ADS START*/
    function get_all_ads_info($data){
        $p0 = $data['rm_id'];
        $p1 = $data['ad_id'];
        $p2 = $data['category_in'];
        $p3 = $data['offset_val'];
        $p4 = $data['limit_in'];

        $success=$this->db->query("CALL get_all_ads_info('$p0','$p1','$p2','$p3','$p4',@p5);");
        $reslt1=$success->result();
        return $reslt1;
    }


    function insertAdsadmin($data){

        $p = $data['ad_id_in'];
        $p1 = $data['rm_id'];
        $p2 = $data['category'];
        $p3 = $data['ads_title'];
        $p4 = $data['ads_desc'];
        $p5 = $data['ads_views'];
        $p6 = $data['ads_approved'];
        $p7 = $data['enable_ads'];
        $p8 = $data['ads_status'];
        $p9 = $data['page_position'];
        $p10 = $data['photo_id_int'];
        $p11 = $data['pic_url'];
        $p12 = $data['pic_privacy'];
        $p13 = $data['pic_height'];
        $p14 = $data['pic_width'];

        $this->db->query("SET @ad_id_in := '$p';");
        $this->db->query("SET @photo_id_int := '$p10';");

        $this->db->query("CALL save_update_ads_info(@ad_id_in,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9',@photo_id_in,'$p11','$p12','$p13','$p14');");
        $out_param_query = $this->db->query('select @ad_id_in as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }
    /*ADS END*/

    /*FEATURED EVENTS START*/
    function get_featured_events(){

        $success=$this->db->query("CALL get_featured_events();");
        $result=$success->result();
        return $result;
    }

    function get_events_for_featured($data){

        $p0 = $data['event_title_in'];
        $p1 = $data['user_name_in'];

        $success=$this->db->query("CALL get_events_for_featured('$p0','$p1');");
        $result=$success->result();
        return $result;
    }

    function add_featured_events($data){

        $this->db->query("SET @statusMsg := '';");
        $p = $data['event_id'];
        $p1 = $data['event_title'];
        $p2 = $data['user_name'];
        $p3 = $data['position'];
        $p4 = $data['app_type_in'];
        $this->db->query("CALL add_featured_events('$p','$p1','$p2','$p3','$p4',@statusMsg);");
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @statusMsg as out_param;');
        $resultout = $out_param_query->result();

        return $resultout[0]->out_param;
    }

    function delete_featured_events_info($data){

        $p0=$data['featured_id'];

        $this->db->query("CALL delete_featured_events_info('$p0');");
        return true;
    }
    /*FEATURED EVENTS END*/


    /*FEATURED ADS START*/
    function get_featured_ads(){

        $success=$this->db->query("CALL get_featured_ads();");
        $result=$success->result();
        return $result;
    }

    function  get_ads_for_featured($data){

        $p0 = $data['ad_title'];
        $p1 = $data['posted_by'];
        $p2 = $data['app_type_in'];

        $success=$this->db->query("CALL  get_ads_for_featured('$p0','$p1','$p2');");
        $result=$success->result();
        return $result;
    }

    function delete_featured_ads_info($data){

        $p0=$data['featured_id'];

        $this->db->query("CALL delete_featured_ads_info('$p0');");
        return true;
    }

    function add_featured_ads($data){

        $this->db->query("SET @statusMsg := '';");
        $p = $data['ads_id'];
        $p1 = $data['ads_title'];
        $p2 = $data['user_name'];
        $p3 = $data['position'];
        $p4 = $data['app_type_in'];
        $this->db->query("CALL add_featured_ads('$p','$p1','$p2','$p3','$p4',@statusMsg);");
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @statusMsg as out_param;');
        $resultout = $out_param_query->result();

        return $resultout[0]->out_param;
    }
    /*FEATURED ADS END*/

    function delete_profile_info($data){

        $p1 = $data;

        $this->db->query("CALL delete_profile_info ('$p1');");
        return true;
    }



    function delete_testimonial_info($data)
    {
//        $this->db->trans_start();
        $p0 = $data['testimonials_id'];
        $success = $this->db->query("CALL delete_testimonial_info('$p0');");
//        $this->db->trans_complete();
        return $success;
    }

/*Testimonial*/

    function get_testimonials_list($data)
    {
        $p0 = $data['testimonials_id'];
        $success = $this->db->query("CALL get_testimonials_list('$p0');");
        $result = $success->result();
        return $result;
    }

    function save_update_testimonial($data)
    {
        $p0 = $data['testimonials_id'];
        $p1 = $data['full_name'];
        $p2 = $data['location'];
        $p3 = $data['detail_info'];
        $p4 = $data['pic_url'];
        $p5 = $data['prof_url'];

        $this->db->query("SET @testimonials_id := '$p0';");
        $this->db->query("SET @full_name := '$p1';");
        $this->db->query("SET @location := '$p2';");
        $this->db->query("SET @detail_info := '$p3';");
        $this->db->query("SET @pic_url := '$p4';");
        $this->db->query("SET @prof_url := '$p5';");


        $this->db->query("CALL save_update_testimonial(@testimonials_id , @full_name , @location , @detail_info , @pic_url,@prof_url);");

        $out_param_query = $this->db->query('select @testimonials_id AS `testimonials_id`');
        $result = $out_param_query->result();
        return $result;
    }

    function add_home_page_slider($data){

        $this->db->query("SET @statusMsg := '';");
        $p0 = $data['image_url'];
        $p1 = $data['slide_desc'];
        $p2 = $data['position'];
        $p3 = $data['slider_type'];
        $p4 = $data['app_type'];
        $p5 = $data['uploaded_by'];

        $this->db->query("CALL add_home_page_slider('$p0','$p1','$p2','$p3','$p4','$p5',@statusMsg);");
        @$this->db->free_db_resource();
        $out_param_query = $this->db->query('select @statusMsg as out_param;');
        $resultout = $out_param_query->result();
        return $resultout[0]->out_param;
    }

    function get_home_page_slider($data){

        $p0=$data['slider_id'];
        $p1=$data['slider_type'];
        $p2=$data['app_type'];

        $success = $this->db->query("CALL get_home_page_slider('$p0','$p1','$p2');");
        $result = $success->result();
//        print_r($result);exit();
        return $result;


    }

    function delete_slider_info($p0){

        //        $this->db->trans_start();
        $success = $this->db->query("CALL delete_slider_info('$p0');");
//        $this->db->trans_complete();
        return $success;
    }

    function get_username(){
        $sql = "select user_name from users where rm_id=\'2015110700000000024\'";

        return $sql;
    }

    function postSeekRequirements($data){
        $p = $data['serial_no_in'];
        $p1 = $data['rm_id_in'];
        $p2 = $data['keyword'];
        $p3 = $data['location'];
        $p4 = $data['description'];
        $p5 = $data['hashtags'];

        $this->db->query("SET @serial_no_in := '$p';");
        $this->db->query("CALL save_update_seeking_info(@serial_no_in,'$p1','$p2','$p3','$p4','$p5');");
        $out_param_query = $this->db->query('select @serial_no_in as out_param;');
        $result = $out_param_query->result();
        return $result[0]->out_param;
    }

    function getSeekingRequirements($data){

        $this->db->query("SET @statusMsg := '';");
        $p0=$data['rm_id'];
        $p1=$data['offset'];
        $p2=$data['limit'];

        $query = $this->db->query("CALL get_all_seeking_info('$p0','$p1','$p2',@statusMsg);");
        $result = $query->result();
        return $result;

//        $p = $data['ads_id'];
//        $p1 = $data['ads_title'];
//        $p2 = $data['user_name'];
//        $p3 = $data['position'];
//        $p4 = $data['app_type_in'];
//        $this->db->query("CALL add_featured_ads('$p','$p1','$p2','$p3','$p4',@statusMsg);");
//        @$this->db->free_db_resource();
//        $out_param_query = $this->db->query('select @statusMsg as out_param;');
//        $resultout = $out_param_query->result();
//
//        return $resultout[0]->out_param;

    }

    function delete_seeking_info($p0){

//        $this->db->trans_start();
        $success = $this->db->query("CALL delete_seeking_info('$p0');");
//        $this->db->trans_complete();
        return $success;
    }
}
?>
