/**
 * Created by developer15 on 25/8/16.
 */


function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}


function seldatechnge(){

    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();

    if(from_date == "" || to_date == ""){
        $('#showingalert').fadeIn('slow').animate({opacity: 1.0}, 1500).fadeOut('slow')
        return false;
    }else{
        $('#loading').show();
    }
}

function seltimechnge(){

    var time_interval=$('#mySelect').val();

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/getlistview',
        data: {
            time_interval: time_interval
        },
        beforeSend: function(){
            $('#loading').show();
        },
        success: function (data) {

            $('#from_date').val("");
            $('#to_date').val("");

            $("#tableview").html(data).show();

            $('#loading').hide();

        }
    });

}

function gettingusertype(val,val1){

    $('#usertypedetails').val(val);
    $('#usertypeid').val(val1);
}

/*function change the list of section and populate in grid*/
function sectionlistchange(){

    var section=$('#section').val();
    var portal=$('#app_type').val();

    $('#loading').show();
    window.location.href = window.location.origin+"/ragaadmin/Admin/featured_profile?sec="+section+"&type="+portal;
    //$('#loading').hide();
}

function slidelistchange(){

    var app_type=$('#app_type').val();
    var slider_type=$('#slider_type').val();

    $('#loading').show();
    window.location.href = window.location.origin+"/ragaadmin/Admin/slidercontrol?slid_type="+slider_type+"&app_type="+app_type;
}

function sectionlistevents(){

    var portal=$('#app_type').val();

    $('#loading').show();
    window.location.href = window.location.origin+"/ragaadmin/Admin/featured_events?type="+portal;
}

function sectionlistads(){

    var portal=$('#app_type').val();

    $('#loading').show();
    window.location.href = window.location.origin+"/ragaadmin/Admin/featured_ads?type="+portal;
}

/*function to search events*/
function searchitem(){

    var event_title=$('#event_title').val();
    var posted_by=$('#posted_by').val();

    if(event_title == ""){
    }

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/searchEvent',
        data: {
            event_title:event_title,posted_by:posted_by
        },
        cache: false,
        beforeSend: function(){
            $('#loading').show();
        },
        success: function(data)
        {
            var jsr=$.parseJSON(data);

            console.log("length of jsr");
            console.log(jsr.length);
            console.log(jsr);

            if(jsr.length > 0){
                $('#event_title_txtfld').val(jsr[0].event_title);
                $('#event_loc').val(jsr[0].event_city);
                $('#event_date').val(jsr[0].event_date);
                $('#posted_by_txtfld').val(jsr[0].user_name);
                $('#loading').hide();
            }else{
                $('#loading').hide();

                $("#notification").fadeIn("slow").append('No Record Found').delay(2000).fadeOut("slow");
                $(".dismiss").click(function(){
                    $("#notification").fadeOut("slow");
                });
            }
        }
    });
}

function okcheck(){

    $('#modaldisp').hide();
    //$('#loading').show();
    return true;
}

function retcancel(){

    $('#modaldisp').hide();
    return false;
}

function delAds(val){

    $('#modaldisp').show();
    $('#hidval').val(val);

    //only required for featured profile

    var hidpicname= $('#hidpicname').val();
     $('#hidpicval').val(hidpicname);

    var section= $('#section').val();
    $('#hidsection').val(section);

    return false;
}

function delTesti(val,val1){

    $('#modaldisp').show();
    $('#hidval').val(val);
    $('#hidpic').val(val1);

    return false;
}

function removeEvent(val){

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/eventremove',
        data: {
            featured_id:val,
        },
        cache: false,
        beforeSend: function(){

            $('#loading').show();
        },
        success: function(data)
        {
            window.location.href = window.location.origin+"/ragaadmin/Admin/featured_events";
            $('#loading').hide();
        }
    });
}

function removeAds(val){

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/removeAds',
        data: {
            featured_id:val,
        },
        cache: false,
        beforeSend: function(){

            $('#loading').show();
        },
        success: function(data)
        {
            console.log(data);

            window.location.href = window.location.origin+"/ragaadmin/Admin/featured_ads";

            $('#loading').hide();
        }
    });
}

function adssearch(){

    var ad_title=$("#ad_search").val();

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/adsearch',
        data: {
            ad_title:ad_title,
        },
        cache: false,
        beforeSend: function(){

            $('#ad_search').addClass('loadingnewloader');
        },
        success: function(data)
        {
            $("#displaysad").html(data).show();
            $('#ad_search').removeClass('loadingnewloader');
        }
    });
}

function eventsearch(){

    var evnt_title=$("#event_search").val();

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/eventsearch',
        data: {
            evnt_title:evnt_title,
        },
        cache: false,
        beforeSend: function(){

            $('#event_search').addClass('loadingnewloader');
        },
        success: function(data)
        {
            console.log(data);

            $("#displaysevents").html(data).show();

            $('#event_search').removeClass('loadingnewloader');
        }
    });
}

function savevalid(){

    if($('#citydetails').val() == ""){
        BootstrapDialog.alert('Cannot Be Added Without City Name');
        return false;
    }
    if($('#countrydetails').val() == ""){
        BootstrapDialog.alert('Cannot Be Added Without Country Name');
        return false;
    }
    if($('#zonedetails').val() == ""){
        BootstrapDialog.alert('Cannot Be Added Without Zone');
        return false;
    }
}

function savevalidbtngenre(){

    var genre=$('#genredetails').val()

    if(genre == ""){
        BootstrapDialog.alert('Cannot Add Blank Data');
        return false;
    }
}

function savevalidbtnvocal(){

    var vocal=$('#vocaldetails').val()

    if(vocal == ""){
        BootstrapDialog.alert('Cannot Add Blank Data');
        return false;
    }
}

function savevalidbtnstringinstr(){

    var string=$('#stringinstrumentdetails').val()

    if(string == ""){
        BootstrapDialog.alert('Cannot Add Blank Data');
        return false;
    }
}

function savevalidbtnreedinstr(){

    var reed=$('#reedinstrumentdetails').val()

    if(reed == ""){
        BootstrapDialog.alert('Cannot Add Blank Data');
        return false;
    }
}

function savevalidbtnwindinstr(){

    var wind=$('#windinstrumentdetails').val();

    if(wind == ""){
        BootstrapDialog.alert('Cannot Add Blank Data');
        return false;
    }
}

function savevalidusertype(){

    var usertype=$('#usertypedetails').val();

    if(usertype == ""){
        BootstrapDialog.alert('Cannot Add Blank Data');
        return false;
    }
}

function searchloc(){

    if($('#citydetails').val() == ""){
        BootstrapDialog.alert('Please Provide City Name To Search');
        return false;
    }
}

function confirmloc(){

    var test=confirm("Are you sure want to delete?");

    if(test == true){
    return true;
    }else{
        return false;
    }
}

function validate_slider(){

    var fileval = $("input:file").val();
    var slide_desc = $("#slide_desc").val();
    var position = $("#position").val();

    if(slide_desc == ""){
        BootstrapDialog.alert('Please Provide Description');
        return false;
    }

    if(position == ""){
        BootstrapDialog.alert('Please Provide Position');
        return false;
    }

    if (fileval != "") {
        var file_ext = fileval.substring(fileval.lastIndexOf('.') + 1).toLowerCase();

        if (file_ext == 'jpeg' || file_ext == 'jpg' || file_ext == 'png') {
            return true;
        }else {
            BootstrapDialog.alert('Please Select Valid Image');
            return false;
        }
    }else{
        BootstrapDialog.alert('Please Select A Image');
        return false;
    }


}

function validate_testi() {

    var fileval = $("input:file").val();
    var fullname = $("#fullname").val();
    var location = $("#location").val();
    var details = $("#ads_desc").val();
    var prof_url = $("#prof_url").val();
    var hiddenpic = $("#hiddenpic").val();

if(fullname == ""){
    BootstrapDialog.alert('Please Provide Full Name');
    return false;
}


    if(location == ""){
        BootstrapDialog.alert('Please Provide Location');
        return false;
    }

    if(details == ""){
        BootstrapDialog.alert('Please Provide Details');
        return false;
    }

    if (fileval == "" && hiddenpic == "") {
        BootstrapDialog.alert('Please Select A Image');
        return false;
    }

    if (fileval != "") {
        var file_ext = fileval.substring(fileval.lastIndexOf('.') + 1).toLowerCase();

        if (file_ext == 'gif' || file_ext == 'jpg' || file_ext == 'png') {
            return true;
        } else {
            BootstrapDialog.alert('Please Select Valid Image');
            return false;
        }
    }

}

function savevalidbtnseek(){

}

/*Testimonials starting*/
function testimonialsval(val){

console.log(val);

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/gettingtestimonials',
        data: {
            testimonials_id:val,
        },
        cache: false,

        success: function(data)
        {
            var prsejsn=$.parseJSON(data);

            console.log(prsejsn);

            $("#testimonials_id").val(prsejsn[0].testimonials_id);
            $("#fullname").val(prsejsn[0].full_name);
            $("#location").val(prsejsn[0].location);
            $("#prof_url").val(prsejsn[0].profile_url);
            $("#ads_desc").val(prsejsn[0].detail_info);
            $("#hiddenpic").val(prsejsn[0].pic_url);

            //console.log("Filename");
            //console.log($('#fileToUpload').val());
        }
    });
}
/*Testimonials ending*/

function slideredit(val){


    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/gettingsliders',
        data: {
            sliders_id:val,
        },
        cache: false,

        success: function(data)
        {
            var prsejsn=$.parseJSON(data);

            console.log(prsejsn);

            $("#slide_desc").val(prsejsn[0].desc_text);
            $("#position").val(prsejsn[0].position);
            $("#slider_id").val(prsejsn[0].slider_id);

            //console.log("Filename");
            //console.log($('#fileToUpload').val());
        }
    });

}

function showhide(){

    if($('#usercat').val() == 'Ragamix'){
        $('#search1').hide();
    }else{
        $('#search1').show();
    }
}


/*Added By Nandini Starting*/

function savevalidbtnfeaturedprofile(){

    var featured_profile=$('#positionprof').val()

    if(featured_profile == ""){
        BootstrapDialog.alert('Cannot Be Added Without Position Id');
        return false;
    }
        return true;
}

function savevalidbtnfeaturedevent(){

    var featured_event=$('#positionevnt').val()

    if(featured_event == ""){
        BootstrapDialog.alert('Cannot Be Added Without Position Id');
        return false;
    }
}

function savevalidbtnfeaturedadd(){

    var featured_event=$('#positionadd').val()

    if(featured_event == ""){
        BootstrapDialog.alert('Cannot Be Added Without Position Id');
        return false;
    }
}

function gettinglocation(val,val1,val2,val3,val4){
    $('#citydetails').val(val);
    $('#countrydetails').val(val1);
    $('#zonedetails').val(val2);
    $('#fullname').val(val3);
    $('#cityname').val(val4);
}

function gettingstringinstrument(val,val1){
    $('#stringinstrumentdetails').val(val);
    $('#stringinstrumentid').val(val1);
}

function gettingwindinstrument(val,val1){
    $('#windinstrumentdetails').val(val);
    $('#windinstrumentid').val(val1);
}

function gettingreedinstrument(val,val1){
    $('#reedinstrumentdetails').val(val);
    $('#reedinstrumentid').val(val1);
}

function gettinggenre(val,val1){
    $('#genredetails').val(val);
    $('#genreid').val(val1);
}

function gettingvocal(val,val1){
    $('#vocaldetails').val(val);
    $('#vocalid').val(val1);
}
/*Added By Nandini Ending*/

function seekval(val){

    console.log(val);

    $.ajax({
        type: "POST",
        url: window.location.origin+'/ragaadmin/Admin/seekingajax',
        data: {
            rm_id:val,
        },
        cache: false,

        success: function(data)
        {
            var jsr=$.parseJSON(data);

            console.log(jsr);

            $('#full_name_seek').val(jsr[0].user_name);
            $('#keyword').val(jsr[0].keyword);
            $('#location').val(jsr[0].location);
            $('#description').val(jsr[0].description);
            $('#hashtags').val(jsr[0].hashtags);
            $('#rm_id_take').val(jsr[0].rm_id);
            $('#serial_no').val(jsr[0].serial_no);

            console.log(jsr[0].rm_id);

        }
    });

}

function seekingkeyup(){

    console.log("hello inside keyup function");

    var min_length = 0;
    var searchval=$("#search1").val();

    console.log(searchval);

    if(searchval.length > min_length)
    {
        $.ajax({
            type: "POST",
            url: window.location.origin+'/ragaadmin/Admin/seekingsearch',
            data: {
                search:searchval,usercat:"",location:""
            },
            cache: false,
            beforeSend: function(){

                $('#search1').addClass('loadingnewloader');
            },
            success: function(data)
            {
                console.log(data);

                $("#displays").html(data).show();

                $('#search1').removeClass('loadingnewloader');
            }
        });
    }
    else
    {
        $("#displays").hide();
    }return false;
}

function savevalidbtnseek(){

}


$(document).ready(function(){

    $('#search1').hide();

    //used for container height dynamic in dashboard page
    $("#columnTwo").height($("#columnOne").height());
    $("#columnThree").height($("#columnOne").height());
    $("#columnFour").height($("#columnOne").height());
    $("#columnFive").height($("#columnOne").height());
    $("#columnSix").height($("#columnOne").height());
    $("#columnSeven").height($("#columnOne").height());
    $("#columnEight").height($("#columnOne").height());
    $("#columnNine").height($("#columnOne").height());
    $("#columnTen").height($("#columnOne").height());
    $("#columnEleven").height($("#columnOne").height());
    $("#columnTwelve").height($("#columnOne").height());

//alert with bootstrap
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
    $("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#error-alert").slideUp(500);
    });
    $("#counterCtrl").click(function(){
        $(".showCountr").toggle();
    });

    $("#notification").fadeIn("slow").delay(4000).fadeOut("slow");
    $(".dismiss").click(function(){
        $("#notification").fadeOut("slow");
    });




    /*Ajax call for search Profile */
    $("#search").keyup(function()
    {
        var min_length = 0;
        $('#display').removeClass('disp');
        var searchbox = $(this).val();
        var location = $("#locationname").val();
        var usercat = $("#usercat").val();

        //Cookies.set('name', searchbox);
        //var dataString = 'searchword='+ searchbox;
        if(searchbox.length > min_length)
        {
            $.ajax({
                type: "POST",
                url: window.location.origin+'/ragaadmin/Admin/search',
                data: {
                    search:searchbox,usercat:usercat,location:location
                },
                cache: false,
                beforeSend: function(){

                    $('#search').addClass('loadingnewloader');
                },
                success: function(data)
                {
                    console.log(data);

                    $("#displays").html(data).show();

                    $('#search').removeClass('loadingnewloader');
                }
            });
        }
        else
        {
            $("#displays").hide();
        }return false;
    });

    $('#example').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'lfiptB',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //DataTable For location Page
    $('#locdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //DataTable For usertype
    $('#usertypedatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //DataTable For stringinstrument
    $('#stringinstrdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //DataTable For reedinstrument
    $('#reedinstrdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //DataTable For windinstrument
    $('#windinstrdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );


    //DataTable For vocal
    $('#vocaldatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //DataTable For genre
    $('#genredatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "555px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //For seeking data

    $('#seekingdata').DataTable(
        {
            "aaSorting": [],
            "scrollY": "500px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'fpt',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //For seeking data

    $('#slidercontrol').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 't',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    $('#testimonial').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 't',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

});

